<?php

use yii\db\Migration;

/**
 * Handles the creation of table `house_documents`.
 */
class m180529_180109_create_house_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('house_documents', [
            'id' => $this->primaryKey(),
            'house_id' => $this->integer(),
            'title' => $this->string(255),
            'path' => $this->text(),
        ]);

        $this->createIndex('idx-house_documents-house_id', 'house_documents', 'house_id', false);
        $this->addForeignKey("fk-house_documents-house_id", "house_documents", "house_id", "house", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-house_documents-house_id','house_documents');
        $this->dropIndex('idx-house_documents-house_id','house_documents');
        
        $this->dropTable('house_documents');
    }
}
