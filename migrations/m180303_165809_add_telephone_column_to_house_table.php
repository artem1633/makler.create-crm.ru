<?php

use yii\db\Migration;

/**
 * Handles adding telephone to table `house`.
 */
class m180303_165809_add_telephone_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'telephone', $this->string(255));
        $this->addColumn('clients_houses', 'telephone', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'telephone');
        $this->dropColumn('clients_houses', 'telephone');
    }
}
