<?php

use yii\db\Migration;

/**
 * Handles adding public_site to table `house`.
 */
class m180313_070836_add_public_site_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'public_site', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'public_site');
    }
}
