<?php

use yii\db\Migration;

/**
 * Handles the creation of table `house_file`.
 */
class m180216_153121_create_house_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('house_file', [
            'id' => $this->primaryKey(),
            'house_id' => $this->integer(),
            'title' => $this->string(255),
            'path' => $this->text(),
        ]);

        $this->createIndex('idx-house_file-house_id', 'house_file', 'house_id', false);
        $this->addForeignKey("fk-house_file-house_id", "house_file", "house_id", "house", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-house_file-house_id','house_file');
        $this->dropIndex('idx-house_file-house_id','house_file');

        $this->dropTable('house_file');
    }
}
