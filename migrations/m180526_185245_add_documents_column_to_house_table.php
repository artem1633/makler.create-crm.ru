<?php

use yii\db\Migration;

/**
 * Handles adding documents to table `house`.
 */
class m180526_185245_add_documents_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'documents',  $this->text()->comment('Документы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'documents');
    }
}
