<?php

use yii\db\Migration;

/**
 * Handles adding furniture to table `house`.
 */
class m180430_152046_add_furniture_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'furniture', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'furniture');
    }
}
