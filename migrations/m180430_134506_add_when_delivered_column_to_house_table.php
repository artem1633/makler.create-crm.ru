<?php

use yii\db\Migration;

/**
 * Handles adding when_delivered to table `house`.
 */
class m180430_134506_add_when_delivered_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'when_delivered', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'when_delivered');
    }
}
