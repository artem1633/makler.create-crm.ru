<?php

use yii\db\Migration;

/**
 * Handles adding furniture to table `clients_houses`.
 */
class m180527_141717_add_furniture_column_to_clients_houses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients_houses', 'furniture', $this->boolean()->comment('Без мебели'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('clients_houses', 'furniture');
    }
}
