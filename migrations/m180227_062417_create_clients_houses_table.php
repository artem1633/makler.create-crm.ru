<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_houses`.
 */
class m180227_062417_create_clients_houses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients_houses', [
            'id' => $this->primaryKey(),
            'clients_id' => $this->integer()->comment('Клиент'),
            'house_id' => $this->integer()->comment('Дом/Квартира/Комната'),
            'commentary' => $this->text()->comment('Комментарий'),
            'cause' => $this->text()->comment('Причина сьезда'),
            'situation' => $this->integer()->comment('Ситуация'),
            'commission' => $this->float()->comment('Комиссия'),
            'date_add_commission' => $this->date()->comment('Дата оплаты'),

            //--------------------------------------------------------------------------//
            'date_cr' => $this->datetime()->comment('Дата создания'),
            'date_up' => $this->datetime()->comment('Дата изменения'),
            'house_type' => $this->integer()->notNull()->comment('Квартиры/Комнаты/Дома'),
            //----------------------- Поля для посуточного типа ------------------------//
            'begin_date' => $this->date()->comment('Дата начала'),
            'end_date' => $this->date()->comment('Дата окончании'),
            //--------------------------------------------------------------------------//
            'status' => $this->integer()->comment('Статус'),
            'rooms_count' => $this->integer()->comment('Количество комнат'),
            'type' => $this->integer()->comment('Тип дома'),
            'floor' => $this->integer()->comment('Этаж'),
            'house_floor' => $this->integer()->comment('Этажей в доме'),
            'total_area' => $this->float()->comment('Общая площадь'),
            'kitchen_area' => $this->float()->comment('Площадь кухни'),
            'living_area' => $this->float()->comment('Жилая площадь'),
            'beds_count' => $this->integer()->comment('Количество кроватей'),
            'berth_count' => $this->integer()->comment('Количество спальных мест'),
            'conditioner' => $this->boolean()->comment('Кондиционер'),
            'camin' => $this->boolean()->comment('Камин'),
            'balcony' => $this->boolean()->comment('Балкон'),
            'microwave' => $this->boolean()->comment('Микроволновка'),
            'fridge' => $this->boolean()->comment('Холодильник'),
            'washer' => $this->boolean()->comment('Стиральная машина'),
            'wifi' => $this->boolean()->comment('WiFi'),
            'tv' => $this->boolean()->comment('Телевизор'),
            'digital_tv' => $this->boolean()->comment('Цифровое ТВ'),
            'with_pets' => $this->boolean()->comment('Можно с питомцами'),
            'with_childrens' => $this->boolean()->comment('Можно с детьми'),
            'with_events' => $this->boolean()->comment('Можно для мероприятий'),
            'description' => $this->text()->comment('Описание'),
            'cost' => $this->float()->comment('Цена'),
            'zalog' => $this->integer()->comment('Залог'),
            'video' => $this->string(255)->comment('Видео'),
            'tempFiles' => $this->text()->comment('Фото'),
            'surname_owner' => $this->string(255)->comment('Фамилия собственника'),
            'name_owner' => $this->string(255)->comment('Имя собственника'),
            'middle_name_owner' => $this->string(255)->comment('Отчество собственника'),
            'last_call' => $this->date()->comment('Последний звонок собеседнику'),
            'call_back' => $this->date()->comment('Перезвонить'),
            'purpose_call' => $this->string(255)->comment('Цель звонка'),
            'public_avito' => $this->boolean()->comment('Публикация на авито'),
            'description_object' => $this->text()->comment('Описание объекта'),
            'client_id' => $this->integer()->comment('Кто снимает сейчас'),
            //-----------------------Поля для дома---------------------------------------//
            'metro' => $this->string(255)->comment('Метро рядом'),
            'wall_material' => $this->integer()->comment('Материал стен'),
            'distance_to_city' => $this->float()->comment('Расстояние до города'),
            'home_area' => $this->float()->comment('Площадь дома'),
            'land_area' => $this->float()->comment('Площадь участка'),
            //-----------------------Поля для Местоположение---------------------------------------//
            'city' => $this->string(255)->comment('Город, населённый пункт'),
            'location_metro' => $this->string(255)->comment('Метро'),
            'address' => $this->string(255)->comment('Адрес'),
            'dom' => $this->string(255)->comment('Дом'),
            'map' => $this->string(255)->comment('Посмотреть на карте'),
            //----------Доп. параметры ---------------------//
            'coordinate_x' => $this->string(255)->comment('Координата х'),
            'coordinate_y' => $this->string(255)->comment('Координата y'),
            'room_area' => $this->integer()->comment('Площадь комнаты'),
            'home_type' => $this->integer()->comment('Вид обьекта'),
            'home_floor' => $this->integer()->comment('Этажей в доме'),

        ]);

        $this->createIndex('idx-clients_houses-clients_id', 'clients_houses', 'clients_id', false);
        $this->addForeignKey("fk-clients_houses-clients_id", "clients_houses", "clients_id", "clients", "id");

        /*$this->createIndex('idx-clients_houses-house_id', 'clients_houses', 'house_id', false);
        $this->addForeignKey("fk-clients_houses-house_id", "clients_houses", "house_id", "house", "id");*/
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-clients_houses-clients_id','clients_houses');
        $this->dropIndex('idx-clients_houses-clients_id','clients_houses');

        /*$this->dropForeignKey('fk-clients_houses-house_id','clients_houses');
        $this->dropIndex('idx-clients_houses-house_id','clients_houses');*/

        $this->dropTable('clients_houses');
    }
}
