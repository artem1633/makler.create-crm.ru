<?php

use yii\db\Migration;

/**
 * Handles the creation of table `purchased_licenses`.
 */
class m180209_151604_create_purchased_licenses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('purchased_licenses', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'license_type' => $this->integer(),
            'user_type' => $this->integer(),
            'month' => $this->integer(),
            'cost' => $this->float(),
            'data' => $this->datetime(),
        ]);

        $this->createIndex('idx-purchased_licenses-user_id', 'purchased_licenses', 'user_id', false);
        $this->addForeignKey("fk-purchased_licenses-user_id", "purchased_licenses", "user_id", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-purchased_licenses-user_id','purchased_licenses');
        $this->dropIndex('idx-purchased_licenses-user_id','purchased_licenses');
        
        $this->dropTable('purchased_licenses');
    }
}
