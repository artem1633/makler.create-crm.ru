<?php

use yii\db\Migration;

/**
 * Handles adding information to table `house`.
 */
class m180430_141931_add_information_column_to_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('house', 'information', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('house', 'information');
    }
}
