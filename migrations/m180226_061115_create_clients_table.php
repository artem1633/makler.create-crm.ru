<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m180226_061115_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment('ФИО'),
            'status' => $this->integer()->comment('Статус'),
            'type' => $this->integer()->comment('Тип'),
            'category' => $this->integer()->comment('Категория'),
            'room_count' => $this->integer()->comment('К-во комнат'),
            'budjet' => $this->float()->comment('Бюджет'),
            'mebel' => $this->integer()->comment('Мебель'),
            'floor' => $this->integer()->comment('Этаж не выше'),
            'last_call' => $this->date()->comment('Последний звонок'),
            'why_call' => $this->date()->comment('Позвонить'),
            'why_comment' => $this->text()->comment('Зачем'),
            'comment' => $this->text()->comment('Комментария'),
            'telephone' => $this->string(255)->comment('Телефон'),
            'date_cr' => $this->date()->comment('Создано'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clients');
    }
}
