<?php

use yii\db\Migration;
use app\models\Users;
use app\models\Columns;

/**
 * Class m180530_070000_add_no_armchair_column
 */
class m180530_070000_add_no_armchair_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $users = Users::find()->all();
        foreach ($users as $user) 
        {
            $column = Columns::find()->where(['user_id' => $user->id, 'type' => 'furniture'])->one();
            if($column == null)
            {
                $column = new Columns();
                $column->user_id = $user->id;
                $column->type = 'furniture'; //Без мебели
                $column->name = 'Без мебели';
                $column->status = 0;
                $column->order_number = 1;
                $column->save();
            }
        }
    }

    public function down()
    {

    }
}
