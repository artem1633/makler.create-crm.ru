<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171209_075358_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'creation_date' => $this->datetime(),
            'end_date' => $this->date(),
            'birthday'=> $this->date(),
            'telephone' => $this->string(255),
            'email' => $this->string(255),
            'login' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'type' => $this->integer()->notNull(),
            'balance'=> $this->float()->defaultValue(0),
            'license' => $this->integer(),
            'licensed_user' => $this->integer(),
        ]);

        $this->insert('users',array(
            'fio' => 'Власов Александр Сергеевич',
            'creation_date' => date('Y-m-d H:i:s'),
            'end_date' => date('Y-m-d'),
            'birthday'=> date('Y-m-d'),
            'telephone' => '+7-921-323-44-56',
            'email' => 'vlasoff@mail.ru',
            'login' => 'vlasoff',
            'password' => md5('admin'),
            'type' => 1,
            'balance'=> 0,
            'license'=> 0,
            'licensed_user' => 0,
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
