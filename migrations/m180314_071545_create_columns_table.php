<?php

use yii\db\Migration;

/**
 * Handles the creation of table `columns`.
 */
class m180314_071545_create_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('columns', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type' => $this->string(255),
            'name' => $this->string(255),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('columns');
    }
}
