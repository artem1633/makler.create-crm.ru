<?php

use yii\db\Migration;

/**
 * Handles adding order_number to table `columns`.
 */
class m180322_072026_add_order_number_column_to_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('columns', 'order_number', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('columns', 'order_number');
    }
}
