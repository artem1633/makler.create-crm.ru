<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nastroyka`.
 */
class m180313_095149_create_nastroyka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('nastroyka', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'coordinate_x' => $this->string(255),
            'coordinate_y' => $this->string(255),
            'city' => $this->string(255),
            'location_metro' => $this->string(255),
            'address' => $this->string(255),
            'dom' => $this->string(255),
        ]);
        $this->createIndex('idx-nastroyka-user_id', 'nastroyka', 'user_id', false);
        $this->addForeignKey("fk-nastroyka-user_id", "nastroyka", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-nastroyka-user_id','nastroyka');
        $this->dropIndex('idx-nastroyka-user_id','nastroyka');

        $this->dropTable('nastroyka');
    }
}
