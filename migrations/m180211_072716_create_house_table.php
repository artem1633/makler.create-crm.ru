<?php

use yii\db\Migration;

/**
 * Handles the creation of table `house`.
 */
class m180211_072716_create_house_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('house', [
            'id' => $this->primaryKey(),
            'date_cr' => $this->datetime()->comment('Дата создания'),
            'date_up' => $this->datetime()->comment('Дата изменения'),
            'house_type' => $this->integer()->notNull()->comment('Квартиры/Комнаты/Дома'),
            //----------------------- Поля для посуточного типа ----------//
            'begin_date' => $this->date()->comment('Дата начала'),
            'end_date' => $this->date()->comment('Дата окончании'),
            //------------------------------------------------------------//
            'status' => $this->integer()->comment('Статус'),
            'rooms_count' => $this->integer()->comment('Количество комнат'),
            'type' => $this->integer()->comment('Тип дома'),
            'floor' => $this->integer()->comment('Этаж'),
            'house_floor' => $this->integer()->comment('Этажей в доме'),
            'total_area' => $this->float()->comment('Общая площадь'),
            'kitchen_area' => $this->float()->comment('Площадь кухни'),
            'living_area' => $this->float()->comment('Жилая площадь'),
            'beds_count' => $this->integer()->comment('Количество кроватей'),
            'berth_count' => $this->integer()->comment('Количество спальных мест'),
            'conditioner' => $this->boolean()->comment('Кондиционер'),
            'camin' => $this->boolean()->comment('Камин'),
            'balcony' => $this->boolean()->comment('Балкон'),
            'microwave' => $this->boolean()->comment('Микроволновка'),
            'fridge' => $this->boolean()->comment('Холодильник'),
            'washer' => $this->boolean()->comment('Стиральная машина'),
            'wifi' => $this->boolean()->comment('WiFi'),
            'tv' => $this->boolean()->comment('Телевизор'),
            'digital_tv' => $this->boolean()->comment('Цифровое ТВ'),
            'with_pets' => $this->boolean()->comment('Можно с питомцами'),
            'with_childrens' => $this->boolean()->comment('Можно с детьми'),
            'with_events' => $this->boolean()->comment('Можно для мероприятий'),
            'description' => $this->text()->comment('Описание'),
            'cost' => $this->float()->comment('Цена'),
            'zalog' => $this->integer()->comment('Залог'),
            'video' => $this->string(255)->comment('Видео'),
            'tempFiles' => $this->text()->comment('Фото'),
            'surname_owner' => $this->string(255)->comment('Фамилия собственника'),
            'name_owner' => $this->string(255)->comment('Имя собственника'),
            'middle_name_owner' => $this->string(255)->comment('Отчество собственника'),
            'last_call' => $this->date()->comment('Последний звонок собеседнику'),
            'call_back' => $this->date()->comment('Перезвонить'),
            'purpose_call' => $this->string(255)->comment('Цель звонка'),
            'public_avito' => $this->boolean()->comment('Публикация на авито'),
            'description_object' => $this->text()->comment('Описание объекта'),
            'client_id' => $this->integer()->comment('Кто снимает сейчас'),
            //-----------------------Поля для дома---------------------------------------//
            'metro' => $this->string(255)->comment('Метро рядом'),
            'wall_material' => $this->integer()->comment('Материал стен'),
            'distance_to_city' => $this->float()->comment('Расстояние до города'),
            'home_area' => $this->float()->comment('Площадь дома'),
            'land_area' => $this->float()->comment('Площадь участка'),
            //-----------------------Поля для Местоположение---------------------------------------//
            'city' => $this->string(255)->comment('Город, населённый пункт'),
            'location_metro' => $this->string(255)->comment('Метро'),
            'address' => $this->string(255)->comment('Адрес'),
            'dom' => $this->string(255)->comment('Дом'),
            'map' => $this->string(255)->comment('Посмотреть на карте'),
            //----------Доп. параметры ---------------------//
            'coordinate_x' => $this->string(255)->comment('Координата х'),
            'coordinate_y' => $this->string(255)->comment('Координата y'),
            'room_area' => $this->integer()->comment('Площадь комнаты'),
            'home_type' => $this->integer()->comment('Вид обьекта'),
            'home_floor' => $this->integer()->comment('Этажей в доме'),
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('house');
    }
}
