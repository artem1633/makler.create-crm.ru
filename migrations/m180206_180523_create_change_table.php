<?php

use yii\db\Migration;

/**
 * Handles the creation of table `change`.
 */
class m180206_180523_create_change_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('change', [
            'id' => $this->primaryKey(),
            'data' => $this->datetime(),
            'user_id' => $this->integer(),
            'summa' => $this->float(),
        ]);

        $this->createIndex('idx-change-user_id', 'change', 'user_id', false);
        $this->addForeignKey("fk-change-user_id", "change", "user_id", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-change-user_id','change');
        $this->dropIndex('idx-change-user_id','change');

        $this->dropTable('change');
    }
}
