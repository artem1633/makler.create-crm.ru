<?php

use yii\db\Migration;

/**
 * Handles the creation of table `owner_contacts`.
 */
class m180217_163815_create_owner_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('owner_contacts', [
            'id' => $this->primaryKey(),
            'telephone' => $this->string(255),
            'contact' => $this->string(255),
            'house_id' => $this->integer(),
        ]);

        $this->createIndex('idx-owner_contacts-house_id', 'owner_contacts', 'house_id', false);
        $this->addForeignKey("fk-owner_contacts-house_id", "owner_contacts", "house_id", "house", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-owner_contacts-house_id','owner_contacts');
        $this->dropIndex('idx-owner_contacts-house_id','owner_contacts');

        $this->dropTable('owner_contacts');
    }
}
