<?php

use yii\helpers\Html;
use app\models\House; 

?>
<div class="zalog-create"><br> 

	<?php if($model->scenario == House::SCENARIO_PARAMETER): ?>
        <?= $this->render('parametr', [
            'model' => $model,
        ]) ?>
    <?php endif; ?> 

    <?php if($model->scenario == House::SCENARIO_OPTIONS): ?>
        <?= $this->render('options', [
            'model' => $model,
        ]) ?> 
    <?php endif; ?>

    <?php if($model->scenario == House::SCENARIO_DESCRIPTION): ?>
        <?= $this->render('description', [
            'model' => $model,
            'modelUpload' => $modelUpload,
        ]) ?>
    <?php endif; ?>

    <?php if($model->scenario == House::SCENARIO_LOCATION): ?>
        <?= $this->render('location', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>

    <?php if($model->scenario == House::SCENARIO_DATAS): ?>
        <?= $this->render('datas', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>

    <?php if($model->scenario == House::SCENARIO_OWNER): ?>
        <?= $this->render('owner', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>
    
</div>
