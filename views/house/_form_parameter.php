<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$house_type = 2;
if($model->house_type == 1 || $model->house_type == 2) { $rooms_count_label = 'Количество комнат'; $house_type = 1; }
if($model->house_type == 3 || $model->house_type == 4) { $rooms_count_label = 'Комнат в квартире'; $house_type = 1; }

?>

<?php 
$layout = <<< HTML
    <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">С</span>
    {input1}
    <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">По</span>
    {input2}
    <span class="input-group-addon" style="background-color: #ecf0f5; width: 50px; color:black;">
    </span>

HTML;
?>
    <?php $form = ActiveForm::begin(['id' => 'parameter-form']); ?>
    
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>

        <?php if($house_type == 1){ ?> 
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model->getStatus()); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'type')->dropDownList($model->getTypeHouseList()); ?>
                </div>
                <div class="col-md-3">
                    
                    <?php if($model->house_type == 3 || $model->house_type == 4) echo $form->field($model, 'rooms_count')->dropDownList($model->getRoomsCountList('komnata'))->label($rooms_count_label);
                        else echo $form->field($model, 'rooms_count')->dropDownList($model->getRoomsCountList())->label($rooms_count_label);
                    ?>
                </div>            
                <div class="col-md-3">
                    <?php if($model->house_type == 1 || $model->house_type == 2) echo $form->field($model,'total_area')->textInput(['type' =>'number',]);
                          if($model->house_type == 3 || $model->house_type == 4) echo $form->field($model,'room_area')->textInput(['type' =>'number',]);
                    ?>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'floor')->dropDownList(
                        $model->getFloorList(),
                        [
                            'onchange' => '
                                var a = parseInt( $("#house-house_floor").val() );
                                var floor = parseInt( $("#house-floor").val() );
                                
                                $.post( "house-floor-list?id='.'"+$(this).val(), function( data ){
                                    $( "#house-house_floor" ).html( data);
                                    if(a < floor ) { $( "#house-house_floor" ).val(a);  }
                                    else { $( "#house-house_floor" ).val(floor);  }
                                });
                            ',
                        ]
                    ); ?>
                </div>  
                <div class="col-md-3">
                    <?= $form->field($model, 'house_floor')->dropDownList($model->getHouseFloorList($model->floor)); ?>
                </div>
                <?php if($model->house_type == 1 || $model->house_type == 2){ ?>
                    <div class="col-md-3">
                        <?= $form->field($model,'living_area')->textInput(['type' =>'number',]) ?>
                    </div>
                    <div class="col-md-3"> 
                        <?= $form->field($model,'kitchen_area')->textInput(['type' =>'number',]) ?>
                    </div>
                <?php } ?>
            <?php if($model->house_type == 2 || $model->house_type == 4) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <?php
                            echo '<label class="control-label">Срок снятия квартиры </label>';
                            echo DatePicker::widget([
                                'model' => $model,
                                'attribute' => 'begin_date',
                                'attribute2' => 'end_date',
                                'options' => ['placeholder' => 'Дата начала', /*'style' => 'margin-top:-7px;'*/],
                                'options2' => ['placeholder' => 'Дата окончании', /*'style' => 'margin-top:-7px;'*/],
                                'type' => DatePicker::TYPE_RANGE,
                                'form' => $form,
                                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                                'layout' => $layout,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy',
                                    'todayHighlight' => false,
                                ]
                            ]);
                        ?>
                    </div>                               
                </div>
            <?php } ?>
            </div>
        <?php }else { ?>      
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model->getStatus()); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'home_type')->dropDownList($model->getTypeHomeList()); ?>
                </div>
                <div class="col-md-3"> 
                    <?= $form->field($model, 'home_floor')->dropDownList($model->getHomeFloorList()); ?>
                </div>
                <div class="col-md-3"> 
                    <?= $form->field($model, 'metro')->textInput() ?>       
                </div>            
                    
            </div>  
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model,'wall_material')->dropDownList($model->getWallMaterialList()); ?>    
                </div>  
                <div class="col-md-3">
                    <?= $form->field($model,'distance_to_city')->textInput(['type' =>'number',]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'land_area')->textInput(['type' =>'number',]) ?>
                </div>
                <div class="col-md-3"> 
                    <?= $form->field($model, 'home_area')->textInput(['type' =>'number',]) ?>
                </div> 
            </div>
        <?php if($model->house_type == 6) { ?>
            <div class="row">
                <div class="col-md-6">
                    <?php
                        echo '<label class="control-label">Срок снятия квартиры </label>';
                        echo DatePicker::widget([
                            'model' => $model,
                            'attribute' => 'begin_date',
                            'attribute2' => 'end_date',
                            'options' => ['placeholder' => 'Дата начала', /*'style' => 'margin-top:-7px;'*/],
                            'options2' => ['placeholder' => 'Дата окончании', /*'style' => 'margin-top:-7px;'*/],
                            'type' => DatePicker::TYPE_RANGE,
                            'form' => $form,
                            'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                            'layout' => $layout,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                                'todayHighlight' => false,
                            ]
                        ]);
                    ?>
                </div>
            </div>
        <?php } ?>
        <?php } ?>
          
        <?php ActiveForm::end(); ?>
