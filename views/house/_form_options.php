<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <?php $form = ActiveForm::begin(['id' => 'options-form']); ?>
    
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'beds_count')->dropDownList($model->getBedsCountList()); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'berth_count')->dropDownList($model->getBertsCountList()); ?>
            </div>
        </div>
        
        <div class="row" style="margin-top: 20px;">            
            <div class="col-md-4">
                <?= $form->field($model, 'camin')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'balcony')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'furniture')->checkbox(); ?>
            </div>
        </div>
        <div class="row">      
            <div class="col-md-4">
                <?= $form->field($model, 'microwave')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'fridge')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'conditioner')->checkbox(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'wifi')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tv')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'washer')->checkbox(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'with_pets')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'with_childrens')->checkbox(); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'digital_tv')->checkbox(); ?>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'with_events')->checkbox(); ?>
            </div>
        </div>
    
    <?php ActiveForm::end(); ?>
