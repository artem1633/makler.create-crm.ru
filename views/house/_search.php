<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-1">          
        <?= $form->field($model, 'search_id')->textInput(['value' => $post['HouseSearch']['search_id'], 'placeholder' => '  ID'])->label(''); ?>         
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'search_owner_surname')->textInput(['value' => $post['HouseSearch']['search_owner_surname'], 'placeholder' => 'Фамилии собственника'])->label(''); ?>         
    </div>

    <div class="col-md-4">          
        <?= $form->field($model, 'search_owner_name')->textInput(['value' => $post['HouseSearch']['search_owner_name'],'placeholder' => ' Имени собственника',])->label(''); ?>       
    </div>
    <div class="col-md-3">
                
        <?php /*$form->field($model, 'search_telephone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9(999) 999-9999', 
            'options' => [
                'class'=>'form-control',
                'placeholder' => '9(999) 999-9999',
                'value' => $post['search_telephone'], 
            ]
            ])->label('');*/ ?> 
        <?= $form->field($model, 'search_telephone')->textInput(['value' => $post['HouseSearch']['search_telephone'],'placeholder' => 'Телефон номер',])->label(''); ?>           
    </div>
</div>     
    
<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'search_address')->textInput(['value' => $post['HouseSearch']['search_address'], 'placeholder' => 'Введите адрес, улица'])->label(''); ?>         
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'search_home_number')->textInput(['value' => $post['HouseSearch']['search_home_number'], 'placeholder' => 'Введите номер дома'])->label(''); ?>         
    </div>
    <div class="col-md-3">          
        <?= $form->field($model, 'search_city')->textInput(['value' => $post['HouseSearch']['search_city'],'placeholder' => 'Введите города' ])->label(''); ?>       
    </div>
    <div class="col-md-3">          
        <?= $form->field($model, 'search_metro')->textInput(['value' => $post['HouseSearch']['search_metro'], 'placeholder' => 'Введите метро',])->label(''); ?>         
    </div>
    <div class="form-group pull-right">
        <?= Html::submitButton('Поиск', [ 'class' => 'btn btn-primary']) ?>
    </div>  
</div
<?php ActiveForm::end(); ?>
