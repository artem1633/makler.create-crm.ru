<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id' => 'parameter-form']); ?>
   		<div class="row">         
            <div class="col-md-12">
                <?= $form->field($model, 'metro')->textInput() ?>
            </div>    
        </div>
<?php ActiveForm::end(); ?>
