<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Columns;

$house_type = "";
if($type == 1 || $type == 4 || $type == 7) $house_type = "Квартиры";
if($type == 2 || $type == 5 || $type == 8) $house_type = "Комнаты";
if($type == 3 || $type == 6 || $type == 9) $house_type = "Дома";
$columns = new Columns();

$this->title = "Квартиры";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div class="panel panel-inverse" data-sortable-id="ui-widget-1">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title="" data-init="true"><i class="fa fa-plus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Поиск</h4>
    </div>
    <div class="panel-body" style="display: none;">
        <?php echo $this->render('_search', ['model' => $searchModel, 'post' => $post]); ?>
    </div>
</div>

<div class="panel panel-inverse users-index">

    <div class="panel-heading">
        <h4 class="panel-title"><?=$house_type?></h4>
    </div>

    <div class="panel-body">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <h5>На длительный срок <span><?=($apartment_long + $room_long + $home_long)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-'. ($type == 4 ? 'info' : 'default') .' btn-sm">Квартиры <span>'.$apartment_long.'</span></span>', ['/house/apartment','type' => 4], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 4 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 1], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 5 ? 'info' : 'default') .' btn-sm">Комнаты <span>'.$room_long.'</span></span>', ['/house/apartment','type' => 5], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 5 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 3], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 6 ? 'info' : 'default') .' btn-sm">Дома <span>'.$home_long.'</span></span>', ['/house/apartment','type' => 6], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 6 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 5], ['data-pjax'=>0,])?> 
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <h5>Посуточно <span><?=($apartment_short + $room_short + $home_short)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-'. ($type == 7 ? 'info' : 'default') .' btn-sm">Квартиры <span>'.$apartment_short.'</span></span>', ['/house/apartment','type' => 7], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($type == 7 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 2], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($type == 8 ? 'info' : 'default') .' btn-sm">Комнаты <span>'.$room_short.'</span></span>', ['/house/apartment','type' => 8], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($type == 8 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 4], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($type == 9 ? 'info' : 'default') .' btn-sm">Дома <span>'.$home_short.'</span></span>', ['/house/apartment','type' => 9], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($type == 9 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 6], ['data-pjax'=>0,])?>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <h5>Доп. нав</h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-default btn-sm">Клиенты <span>'.$clients.'</span></span>', ['/clients'], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/clients/add'], ['role'=>'modal-remote',])?>
                        <?= Html::a('<span class="btn btn-default btn-sm">Заработано</span>', ['/site/earned'], ['data-pjax'=>0,])?>
                        <a class="btn btn-default btn-sm" href="#">Сайт</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div id="ajaxCrudDatatable">
            <?= GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns->getColumns($type),
            'panelBeforeTemplate' => '&nbsp'.Html::a('<span class="btn btn-primary btn-sm">НАСТРОЙКА СТОЛБЦОВ </span>', ['/house/sorting', 'type' => 1], ['data-pjax'=>0,]),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ]) ?><!-- <br> -->
                <?php //echo $this->render('_table', ['dataProvider' => $dataProvider]); ?>
            </div>
        </div>

        </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
