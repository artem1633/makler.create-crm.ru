<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use unclead\multipleinput\MultipleInput;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            
            <div class="col-md-3">
                <?= $form->field($model, 'surname_owner')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'name_owner')->textInput() ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'middle_name_owner')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'telephone')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php 
                    echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                    'columns' => [
                        [
                            'name'  => 'telephone',
                            'title' => 'Доп. телефон номер',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                //'type' => 'number',
                                'placeholder' => 'Введите',
                            ]
                        ],
                        [
                            'name'  => 'contact',
                            'title' => 'Контакт',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Введите контакт',
                            ]
                        ],                     
                    ]
                ])->label('');
                ?>
            </div>
        </div>

        <!-- -->
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rooms_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'house_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'total_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'begin_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'end_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'room_area')->hiddenInput()->label(false) ?>
        <!-- Доп. параметры для Дома на длительный срок -->
        <?= $form->field($model, 'wall_material')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'distance_to_city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'land_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kitchen_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'living_area')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'conditioner')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'camin')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'balcony')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'microwave')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'fridge')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'washer')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'wifi')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'furniture')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'digital_tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_pets')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_childrens')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_events')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'beds_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'berth_count')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'cost')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'zalog')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'video')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'information')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'description')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'location_metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'address')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'dom')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'coordinate_x')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'coordinate_y')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'last_call')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'call_back')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'when_delivered')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'public_avito')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'description_object')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'purpose_call')->hiddenInput()->label(false) ?>
        <!-- -->

        <div class="form-group">
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
        </div>
          
        <?php ActiveForm::end(); ?>
    </div>
</div>
