<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use app\models\OwnerContacts;

?>
    <?php $form = ActiveForm::begin(['id' => 'owner-form']); ?>    
        <div class="row">            
            <div class="col-md-3">
                <?= $form->field($model, 'surname_owner')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'name_owner')->textInput() ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'middle_name_owner')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'telephone')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                $owner_contact = OwnerContacts::find()->where(['house_id'=> $model->id])->all();
                $array = [];
                foreach ($owner_contact as $value) {
                    $array [] = 
                    [
                        'telephone' => $value->telephone,
                        'contact' => $value->contact,
                    ];
                }
                $model->contacts = $array;

                    echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                    'columns' => [
                        [
                            'name'  => 'telephone',
                            'title' => 'Телефон',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'type' => 'number',
                                'placeholder' => '(999)999-9999',
                            ]
                        ],
                        [
                            'name'  => 'contact',
                            'title' => 'Контакт',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                                'placeholder' => 'Введите контакт',
                            ]
                        ],                     
                    ]
                ])->label('');
                 ?>
            </div>
        </div>
    
    <?php ActiveForm::end(); ?>
