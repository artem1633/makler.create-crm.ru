<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax; 
?>
<?php $form = ActiveForm::begin(['id' => 'date-form']); ?>
<div class="row">            
    <div class="col-md-8">
        <?= $form->field($model, 'client_id')->dropDownList($model->getClientsList(), ['prompt'=>'Выберите', 'disabled' => $model->client_id == null ? false : true, ]) ?>
    </div>
    <div class="col-md-4" style="margin-top: 21px;">
      <?= Html::a('<span class="btn btn-warning pull-right" >Добавить клиента</span>', [Url::to(['/clients/addnew', 'id' => $model->id,])], ['role'=>'modal-remote', 'title'=>'Добавить клиента',]);
       ?>
    </div>
</div> 
<?php ActiveForm::end(); ?>
