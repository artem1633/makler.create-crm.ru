<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use dosamigos\datepicker\DatePicker;

if($model->house_type == 1 || $model->house_type == 2) { $date_label = 'Когда сдана'; $house_type = 1; }
if($model->house_type == 3 || $model->house_type == 4) { $date_label = 'Когда сдана'; $house_type = 1; }
if($model->house_type == 5 || $model->house_type == 6) { $date_label = 'Когда сдан'; $house_type = 1; }
?>
    <?php $form = ActiveForm::begin(['id' => 'date-form']); ?>
    
        <div class="row"> 
            <div class="col-md-8">
                <?= $form->field($model, 'client_id')->dropDownList($model->getClientsList(), ['prompt'=>'Выберите', 'disabled' => $model->client_id == null ? false : true, ]) ?>
            </div>
             <div class="col-md-4">
                <?= $form->field($model, 'public_avito')->dropDownList($model->getAvitoList(), []) ?>
            </div>           
        </div>
        <div class="row"> 
             <div class="col-md-4">
                <?= $form->field($model, 'last_call')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])
                ?>
            </div>
            <div class="col-md-4">
                <?php if($model->house_type == 1 || $model->house_type == 2){ ?>
                <?= $form->field($model, 'when_delivered')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])->label($date_label) ?>
                <?php }?>
                 <?php if($model->house_type == 3 || $model->house_type == 4){ ?>
                <?= $form->field($model, 'when_delivered')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])->label($date_label) ?>
                <?php }?>
                 <?php if($model->house_type == 5 || $model->house_type == 6){ ?>
                <?= $form->field($model, 'when_delivered')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])->label($date_label) ?>
                <?php }?>
            </div>        
            <div class="col-md-4">
                <?= $form->field($model, 'call_back')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])
                ?>
            </div>          
           
          
        </div>
        <div class="row">         
            <div class="col-md-12">
                <?= $form->field($model, 'purpose_call')->textArea(['rows' => 3]) ?>
            </div>
        </div>
        </div>
    
    <?php ActiveForm::end(); ?>
