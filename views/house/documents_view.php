<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Setting */

CrudAsset::register($this);
?>
<div class="table-responsive">
    <?=GridView::widget([
        'id'=>'crud-datatable',
        'dataProvider' => $dataProvider,
        'pjax'=>true,
        'columns' => [
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'title',
                'label' => 'Наименование',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'title',
                'label' => 'Скачать',
                'content' => function($data){
                    return Html::a('Скачать', ['/house/send-document', 'file' => $data->path ],['title'=> 'Скачать', 'data-pjax' => 0]);
                }
            ],
            [
                'class'    => 'kartik\grid\ActionColumn',
                'template' => '{leadDelete}',
                'buttons'  => [
                    'leadDelete' => function ($url, $model) {
                        $url = Url::to(['/house/delete-document', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'role'=>'modal-remote','title'=>'', 
                                  'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                  'data-request-method'=>'post',
                                  'data-toggle'=>'tooltip',
                                  'data-confirm-title'=>'Подтвердите действие',
                                  'data-confirm-message'=>'Вы уверены что хотите удалить данного элемента?',
                        ]);
                    },
                ]
            ]
        ],
        'panelBeforeTemplate' => '&nbsp;',
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
        'headingOptions' => ['style' => 'display: none;'],
        'after'=>'',
        ]
        ])?>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>