<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id' => 'parameter-form']); ?>
    <div class="row">         
            <div class="col-md-12">
                <?= $form->field($model, 'description')->textArea(['rows' => 3]) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>
