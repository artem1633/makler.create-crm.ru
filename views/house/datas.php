<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Zayavka;
use app\models\Relative;
use unclead\multipleinput\MultipleInput;
use kartik\tabs\TabsX;

$items = [
    [
        'label'=>'Параметры',
        'headerOptions' => ['class' => 'disabled']
    ],
    [
        'label'=>'Доп параметры, опции',
        'headerOptions' => ['class' => 'disabled']
    ],
    [
        'label'=>'Описание',
        'headerOptions' => ['class' => 'disabled']
    ],
    [
        'label'=>'Местоположение',
        'headerOptions' => ['class' => 'disabled']
    ],
    [
        'label'=>'Даты',
        'content'=> $this->render('datas_form', ['model' => $model]),
        'active'=> true,
    ],
    [
        'label'=>'Собственник',
        'headerOptions' => ['class' => 'disabled']
    ],
];
?>

<div class="client-form">
    <div class="box box-default">
        <div class="box-body">
    <?php 
        echo TabsX::widget([
            'items'=>$items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
            'bordered'=>true,
        ]);
    ?>
</div>
</div>