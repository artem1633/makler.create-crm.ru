<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\models\HouseFile;
use app\models\HouseDocuments;
//Вы не можете больше загружать фото.
$count = 20 - HouseFile::find()->where(['house_id' => $model->id])->count();
$documentsCount = 10 - HouseDocuments::find()->where(['house_id' => $model->id])->count();

?>
<?php $form = ActiveForm::begin(['id' => 'description-form']); ?>    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'cost')->textInput(['type' =>'number']) ?>
        </div>
        <div class="col-md-4">
                <?= $form->field($model, 'information')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model,'zalog')->dropDownList($model->getZalogList()); ?>
        </div>            
        <div class="col-md-3">
            <?= $form->field($model, 'video')->textInput(['placeholder' => 'Введите ссылку']) ?>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>      
        </div>            
    </div>
    <div class="row">
        <div class="col-md-2">
                <?php /*FileUpload::widget([
                    'model' => $model,
                    'attribute' => 'other_file',
                    'url' => ['house/upload', 'id' => $model->id], // your url, this is just for demo purposes,
                    'options' => ['accept' => 'other_file/*'],
                    'clientOptions' => [
                        'maxFileSize' => 1024*1024*5,
                        'maxFiles' => 10,
                    ],
                    'clientEvents' => [
                        'fileuploaddone' => 'function(e, data) {
                                                console.log(e);
                                                console.log(data);
                                            }',
                        'fileuploadfail' => 'function(e, data) {
                                                console.log(e);
                                                console.log(data);
                                            }',
                    ],
                ]);*/ ?>           
        </div>
    </div>    
        <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
<?php ActiveForm::end(); ?>

<?php if($count > 0 ){ ?>
<div class="row">
    <div class="col-md-6"> 
        <?php $form2= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form2->field($modelUpload, 'anyFiles[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/house/upload']),
                'uploadExtraData' => [
                    'orderId' => $model->id,
                ],
                'maxFileCount' => $count,
                'showCaption' => false,
                'browseClass' => 'btn btn-success',
                'browseIcon' => ' ',
                //'browseLabel' => 'Select Media',
                'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
                'maxFileSize' => 2048,
                'maxImageWidth' => 6000,
                'maxImageHeight' => 6000,
                //'minImageWidth' => 600,
                //'minImageHeight' => 600,
                //'dropZoneTitle' => 'Trascina i files qui...',
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => true,
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '',
                'layoutTemplates' => [
                'actionUpload' => ''
                ],
            ],
            'pluginEvents' => [
                'fileuploaded' => "
                    function(event, data, previewId, index) {                    
                        var image = {
                          name: data.files[index].name,
                          path: data.response.path,
                        };                                       
                        console.log(image);
                        console.log(index);
                        console.log(data.files[index].name);                                    
                        var resultJsonInput = $('#house-tempfiles').val();
                        if(resultJsonInput.length > 0) {
                            var resultArray = JSON.parse(resultJsonInput);
                        } else {
                            var resultArray = Array();
                        }
                        resultArray.push(index);
                        resultArray[index] = image;
                        var JsonResult = JSON.stringify(resultArray);
                        $('#house-tempfiles').val(JsonResult);
                    }
                ",
            ],
        ])->label('<span style="color:red; font-size:14px;">Фото не должен превышать 2 мб. и кол-во загружаемых фото не более '.$count . '</span>');?>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6"> 
        <?php $form3= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form3->field($modelUpload, 'anyDocuments[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/house/upload-documents']),
                'uploadExtraData' => [
                    'orderId' => $model->id,
                ],
                'maxFileCount' => $documentsCount,
                'showCaption' => false,
                'browseClass' => 'btn btn-warning',
                'browseIcon' => ' ',
                //'browseLabel' => 'Select Media',
                //'allowedFileExtensions' => ['file'],
                'maxFileSize' => 10240,
                //'maxImageWidth' => 6000,
                //'maxImageHeight' => 6000,
                //'minImageWidth' => 600,
                //'minImageHeight' => 600,
                //'dropZoneTitle' => 'Trascina i files qui...',
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => true,
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '',
                'layoutTemplates' => [
                'actionUpload' => ''
                ],
            ],
            'pluginEvents' => [
                'fileuploaded' => "
                    function(event, data, previewId, index) {                    
                        var image = {
                          name: data.files[index].name,
                          path: data.response.path,
                        };                                       
                        console.log(image);
                        console.log(index);
                        console.log(data.files[index].name);                                    
                        var resultJsonInput = $('#house-documents').val();
                        if(resultJsonInput.length > 0) {
                            var resultArray = JSON.parse(resultJsonInput);
                        } else {
                            var resultArray = Array();
                        }
                        resultArray.push(index);
                        resultArray[index] = image;
                        var JsonResult = JSON.stringify(resultArray);
                        $('#house-documents').val(JsonResult);
                    }
                ",
            ],
        ])->label('<span style="color:red; font-size:14px;">Документы не должен превышать 5 мб. и кол-во загружаемых документы не более '.$documentsCount . '</span>'); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>

<div class="row">
</div>