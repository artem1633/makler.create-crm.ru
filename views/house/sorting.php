<?php
use kartik\sortable\Sortable;
use kartik\sortinput\SortableInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="panel panel-inverse" data-sortable-id="ui-widget-1">

    <div class="panel-heading">
        <h4 class="panel-title">НАСТРОЙКА СТОЛБЦОВ </h4>
    </div>

    <div class="panel-body">

		<?php $form = ActiveForm::begin(['id' => 'date-form']); ?>

			<?php if (!Yii::$app->request->isAjax){ ?>		    	  	
			    <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-success']) ?>		    	   
			<?php } ?>
			<div class="row">
				<div class="col-sm-6">
					<label><h3 style="color:blue; font-weight:bold;"> Активно </h3></label>
					<?=SortableInput::widget([
					    'name'=>'active',
					    'items' => $active, 
					    /*[
					        1 => ['content' => 'Item # 1'],
					        2 => ['content' => 'Item # 2'],
					        3 => ['content' => 'Item # 3'],
					        4 => ['content' => 'Item # 4'],
					        5 => ['content' => 'Item # 5'],
					    ],*/
					    'hideInput' => true,
					    'sortableOptions' => [
					        'connected'=>true,
					        'itemOptions'=>['class'=>'alert alert-success'],
					    ],
					    'options' => ['class'=>'form-control', 'readonly'=>true]
					])?>
				</div>

				<div class="col-sm-6">
					<label><h3 style="color:red; font-weight:bold;"> Неактивно </h3></label>
					<?=SortableInput::widget([
					    'name'=>'no-active',
					    'items' => $noactive, /*[
					        10 => ['content' => '<div class="grid-item text-danger">Item # 10</div>'],
					        20 => ['content' => '<div class="grid-item text-danger">Item # 20</div>'],
					        30 => ['content' => '<div class="grid-item text-danger">Item # 30</div>'],
					        40 => ['content' => '<div class="grid-item text-danger">Item # 40</div>'],
					        50 => ['content' => '<div class="grid-item text-danger">Item # 50</div>'],
					    ],*/
					    'hideInput' => true,
					    'sortableOptions' => [
					        'itemOptions'=>['class'=>'alert alert-warning'],
					        'connected'=>true,
					    ],
					    'options' => ['class'=>'form-control', 'readonly'=>true]
					])
					?>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>