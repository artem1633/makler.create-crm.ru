<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id' => 'parameter-form']); ?>
   		<div class="row">         
            <div class="col-md-4">
                <?= $form->field($model, 'surname_owner')->textInput() ?>
            </div>       
            <div class="col-md-4">
                <?= $form->field($model, 'name_owner')->textInput() ?>
            </div>            
            <div class="col-md-4">
                <?= $form->field($model, 'middle_name_owner')->textInput() ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>
