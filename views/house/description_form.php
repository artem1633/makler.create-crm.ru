<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\tabs\TabsX;
use kartik\file\FileInput;
use app\models\HouseFile;
?>

<div class="box box-default"> 
    <div class="box-body" style="background-color: #ecf0f5;">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'cost')->textInput(['type' =>'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'information')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model,'zalog')->dropDownList($model->getZalogList()); ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'video')->textInput(['placeholder' => 'Введите ссылку']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>      
            </div>            
        </div>

        <!-- -->
        
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rooms_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'house_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'total_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'begin_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'end_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'room_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kitchen_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'living_area')->hiddenInput()->label(false) ?>
        <!-- Доп. параметры для Дома на длительный срок -->
        <?= $form->field($model, 'wall_material')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'distance_to_city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'land_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_floor')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'conditioner')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'camin')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'balcony')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'microwave')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'fridge')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'washer')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'wifi')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'furniture')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'digital_tv')->hiddenInput()->label(false) ?> 
        <?= $form->field($model, 'with_pets')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_childrens')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_events')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'beds_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'berth_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
        <!-- --> 
        <div class="row">
            <div class="col-md-9" style="margin-top: 5px">
                <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
       
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6"> 
        <?php $count = 20 - HouseFile::find()->where(['house_id' => $model->id])->count(); ?>
        <?php $form2= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form2->field($modelUpload, 'anyFiles[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/house/upload']),
                'uploadExtraData' => [
                    'orderId' => $model->id,
                ],
                'maxFileCount' => $count,
                'showCaption' => false,
                'browseClass' => 'btn btn-success',
                'browseIcon' => ' ',
                //'browseLabel' => 'Select Media',
                'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
                'maxFileSize' => 2048,
                'maxImageWidth' => 6000,
                'maxImageHeight' => 6000,
                //'minImageWidth' => 600,
                //'minImageHeight' => 600,
                //'dropZoneTitle' => 'Trascina i files qui...',
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => true,
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '',
                'layoutTemplates' => [
                'actionUpload' => ''
                ],
            ],
            'pluginEvents' => [
                'fileuploaded' => "
                    function(event, data, previewId, index) {                    
                        var image = {
                          name: data.files[index].name,
                          path: data.response.path,
                        };                                       
                        console.log(image);
                        console.log(index);
                        console.log(data.files[index].name);                                    
                        var resultJsonInput = $('#house-tempfiles').val();
                        if(resultJsonInput.length > 0) {
                            var resultArray = JSON.parse(resultJsonInput);
                        } else {
                            var resultArray = Array();
                        }
                        resultArray.push(index);
                        resultArray[index] = image;
                        var JsonResult = JSON.stringify(resultArray);
                        $('#house-tempfiles').val(JsonResult);
                    }
                ",
            ],
        ])->label('<span style="color:red; font-size:14px;">Фото не должен превышать 2 мб. и кол-во загружаемых фото не более 20</span>');?>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6"> 
        <?php $form3= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form3->field($modelUpload, 'anyDocuments[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/house/upload-documents']),
                'uploadExtraData' => [
                    'orderId' => $model->id,
                ],
                'maxFileCount' => 10,
                'showCaption' => false,
                'browseClass' => 'btn btn-warning',
                'browseIcon' => ' ',
                //'browseLabel' => 'Select Media',
                //'allowedFileExtensions' => ['file'],
                'maxFileSize' => 10240,
                //'maxImageWidth' => 6000,
                //'maxImageHeight' => 6000,
                //'minImageWidth' => 600,
                //'minImageHeight' => 600,
                //'dropZoneTitle' => 'Trascina i files qui...',
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => true,
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '',
                'layoutTemplates' => [
                'actionUpload' => ''
                ],
            ],
            'pluginEvents' => [
                'fileuploaded' => "
                    function(event, data, previewId, index) {                    
                        var image = {
                          name: data.files[index].name,
                          path: data.response.path,
                        };                                       
                        console.log(image);
                        console.log(index);
                        console.log(data.files[index].name);                                    
                        var resultJsonInput = $('#house-documents').val();
                        if(resultJsonInput.length > 0) {
                            var resultArray = JSON.parse(resultJsonInput);
                        } else {
                            var resultArray = Array();
                        }
                        resultArray.push(index);
                        resultArray[index] = image;
                        var JsonResult = JSON.stringify(resultArray);
                        $('#house-documents').val(JsonResult);
                    }
                ",
            ],
        ])->label('<span style="color:red; font-size:14px;">Документы не должен превышать 5 мб. и кол-во загружаемых документы не более 10</span>'); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="row">
</div>