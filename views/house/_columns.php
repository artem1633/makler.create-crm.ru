<?php
use yii\helpers\Url;
use app\models\OwnerContacts;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Columns;

$user_id = Yii::$app->user->id;
$content = 0;
$column = Columns::find()->where(['user_id' => $user_id])->one();
if($column == null) $content = 0;
else $content = 1;
$columns = new Columns();

/*$active_columns = Columns::find()->where(['user_id' => $user_id, 'status' => 1])->orderBy([ 'order_number' => SORT_ASC ])->all();
$result = [];
foreach ($variable as $key => $value) {
    # code...
}*/
//echo "id=".$data->id;die;
return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address',
        'label' => 'Адрес/Статус',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'addres/status'),
        'content' => function ($data) {
            if($data->status == 1)return '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom . '</a>';
            if($data->status == 2)return '<a data-pjax=0 class="btn btn-success btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom . '</a>';
            if($data->status == 3)return '<a data-pjax=0 class="btn btn-danger btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom . '</a>';
        },
     
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_type',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'house_type'),
        'content' => function ($data) {
            if($data->house_type == 1 | $data->house_type == 2) return 'Кв';
            if($data->house_type == 3 | $data->house_type == 4) return 'Ком';
            if($data->house_type == 5 | $data->house_type == 6) return 'Дом';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_type',
        'label' => 'Категория',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'category'),
        'content' => function ($data) {
            if($data->house_type == 1 || $data->house_type == 3 || $data->house_type == 5) return 'Длит';
            if($data->house_type == 2 || $data->house_type == 4 || $data->house_type == 6) return 'Посут';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rooms_count',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'rooms_count'),
        'label' => 'Кол-во Комнат',
        // 'content' => function ($data) {
        //     if($data->rooms_count == 0) return "Студия";
        //     else return $data->rooms_count;            
        // },
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->rooms_count, 
                $data->getRoomsCountList(), 
                [
                    'onchange'=>"
                        $.get('/house/edit-site', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:70px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'cost'),
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->cost,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:100px;',
                    'onchange'=>"$.get('/house/edit-cost', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'floor',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'floor'),
        'content' => function ($data) {
            if($data->house_type == 5 || $data->house_type == 6){
                if($data->home_floor == 6) return '5+';
                else return $data->home_floor;
            }
            else return $data->house_floor. '/'. $data->floor;            
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'public_site',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'site'),
        'label' => 'Сайт',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->public_site, 
                $data->getAvitoList(), 
                [
                    'onchange'=>"
                        $.get('/house/edit-site', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:70px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_call',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'last_call'),
        'label' => 'Последний звонок',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->last_call, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'call_back',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'call_why'),
        //'label' => 'Позвонить зачем',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => \Yii::$app->formatter->asDate($data->call_back, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/house/edit-call-back', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'purpose_call',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'purpose_call'),
        'content' => function ($data) {
            $title = substr($data->purpose_call, 0, 20) . '...';
            return '<span class="label" style="font-size:13px;">'. Html::a( $title, ['show-purpose-call','id' =>$data->id], ['title' => 'Просмотр', 'style' => 'color:#337ab7;','role'=>'modal-remote',]) . '</span>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'surname_owner',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'owner_fio'),
        'label' => 'ФИО',
        'content' => function ($data) {
            return $data->surname_owner . ' ' . $data->name_owner . ' ' . $data->middle_name_owner ;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'telephone'),
    ],

    //dop

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'home_type',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'home_type'),
        'content' => function ($data) {
            if($data->home_type == 1 ) return 'Дом';
            if($data->home_type == 2 ) return 'Дача';
            if($data->home_type == 3 ) return 'Коттедж';
            if($data->home_type == 4 ) return 'Таунхаус';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'total_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'kitchen_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'kitchen_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'living_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'living_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'room_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'room_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'home_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'home_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'land_area',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'land_area'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'conditioner',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'conditioner'),
        'content' => function ($data) {
            if($data->conditioner == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tv',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'tv'),
        'content' => function ($data) {
            if($data->tv == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'furniture',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'furniture'),
        'content' => function ($data) {
            if($data->furniture == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'washer',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'washer'),
        'content' => function ($data) {
            if($data->washer == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fridge',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'fridge'),
        'content' => function ($data) {
            if($data->fridge == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'description'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'zalog',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'zalog'),
        'content' => function ($data) {
            if($data->zalog == 0 ) return 'Без залога';
            if($data->zalog == 1 ) return '0,5 месяца';
            if($data->zalog == 2 ) return '1 месяц';
            if($data->zalog == 3 ) return '1,5 месяца';
            if($data->zalog == 4 ) return '2 месяца';
            if($data->zalog == 5 ) return '3 месяца';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'video',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'video'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'public_avito',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'public_avito'),
        'content' => function ($data) {
            if($data->public_avito == 1 ) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'wall_material',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'wall_material'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'distance_to_city',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'distance_to_city'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'metro',
        'visible'=>  $columns->getAttributeVisible($user_id,$content, 'metro'),
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного элемента?'], 
    ],

];   