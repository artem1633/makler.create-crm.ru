<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id' => 'parameter-form']); ?>
    <div class="row">
       <div class="col-md-6">
                    <?= $form->field($model, 'floor')->dropDownList($model->getFloorList(),
                        [
                            'onchange' => '
                                var a = parseInt( $("#house-house_floor").val() );
                                var floor = parseInt( $("#house-floor").val() );
                                
                                $.post( "house-floor-list?id='.'"+$(this).val(), function( data ){
                                    $( "#house-house_floor" ).html( data);
                                    if(a < floor ) { $( "#house-house_floor" ).val(a);  }
                                    else { $( "#house-house_floor" ).val(floor);  }
                                });
                            ',
                        ]
                    ); ?>
                </div>  
                <div class="col-md-6">
                    <?= $form->field($model, 'house_floor')->dropDownList($model->getHouseFloorList($model->floor)); ?>
                </div>
    </div>
<?php ActiveForm::end(); ?>
