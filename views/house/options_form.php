<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Zayavka;
use app\models\Relative;
use unclead\multipleinput\MultipleInput;
use kartik\tabs\TabsX;
?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'beds_count')->dropDownList($model->getBedsCountList()); ?>
            </div>
              <div class="col-md-2">
                <?= $form->field($model, 'berth_count')->dropDownList($model->getBertsCountList()); ?>
            </div>
        </div>
        <div class="row">
            <div style="margin-top: 30px;">
                <div class="col-md-1">
                    <?= $form->field($model, 'camin')->checkbox(); ?>
                </div>
                <div class="col-md-1">
                    <?= $form->field($model, 'balcony')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'conditioner')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'microwave')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'fridge')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'washer')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'digital_tv')->checkbox(); ?>
                </div>
            </div>
        </div>

        <div class="row">
          
            <div style="margin-top: 30px;">
                <div class="col-md-1">
                    <?= $form->field($model, 'wifi')->checkbox(); ?>
                </div>
                <div class="col-md-1">
                    <?= $form->field($model, 'tv')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'furniture')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'with_pets')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'with_childrens')->checkbox(); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'with_events')->checkbox(); ?>
                </div>
            </div>
        </div>

        <div class="form-group" >
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
        </div>    
        <!-- -->
        
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rooms_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'house_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'total_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'begin_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'end_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'room_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kitchen_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'living_area')->hiddenInput()->label(false) ?>
        <!-- Доп. параметры для Дома на длительный срок -->
        <?= $form->field($model, 'wall_material')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'distance_to_city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'land_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_floor')->hiddenInput()->label(false) ?>
        <!-- -->
        <?php ActiveForm::end(); ?>
    </div>
</div>
