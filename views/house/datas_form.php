<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm; 
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\tabs\TabsX;
//use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;

if($model->house_type == 1 || $model->house_type == 2) { $date_label = 'Когда сдана'; $house_type = 1; }
if($model->house_type == 3 || $model->house_type == 4) { $date_label = 'Когда сдана'; $house_type = 1; }
if($model->house_type == 5 || $model->house_type == 6) { $date_label = 'Когда сдан'; $house_type = 1; }
?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">            
            <div class="col-md-2">
                <?php /*$form->field($model, 'last_call')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        //'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])*/
                ?>
                <?= $form->field($model, 'last_call')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите'],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'startView'=>'year',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?php if($model->house_type == 1 || $model->house_type == 2){ ?>
                <?= $form->field($model, 'when_delivered')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите'],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'startView'=>'year',
                        'todayHighlight' => true,
                    ]
                ])->label($date_label) ?>
                <?php }?>
                 <?php if($model->house_type == 3 || $model->house_type == 4){ ?>
                <?= $form->field($model, 'when_delivered')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите'],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'startView'=>'year',
                        'todayHighlight' => true,
                    ]
                ])->label($date_label) ?>
                <?php }?>
                <?php if($model->house_type == 5 || $model->house_type == 6){ ?>
                <?= $form->field($model, 'when_delivered')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите'],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'startView'=>'year',
                        'todayHighlight' => true,
                    ]
                ])->label($date_label) ?>
                 <?php }?>
            </div>            
            <div class="col-md-2">
                <?= $form->field($model, 'call_back')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите'],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'startView'=>'year',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'public_avito')->dropDownList($model->getAvitoList(), []) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'client_id')->dropDownList($model->getClientsList(), ['prompt'=>'Выберите']) ?>
            </div>
        </div>
        <div class="row">         
            <div class="col-md-12">
                <?= $form->field($model, 'purpose_call')->textArea(['rows' => 3]) ?>
            </div>
        </div>

        <!-- -->
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rooms_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'house_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'total_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'begin_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'end_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'room_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kitchen_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'living_area')->hiddenInput()->label(false) ?>
        <!-- Доп. параметры для Дома на длительный срок -->
        <?= $form->field($model, 'wall_material')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'distance_to_city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'land_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_floor')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'conditioner')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'camin')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'balcony')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'microwave')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'fridge')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'washer')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'wifi')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'furniture')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'digital_tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_pets')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_childrens')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_events')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'beds_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'berth_count')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'cost')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'zalog')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'video')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'information')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'description')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'location_metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'address')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'dom')->hiddenInput()->label(false) ?>        
        <?= $form->field($model, 'coordinate_x')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'coordinate_y')->hiddenInput()->label(false) ?>
            
       <!--надо удалит -->  
        <?= $form->field($model, 'description_object')->hiddenInput()->label(false) ?>
        <!-- -->

        <div class="form-group">
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
        </div>
          
        <?php ActiveForm::end(); ?>
    </div>
</div>
