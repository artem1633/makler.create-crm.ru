<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            
            <div class="col-md-3">
                <?= $form->field($model, 'city')->textInput(['id' => 'street']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'location_metro')->textInput() ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'address')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'dom')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?= YandexMaps::widget([
                    'htmlOptions' => [
                        'style' => 'height: 400px;',
                    ],
                    'map' => new Map('yandex_map', [
                            'center' => [$model->coordinate_x, $model->coordinate_y],
                            'zoom' => 12,
                            'controls' => [Map::CONTROL_ZOOM],
                            'behaviors' => [Map::BEHAVIOR_DRAG],
                            'type' => "yandex#map",
                        ],
                    [
                        'objects' => [
                            new Placemark(new Point($model->coordinate_x, $model->coordinate_y), [], [
                                'draggable' => true,
                                'preset' => 'islands#dotIcon',
                                'iconColor' => 'red',
                                'events' => [
                                    'dragend' => 'js:function (e) {
                                        
                                        //console.log(e.get(\'target\').geometry.getCoordinates());                                        
                                        var coords = e.get(\'target\').geometry.getCoordinates();
                                        $( "#house-coordinate_x" ).val( coords[0]);
                                        $( "#house-coordinate_y" ).val( coords[1]);

                                        var locality = ymaps.geocode(coords, {kind: \'locality\'});
                                        var metro = ymaps.geocode(coords, {kind: \'metro\'});
                                        var street = ymaps.geocode(coords, {kind: \'street\'});
                                        var house = ymaps.geocode(coords, {kind: \'house\'});

                                        $( "#street" ).val(" ");
                                        $( "#house-location_metro" ).val(" ");
                                        $( "#house-address" ).val(" ");
                                        $( "#house-dom" ).val(" ");

                                        locality.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\');
                                                $( "#street" ).val( name);
                                            });

                                        metro.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\');
                                                $( "#house-location_metro" ).val( name);

                                            });

                                        street.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\'); 
                                                $( "#house-address" ).val( name);
                                            });

                                        house.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                //console.log(street.properties);
                                                var name = street.properties.get(\'name\');
                                                var dom_number = "";
                                                var q=0;
                                                for (var i = 0; i < name.length; i++) {
                                                    //alert(name.charAt(i));
                                                    if(q==1) dom_number += name.charAt(i);
                                                    if(name.charAt(i) == ",") q=1;
                                                    //var a = name.charCodeAt(i);
                                                }
                                                $( "#house-dom" ).val( dom_number);
                                            });                                                   
                                    }',                                
                                ]
                            ])
                        ]
                    ])
                ]) ?>

            </div>
        </div>

        <!-- -->
        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'rooms_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'house_floor')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'total_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'begin_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'end_date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'room_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kitchen_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'living_area')->hiddenInput()->label(false) ?>
        <!-- Доп. параметры для Дома на длительный срок -->
        <?= $form->field($model, 'wall_material')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'metro')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'distance_to_city')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'land_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_area')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'home_floor')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'conditioner')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'camin')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'furniture')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'balcony')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'microwave')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'fridge')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'washer')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'wifi')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'digital_tv')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_pets')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_childrens')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'with_events')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'beds_count')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'berth_count')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'cost')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'zalog')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'video')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'information')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'description')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'documents')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'coordinate_x')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'coordinate_y')->hiddenInput()->label(false) ?>
        
        <!-- -->
        <br>
        <div class="form-group">
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
        </div>
          
        <?php ActiveForm::end(); ?>
    </div>
</div>
