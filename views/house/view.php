<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\HouseFile;
use app\models\OwnerContacts;
use app\models\Users;
use app\models\ClientsHouses;

\johnitvn\ajaxcrud\CrudAsset::register($this);
\app\assets\MagnificPopupAsset::register($this);

$this->title = 'Просмотр/Изменение';
$tip = "";
$house_type = "";
if($model->house_type == 1) { $house_type = '1'; $type = 1; $tip = "Тип обьекта Квартиры на длительный срок";}
if($model->house_type == 2) { $house_type = '1'; $type = 1; $tip = "Тип обьекта Квартиры посуточно";}
if($model->house_type == 3) { $house_type = '1'; $type = 2; $tip = "Тип обьекта Комнаты на длительный срок";}
if($model->house_type == 4) { $house_type = '1'; $type = 2; $tip = "Тип обьекта Комнаты посуточно";}
if($model->house_type == 5) { $house_type = '2'; $type = 3; $tip = "Тип обьекта Дома на длительный срок";}
if($model->house_type == 6) { $house_type = '2'; $type = 3; $tip = "Тип обьекта Дома посуточно";}

$status = "";
if($model->status == 1) $status = "Неизвестно";
if($model->status == 2) $status = "Свободна";
if($model->status == 3) $status = "Занята";

?>
    
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h2 class="panel-title">
            Обьект №<?=$model->id?>
        </h2>
    </div>
    <div class="panel-body">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <h5>На длительный срок <span><?=($apartment_long + $room_long + $home_long)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 1 ? 'info' : 'default') .' btn-sm">Квартиры <span>'.$apartment_long.'</span></span>', ['/house/apartment','type' => 4], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 1 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 1], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 3 ? 'info' : 'default') .' btn-sm">Комнаты <span>'.$room_long.'</span></span>', ['/house/apartment','type' => 5], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 3 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 3], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 5 ? 'info' : 'default') .' btn-sm">Дома <span>'.$home_long.'</span></span>', ['/house/apartment','type' => 6], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 5 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 5], ['data-pjax'=>0,])?> 
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <h5>Посуточно <span><?=($apartment_short + $room_short + $home_short)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 2 ? 'info' : 'default') .' btn-sm">Квартиры <span>'.$apartment_short.'</span></span>', ['/house/apartment','type' => 7], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 2 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 2], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 4 ? 'info' : 'default') .' btn-sm">Комнаты <span>'.$room_short.'</span></span>', ['/house/apartment','type' => 8], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 4 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 4], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 6 ? 'info' : 'default') .' btn-sm">Дома <span>'.$home_short.'</span></span>', ['/house/apartment','type' => 9], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-'. ($model->house_type == 6 ? 'info' : 'default') .' btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 6], ['data-pjax'=>0,])?>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <h5>Доп. нав</h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-default btn-sm">Клиенты <span>'.$clients.'</span></span>', ['/clients'], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/clients/add'], ['role'=>'modal-remote',])?>
                        <?= Html::a('<span class="btn btn-default btn-sm">Заработано</span>', ['/site/earned'], ['data-pjax'=>0,])?>
                        <a class="btn btn-default btn-sm" href="#">Сайт</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="text-center">
                        Обьект №<?=$model->id?><br>
                        <?=$tip?>
                    </h3>
                </div>
                <div class="col-sm-6">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'status-pjax']) ?>
                        <h4 class="text-center">
                            Публикуется на сайте:<a  role="modal-remote" href="<?=Url::toRoute(['house/edit-public-site', 'id' => $model->id])?>"><b> <?=$model->public_site == 1 ? 'НЕТ' : 'ДА' ?></b></a>
                        </h4>
                        <h4 class="text-center">                        
                           Статус:<a  role="modal-remote" href="<?=Url::toRoute(['house/edit-status', 'id' => $model->id])?>"><b><?=$status?></b></a>
                        </h4>
                    <?php Pjax::end() ?>    
                    <div class="col-sm-11">
                        <div class="row">

                             <?php Pjax::begin(['enablePushState' => false, 'id' => 'location-pjax']) ?>
                                <h5><b>Местоположение</b><a class="btn btn-xs btn-primary pull-right" data-pjax=0 href="<?=Url::toRoute(['house/edit-location', 'id' => $model->id])?>">Редактировать</a></h5>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody><tr>
                                            <td style="width: 355px"><b><?=$model->getAttributeLabel('city')?></b></td>
                                            <td><?=Html::encode($model->city)?></td>
                                        </tr>
                                        <tr>
                                            <td><b><?=$model->getAttributeLabel('location_metro')?></b></td>
                                            <td><?=Html::encode($model->location_metro)?></td>
                                        </tr>
                                        <tr>
                                            <td><b><?=$model->getAttributeLabel('address')?></b></td>
                                            <td><?=Html::encode($model->address)?></td>
                                        </tr>
                                        <tr>
                                            <td><b><?=$model->getAttributeLabel('dom')?></b></td>
                                            <td><?=Html::encode($model->dom)?></td>
                                        </tr>
                                        <!-- <tr>
                                            <td><b>Посмотреть на карте</b></td>
                                            <td><a href="#">Ссылка на карту</a></td>
                                        </tr> -->
                                    </tbody></table>
                                </div>
                            <?php Pjax::end() ?>

                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'parameter-pjax']) ?>
                                <h5><b>Параметры</b><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['house/edit-parameter', 'id' => $model->id])?>">Редактировать</a></h5>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <?php if($model->house_type == 2 || $model->house_type == 4  || $model->house_type == 6) { ?>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('begin_date')?></b></td>
                                                <td><?=Html::encode(\Yii::$app->formatter->asDate($model->begin_date, 'php:d.m.Y'))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('end_date')?></b></td>
                                                <td><?=Html::encode(\Yii::$app->formatter->asDate($model->end_date, 'php:d.m.Y'))?></td>
                                            </tr>
                                        <?php } ?>

                                <?php if($house_type == 1) { ?>
                                            <tr>
                                                <td style="width: 355px"><b>
                                                    <?php 
                                                        if($model->house_type == 1 || $model->house_type == 2) echo $model->getAttributeLabel('rooms_count');
                                                        else echo 'Комнат в квартире';
                                                    ?>                                                        
                                                    </b></td>
                                                <td><?= $model->rooms_count != 0 ? Html::encode($model->rooms_count) : 'Студия' ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('type')?></b></td>
                                                <td><?=Html::encode($model->getHouseTypeName($model->type))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('floor')?></b></td>
                                                <td><?=Html::encode($model->floor)?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('house_floor')?></b></td>
                                                <td><?=Html::encode($model->house_floor)?></td>
                                            </tr>
                                        <?php if($model->house_type == 1 || $model->house_type == 2){ ?>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('total_area')?></b></td>
                                                <td><?=Html::encode($model->total_area)?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('kitchen_area')?></b></td>
                                                <td><?=Html::encode($model->kitchen_area)?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('living_area')?></b></td>
                                                <td><?=Html::encode($model->living_area)?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if($model->house_type == 3 || $model->house_type == 4){ ?>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('room_area')?></b></td>
                                                <td><?=Html::encode($model->room_area)?></td>
                                            </tr>
                                        <?php } ?> 
                                <?php }else { ?> 
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('home_type')?></b></td>
                                                <td><?=Html::encode($model->getHomeTypeName($model->home_type))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('home_floor')?></b></td>
                                                <td><?= $model->home_floor == 6 ? '5+' : Html::encode($model->home_floor)?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('wall_material')?></b></td>
                                                <td><?=Html::encode($model->getWallMaterialNameList($model->wall_material))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('distance_to_city')?></b></td>
                                                <td><?=Html::encode($model->distance_to_city)?></td>
                                            </tr>
                                             <tr>
                                                <td><b><?=$model->getAttributeLabel('home_area')?></b></td>
                                                <td><?=Html::encode($model->home_area)?></td>
                                            </tr>
                                             <tr>
                                                <td><b><?=$model->getAttributeLabel('land_area')?></b></td>
                                                <td><?=Html::encode($model->land_area)?></td>
                                            </tr>
        
                                <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            <?php Pjax::end() ?>

                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'options-pjax']) ?>
                            <h5><b>Доп параметры, опции</b><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['house/edit-options', 'id' => $model->id])?>">Редактировать</a></h5>
                            <div class="table-responsive">
                                <table class="table table-apartament-fix">
                                    <tbody>
                                        <tr>
                                            <td style="width: 250px">
                                                <b><?=$model->getAttributeLabel('beds_count')?></b>
                                                <label class="pull-right"><?= $model->beds_count == 9 ? '8+' : Html::encode($model->beds_count)?></label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('berth_count')?></b>
                                                <label class="pull-right"><?= $model->berth_count == 17 ? '16+' : Html::encode($model->berth_count)?></label>
                                            </td>
                                        </tr>                                  
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('conditioner')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->conditioner == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                             </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('wifi')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->wifi == 1 ? 'checked' : '' ?> type="checkbox"  disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('camin')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->camin == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('tv')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->tv == 1 ? 'checked' : '' ?> type="checkbox" type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               <b> <?=$model->getAttributeLabel('balcony')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->balcony == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('digital_tv')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->digital_tv == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('furniture')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->furniture == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('microwave')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->microwave == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('with_childrens')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->with_childrens == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('fridge')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->fridge == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('with_events')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->with_events == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                            <td>
                                                <b><?=$model->getAttributeLabel('washer')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->washer == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b><?=$model->getAttributeLabel('with_pets')?></b>
                                                <label class="checkbox pull-right">
                                                     <input style="margin-top: -7px;" <?= $model->with_pets == 1 ? 'checked' : '' ?> type="checkbox" disabled="disabled">
                                                 </label>
                                            </td>
                                        </tr>
                                </tbody></table>
                            </div>
                            <?php Pjax::end() ?>

                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-center"> </h4>
                            <div class="col-sm-12">
                                <div class="row">

                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'owner-pjax']) ?>
                                    <h5><b>Собственик</b><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['house/edit-owner', 'id' => $model->id])?>">Редактировать</a></h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 355px"><b><?=$model->getAttributeLabel('surname_owner')?></b></td>
                                                    <td><?=Html::encode($model->surname_owner)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('name_owner')?></b></td>
                                                    <td><?=Html::encode($model->name_owner)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('middle_name_owner')?></b></td>
                                                    <td><?=Html::encode($model->middle_name_owner)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('telephone')?></b></td>
                                                    <td><?=Html::encode($model->telephone)?></td>
                                                </tr>
                                                <?php $contacts = OwnerContacts::find()->where(['house_id'=> $model->id])->all();
                                                    foreach ($contacts as $value) {
                                                ?>
                                                <tr>
                                                    <td><b><?='Доп. '?>Телефон</b></td>
                                                    <td><?=$value->telephone?> <?=$value->contact?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php Pjax::end() ?>

                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'datas-pjax']) ?>
                                    <h5><b>Даты и Разное</b><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['house/edit-datas', 'id' => $model->id])?>">Редактировать</a></h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody><tr>
                                                <td style="width: 300px"><b><?=$model->getAttributeLabel('date_cr')?></b></td>
                                                <td><?=Html::encode(\Yii::$app->formatter->asDate($model->date_cr, 'php:H:i d.m.Y'))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('date_up')?></b></td>
                                                <td><?=Html::encode(\Yii::$app->formatter->asDate($model->date_up, 'php:H:i d.m.Y'))?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('last_call')?></b></td>
                                                <td><?= $model->last_call != null ? Html::encode(\Yii::$app->formatter->asDate($model->last_call, 'php:d.m.Y')) : '' ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('when_delivered')?></b></td>
                                                <td><?= $model->when_delivered != null ? Html::encode(\Yii::$app->formatter->asDate($model->when_delivered, 'php:d.m.Y')) : '' ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('call_back')?></b></td>
                                                <td><?= $model->call_back != null ? Html::encode(\Yii::$app->formatter->asDate($model->call_back, 'php:d.m.Y')) : '' ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('purpose_call')?></b></td>
                                                <td><?=Html::encode($model->purpose_call)?></td>
                                            </tr>
                                           <!--  <tr>
                                               <td>Завершение съёма</td>
                                               <td>09.01.2018</td>
                                           </tr> -->
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('public_avito')?></b></td>
                                                <td><b><?=$model->public_avito == 1 ? 'НЕТ' : 'ДА' ?></b></td>
                                            </tr>
                                            <!-- <tr>
                                                <td><b><?php //$model->getAttributeLabel('description_object')?></b></td>
                                                <td><?php //Html::encode($model->description_object)?></td>
                                            </tr> -->
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('client_id')?></b></td>
                                                <td>
                                                    <b>
                                                        <a href="<?=Url::toRoute(['/clients/view', 'id' => $model->client_id])?>" ><?=Html::encode($model->getClientName())?></a>
                                                    </b> 
                                                    <?php 
                                                        $client_house = ClientsHouses::find()->where(['house_id' => $model->id, 'clients_id' => $model->client_id, 'situation' => 1])->one(); 

                                                        if($model->client_id != null) 
                                                        {
                                                            echo Html::a('<span class="btn btn-default pull-right btn-xs" ><i class="fa fa-remove"></i></span>', [Url::to(['/house/remove-client', 'id' => $client_house->id, 'house_id' => $model->id ])], [
                                                                'role'=>'modal-remote', 'title'=>'Удалить',
                                                                'data-confirm'=>false, 'data-method'=>false,
                                                                'data-request-method'=>'post',
                                                                'data-confirm-title'=>'Вы уверены?',
                                                                'data-confirm-message'=>'Вы действительно хотите вычеркнуть данного клиента?'
                                                            ]);
                                                        }
                                                        else  
                                                        {
                                                            echo Html::a('<span class="btn btn-info pull-right btn-xs" ><i class="fa fa-plus"></i></span>', [Url::to(['/house/add-client', 'id' => $model->id,])], [
                                                                'role'=>'modal-remote', 'title'=>'Добавить',
                                                            ]);
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                <?php Pjax::end() ?>
 
                                <br>
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'description-pjax']) ?>
                                    <h5><b>Описание</b><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['house/edit-description', 'id' => $model->id])?>">Редактировать</a></h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 355px"><b><?=$model->getAttributeLabel('description')?></b></td>
                                                    <td><?=Html::encode($model->description)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('cost')?></b></td>
                                                    <td><?=Html::encode($model->cost)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('information')?></b></td>
                                                    <td><?=Html::encode($model->information)?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('documents')?></b></td>
                                                    <td><?=Html::a('Все документы', [Url::to(['/house/documents-view', 'id' => $model->id,])],['title'=> '', 'role'=>'modal-remote'])?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('zalog')?></b></td>
                                                    <td><?=Html::encode($model->getZalogName($model->zalog))?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?=$model->getAttributeLabel('video')?></b></td>
                                                    <td><a data-pjax=0 target="_blank" href="<?=Html::encode($model->video)?>"><?=Html::encode($model->video)?></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <?php $images = HouseFile::find()->where(['house_id' => $model->id])->all();
                                    if($images != null) echo '<h4><b>Фото  '. Html::a('<span style="font-size:16px;" class="fa fa-download"></span>', ['/house/download-images?id='.$model->id], ['title' => "Скачать все фотографии", 'data-pjax' => 0 ]). '</b></h4> <div class="col-sm-12"><div class="row"><div class="photos">';
                                        foreach ($images as $image) {
                                        $removeUrl = Url::to(['/house/remove-file', 'id' => $image->id]);
                                    ?>                                     
                                        <div class="col-md-2">
                                            <br>
                                            <img style="width: 80px; height: 60px; object-fit: cover; margin-top: 10px; cursor: pointer;" src="http://<?= $_SERVER['SERVER_NAME'].'/'.$image->path ?>" href="http://<?= $_SERVER['SERVER_NAME'].'/'.$image->path ?>">
                                            <br>
                                            <?= Html::a('<span class=" text-danger" style="font-size:13px;font-weight:bold;">&nbspУдалить<i class="fa fa-trash" ></i></span>', [$removeUrl], [
                                                'role'=>'modal-remote', 'title'=>'Удалить',
                                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                                'data-request-method'=>'post',
                                                'data-confirm-title'=>'Вы уверены?',
                                                'data-confirm-message'=>'Вы действительно хотите удалить данную картинку?'
                                            ]) ?>
                                        </div>                                  
                                    <?php  }
                                    if($images != null) echo '</div></div></div>'; ?>
                                <?php Pjax::end() ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>

<?php
if($model->isNewRecord == false){
    $galleryScript = "
    $('.photos').magnificPopup({
        delegate: 'img',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1], // Will preload 0 - before current, and 1 after the current image   
            tPrev: 'Предыдущая', // title for left button
            tNext: 'Следующая', // title for right button
            tCounter: '<span class=\"mfp-counter\">%curr% из %total%</span>' // markup of counter
        },
        image: {
            tError: '<a href=\"%url%\">Изображение #%curr%</a> не может быть загружено.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });
";
$this->registerJs($galleryScript, \yii\web\View::POS_READY);
}
?>