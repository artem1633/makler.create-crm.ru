<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> Маклер
            <small>Введите данные для авторизации</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
            </div>
            <!-- <div class="col-md-4" style=" float: right;  margin-top: 10px; ">
                <?php // Html::a('Регистрация', ['site/sign', ]);?>                
            </div> -->
            
        <div class="login-buttons">
            <?= Html::submitButton('Вход', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
        </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

