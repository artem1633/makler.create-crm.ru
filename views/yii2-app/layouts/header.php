<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 

CrudAsset::register($this);
?>
<div id="ajaxCrudDatatable">
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand"><span class="navbar-logo"></span> Makler</a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <?php // Html::a('Настройки', ['/site/tuning'], [ ])?>
                <?= Html::a('Настройки', ['/#'], [ ])?>
            </li>
            <li>
                <?php // Html::a('Инструкция', ['/site/instruksion'], [ ])?>
                <?= Html::a('Инструкция', ['/#'], [ ])?>
            </li>
            <li><?= Html::a('Профиль', 
                    ['/users/view', 'id' => Yii::$app->user->identity->id], 
                    [
                        /*'role'=>'modal-remote',*/ 'data-pjax' => 0, 'title'=>'Изменить профиля',
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                    ]
                )?>
            </li>
            <li class="dropdown navbar-user">
                <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post'] ) ?>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
