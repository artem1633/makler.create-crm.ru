<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    // ['label' => 'Рабочий стол', 'icon' => 'fa fa-desktop', 'url' => ['site/dashboard'], ],
                    ['label' => 'Квартиры', 'icon' => 'fa  fa-home', 'url' => 'javascript:;',
                        'options' => [ 'class' => 'has-sub', ],
                        'items' => [
                            ['label' => 'На длительный срок', 'url' => ['/house/apartment', 'type' => 4],],
                            ['label' => 'Посуточно ', 'url' => ['/house/apartment', 'type' => 7],],
                        ],
                    ],
                    ['label' => 'Комнаты', 'icon' => 'fa  fa-home', 'url' => 'javascript:;',
                        'options' => [ 'class' => 'has-sub', ],
                        'items' => [
                            ['label' => 'На длительный срок', 'url' => ['/house/apartment', 'type' => 5],],
                            ['label' => 'Посуточно ', 'url' => ['/house/apartment', 'type' => 8],],
                        ],
                    ],
                    ['label' => 'Дома', 'icon' => 'fa  fa-home', 'url' => 'javascript:;',
                        'options' => [ 'class' => 'has-sub', ],
                        'items' => [
                            ['label' => 'На длительный срок', 'url' => ['/house/apartment', 'type' => 6],],
                            ['label' => 'Посуточно ', 'url' => ['/house/apartment', 'type' => 9],],
                        ],
                    ],
                    //['label' => 'Квартиры', 'url' => ['/house/apartment', 'type' => 1], ],
                    //['label' => 'Комнаты', 'url' => ['/house/apartment', 'type' => 2], ],
                    //['label' => 'Дома', 'url' => ['/house/apartment', 'type' => 3], ],
                    ['label' => 'Клиенты', 'icon' => 'fa  fa-user', 'url' => ['/clients'], ],
                    ['label' => 'Заработано',  'icon' => 'fa  fa-cubes','url' => ['/site/earned'], ],
                    ['label' => 'Пользователи', 'icon' => 'fa fa-users', 'url' => ['/users/index'], ],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
