<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'money')->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' =>  'decimal',
                        'groupSeparator' => '',
                        'autoGroup' => true
                    ],
                ]) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>
