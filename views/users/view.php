<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'creation_date',
            'end_date',
            'birthday',
            'telephone',
            'email:email',
            'login',
            'password',
            'type',
            'balance',
            'license',
            'licensed_user',
        ],
    ]) ?>

</div>
