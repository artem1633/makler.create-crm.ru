<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>        
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'type')->dropDownList($model->getType(), ['prompt' => 'Выберите тип', 'id' => 'type']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'birthday')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                                'startView'=>'decade',
                            ]
                        ])
                    ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>        
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>        
            </div>
        </div>



        <div class="row" >

            <div class="col-md-6">
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-6" id="licensed_user" <?php if($model->type != 2) echo 'style=" display: none;"';?>>
                <?= $form->field($model, 'license')->dropDownList($model->getLisenceType(), ['prompt' => 'Выберите лицензию']) ?>
            </div>
            
            <div class="col-md-6" id="agent" <?php if($model->type != 3) echo 'style=" display: none;"';?> >
                <?= $form->field($model, 'licensed_user')->dropDownList($model->getLisencedUser(), ['prompt' => 'Выберите ']) ?>
            </div>
        </div>

        <div style="display: none">

            <div class="col-md-3">
                <?= $form->field($model, 'balance')->widget(\yii\widgets\MaskedInput::className(), [
                 'clientOptions' => [
                    'alias' =>  'decimal',
                    'groupSeparator' => ' ',
                    'autoGroup' => true
                ],
                ]) ?>
            </div>
            <?= $form->field($model, 'creation_date')->textInput(['value' => date('Y-m-d H:i:s')]) ?>
            <?= $form->field($model, 'end_date')->textInput() ?>
        </div>

    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; //console.log(type);
        $('#licensed_user').hide(); $('#agent').hide();
        if(type == 2) $('#licensed_user').show(); 
        if(type == 3) $('#agent').show(); 
    }
);
JS
);
?>