<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Users;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
   
        <div class="row">
            <div class="col-md-12">
                <?=$form->field($model, 'transfer')->dropDownList(
                    ArrayHelper::map(Users::find()->where(['type' => 2])->all(), 'id', 'fio'),
                    [
                        'prompt' => 'Выберите пользователя'
                    ]

                );
                ?>
            </div>
            
        </div>

    <?php ActiveForm::end(); ?>

</div>

