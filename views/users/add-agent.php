<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

?>
<div class="users-create">
    <?= $this->render('add-agent-form', [
        'model' => $model,
    ]) ?>
</div>
