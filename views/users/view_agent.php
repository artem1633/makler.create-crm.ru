<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;

$lisensed_user = Users::findOne($model->licensed_user)->fio;
$this->title = 'Изменить';
?>
<div class="row">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'admin-change-form-pjax']) ?>
        <div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <b class="panel-title" style="color:red;"> Кабинет агента </b>
        </div>
            <div class="panel-body">
                <div style="margin-top: 10px" class="table-responsive">
                    <table class="table table-bordered" style="font-size: 16px;">
                        <tbody>
                            <tr>
                                <td rowspan="3">
                                    <center><?= Html::img("@web/license_img/agent.jpg", ['style' => 'width:180px; height:180px;',])?></center>
                                </td>
                                <td style=" text-align: center;">
                                    <center>
                                        <span style="color:red;font-size: 20px;font-weight: bold;">Агент</span>
                                        <h3><?=$model->fio?></h3>
                                    </center></td>
                                <td >
                                    <center>
                                        <h3><b>Прикреплено : </b><?=$lisensed_user?></h3>
                                        <span class="label" style="font-size: 20px;">
                                            <?= Html::a('Изменить личные данные', ['update', 'id' => $model->id], ['role'=>'modal-remote',])?>
                                        </span>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Логин : </b> <?=$model->login?></td>
                                <td><b>Email : </b> <?=$model->email?></td>
                                
                            </tr>
                            <tr>
                                <td><b>Дата рождения : </b> <?=\Yii::$app->formatter->asDate($model->birthday,'php:d.m.Y')?></td>
                                <td><b>Телефон : </b> <?=$model->telephone?></td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end() ?>
</div>
