<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

?>
<div class="users-create">
    <?= $this->render('transfer_form', [
        'model' => $model,
    ]) ?>
</div>
