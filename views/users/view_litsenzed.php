<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;
use app\models\UsersSearch;
use yii\helpers\Url;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;

$this->title = 'Изменить';
CrudAsset::register($this);

$license = Users::findOne($model->id)->license;
$type = Yii::$app->user->identity->type;// echo "t=".$type;die;
$agents_count = 0;

if($license == 1) { $agents_count = 1; $img = "@web/license_img/one1.jpg"; }
if($license == 2) { $agents_count = 3; $img = "@web/license_img/trio.jpg"; }
if($license == 3) { $agents_count = 5; $img = "@web/license_img/five5.jpg"; }
if($license == 4) { $agents_count = 12; $img = "@web/license_img/pro12.jpg"; }
//if($license == 5) { $agents_count = ; $img = "@web/license_img/super_pro.jpg"; }

$diff = strtotime($model->end_date) - time();
$days = (int)($diff / 60 / 60 / 24);

if($days <= 5) $ostatok = '<span class="label label-danger" style="font-size:16px;">Лицензия закончится через <span class="badge" style="background:white;color:#ff5b57;" > '.$days.' </span> дней </span> ';
else $ostatok = '<span class="label label-primary" style="font-size:16px;">Лицензия закончится через <span class="badge" style="background:white;color:#348fe2;">'.$days.' </span> дней </span> ';
?>



<?php Pjax::begin(['enablePushState' => false, 'id' => 'admin-change-form-pjax']) ?>
<div class="row">
        <div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <b class="panel-title" style="color:red;"> Кабинет лицензиата </b>
        </div>
            <div class="panel-body">
                <div style="margin-top: 10px" class="table-responsive">
                    <table class="table table-bordered table-condensed" style="font-size: 16px;">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <center><?= Html::img($img, ['style' => 'width:180px; height:180px;',])?><br>
                                        <?=$ostatok?>
                                    </center>
                                    <br>
                                </td>
                                <td colspan="2">
                                    <center>
                                        <span style="color:red;font-size: 20px;font-weight: bold;">Лицензиат</span>
                                        <h1><?=$model->fio?></h1>
                                        <h3><?=$model->balance?>руб. <?= $type == 1 ? Html::a('Пополнить', ['add-cash', 'id' => $model->id], ['role'=>'modal-remote',]) : '' ?></h3>
                                        <h3><?= Html::a('История платежей', ['view-history', 'id' => $model->id], ['role'=>'modal-remote',])?></h3>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <center>
                                        <span class="label" style="font-size: 20px;">
                                            <?= Html::a('Изменить личные данные', ['update', 'id' => $model->id], ['role'=>'modal-remote',])?>
                                        </span>
                                    </center>
                                </td>
                                <td><b>Сдано объектов в этом месяце - </b>15</td>
                                <td><b>Сменить лицензию на:</b></td>
                                
                            </tr>
                            <tr>
                                <td><b>Логин : </b> <?=$model->login?></td>
                                <td><b>Дата рождения : </b> <?=\Yii::$app->formatter->asDate($model->birthday,'php:d.m.Y')?></td>
                                <td><b>Заработано в этом месяце - </b>170 000</td>
                                <td <?=$model->getCurrentStatus(1) ? 'class="danger"' : 'class="info"' ?> >
                                    <?=$model->getCurrentStatus(1) ? '<b>ONE (1 агент) - </b>'.$model->getMonthCost(1).' руб. / месяц ':
                                    '<b>'. Html::a('ONE', ['change-license', 'id' => $model->id, 'type' => 1], [
                                        'role'=>'modal-remote', 'title'=>'Сменить лицензию',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите cменить лицензию ?'
                                    ]). ' (1 агента)</b> - '. $model->getMonthCost(1) . ' руб. / месяц';

                                    ?>
                                </td>
                                
                            </tr>
                            <tr>
                                <td><b>Email : </b> <?=$model->email?></td>
                                <td><b>Дата создания счёта : </b><?=\Yii::$app->formatter->asDate($model->creation_date,'php:d.m.Y')?></td>
                                <td><b>Сдано объектов всего - </b>545</td>
                                <td <?=$model->getCurrentStatus(2) ? 'class="danger"' : 'class="info"' ?> >
                                    <?=$model->getCurrentStatus(2) ? '<b>TRIO (3 агент) - </b>'.$model->getMonthCost(2).' руб. / месяц' : 
                                    '<b>'. Html::a('TRIO', ['change-license', 'id' => $model->id, 'type' => 2], [
                                        'role'=>'modal-remote', 'title'=>'Сменить лицензию',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите cменить лицензию ?'
                                    ]). ' (3 агента)</b> - '. $model->getMonthCost(2) . ' руб. / месяц';
                                    ?>
                                </td>
                                
                            </tr>
                            <tr>
                                <td><b>Телефон : </b> <?=$model->telephone?></td>                                
                                <td><b>Дата окончание лицензии : </b><?=\Yii::$app->formatter->asDate($model->end_date,'php:d.m.Y')?></td>
                                <td><b>Заработано за всё время - </b>6 565 210</td>
                                <td <?=$model->getCurrentStatus(3) ? 'class="danger"' : 'class="info"' ?> >
                                    <?=$model->getCurrentStatus(3) ? '<b>FIVE (5 агент) - </b>'.$model->getMonthCost(3).' руб. / месяц ' :
                                    '<b>'. Html::a('FIVE', ['change-license', 'id' => $model->id, 'type' => 3,], [
                                        'role'=>'modal-remote', 'title'=>'Сменить лицензию',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите cменить лицензию ?'
                                    ]). ' (5 агента)</b> - '. $model->getMonthCost(3) . ' руб. / месяц';
                                    ?>
                                </td>
                                
                            </tr>
                            <tr>
                                <td><b>
                                    <?= Html::a('Передать все свои объекты', ['transfer', 'id' => $model->id], ['role'=>'modal-remote',])?>
                                </b></td>
                                <td></td>                                
                                <td></td>
                                <td <?=$model->getCurrentStatus(4) ? 'class="danger"' : 'class="info"' ?> >
                                    <?=$model->getCurrentStatus(4) ? '<b>PRO (12 агент) - </b>'.$model->getMonthCost(4).' руб. / месяц ' :
                                    '<b>'. Html::a('PRO', ['change-license', 'id' => $model->id, 'type' => 4], [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите cменить лицензию ?'
                                    ]). ' (12 агента)</b> - '. $model->getMonthCost(4) . ' руб. / месяц';
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






<?php 

    $searchModel = new UsersSearch();
    $query = Users::find()->where(['licensed_user' => $model->id]);
    $count = Users::find()->where(['licensed_user' => $model->id])->count();
    $dataProvider = new ActiveDataProvider(['query' => $query]);
    $add = 1;
    if($count >= $agents_count )$add = 0;
    //echo "add=".$add;die;

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Мои агенты</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/agents_columns.php'),
                    'panelBeforeTemplate' =>  $add ? Html::a('Добавить <i class="fa fa-plus"></i>', ['add-agent', 'id' => $model->id],
                            ['role'=>'modal-remote','title'=> 'Добавить Пользователя','class'=>'btn btn-success']) : '',
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>'',
                    ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
