<?php
use yii\helpers\Url;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        'content' => function ($data) {
            return '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['view', 'id' => $data->id]).'">'.$data->fio.'</a>';
        },
    ],
    [
        'attribute' => 'type',
        'content' => function ($data) {
            if($data->type == 1)return '<span class="label label-info" style="font-size:11px;"><b>Администратор </b></span>';
            if($data->type == 2)return '<span class="label label-warning" style="font-size:11px;"><b>Лицензиат</b></span>';
            if($data->type == 3)return '<span class="label label-success" style="font-size:11px;"><b>Агент</b></span>';;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    /*[
        'attribute' => 'creation_date',
        'content' => function ($data) {
            if($data->type == 1)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';
            if($data->type == 2)return \Yii::$app->formatter->asDate($data->creation_date, 'php:d.m.Y');
            if($data->type == 3)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';;
        },
    ],*/
    [
        'attribute' => 'end_date',
        'content' => function ($data) {
            if($data->type == 1)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';
            if($data->type == 2)return \Yii::$app->formatter->asDate($data->end_date, 'php:d.m.Y');
            if($data->type == 3)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';;
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'birthday',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'telephone',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'login',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'password',
    // ],
    [
        'attribute'=>'balance',
        'content' => function ($data) {
            if($data->type == 1)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';
            if($data->type == 2)return $data->balance;
            if($data->type == 3)return '<span class="label label-danger" style="font-size:11px;"><b>Нет</b></span>';;
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>[/*'role'=>'modal-remote',*/ 'data-pjax' => 0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного элемента?'], 
    ],

];   