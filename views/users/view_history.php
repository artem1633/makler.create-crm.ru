<table class="table table-bordered">
    <tbody>
        <tr>
            <th>Дата</th>
            <th>Сумма</th>
        </tr>
        <?php foreach ($history as $value) { ?>
           <td><?=\Yii::$app->formatter->asDate($value->data,'php:H:i, d.m.Y')?></td>
           <td><?=$value->summa?></td>
        <?php } ?>
    </tbody>
</table>