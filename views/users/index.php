<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Пользователи";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse users-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Пользователи</h4>
    </div>
    <div class="panel-body">

        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <h5>На длительный срок <span><?=($apartment_long + $room_long + $home_long)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_long.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 1], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_long.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 3], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_long.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 5], ['data-pjax'=>0,])?> 
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <h5>Посуточно <span><?=($apartment_short + $room_short + $home_short)?></span></h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_short.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 2], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_short.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 4], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_short.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 6], ['data-pjax'=>0,])?>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <h5>Доп. нав</h5>
                    <div class="form-group">
                        <?= Html::a('<span class="btn btn-default btn-sm">Клиенты <span>'.$clients.'</span></span>', ['/clients'], ['data-pjax'=>0,])?>
                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/clients/add'], ['role'=>'modal-remote',])?>
                        <?= Html::a('<span class="btn btn-default btn-sm">Заработано</span>', ['/site/earned'], ['data-pjax'=>0,])?>
                        <a class="btn btn-default btn-sm" href="#">Сайт</a>
                    </div>
                </div>
            </div>
            <br>
        </div>

        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить Пользователя','class'=>'btn btn-success']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
