<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>        
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'birthday')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                                'startView'=>'decade',
                            ]
                        ])
                    ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>        
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>        
            </div>
        </div>

        <div style="display: none">
            <?= $form->field($model, 'license')->dropDownList($model->getLisenceType(), ['prompt' => 'Выберите лицензию']) ?>                        
            <?= $form->field($model, 'licensed_user')->dropDownList($model->getLisencedUser(), ['prompt' => 'Выберите ']) ?>
            <?= $form->field($model, 'type')->dropDownList($model->getType(), ['prompt' => 'Выберите тип', 'id' => 'type']) ?>            
            <?= $form->field($model, 'balance')->widget(\yii\widgets\MaskedInput::className(), [
             'clientOptions' => [
                'alias' =>  'decimal',
                'groupSeparator' => ' ',
                'autoGroup' => true
            ],
            ]) ?>            
            <?= $form->field($model, 'creation_date')->textInput(['value' => date('Y-m-d')]) ?>
            <?= $form->field($model, 'end_date')->textInput() ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>
