<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>        
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'birthday')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                                'startView'=>'decade',
                            ]
                        ])
                    ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>        
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>        
            </div>  

        </div>

        <div style="display: none">

            <div class="col-md-3">
                <?= $form->field($model, 'balance')->widget(\yii\widgets\MaskedInput::className(), [
                 'clientOptions' => [
                    'alias' =>  'decimal',
                    'groupSeparator' => ' ',
                    'autoGroup' => true
                ],
                ]) ?>
            </div>
            <?= $form->field($model, 'creation_date')->textInput(['value' => date('Y-m-d')]) ?>
            <?= $form->field($model, 'end_date')->textInput() ?>
        </div>

    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
