<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;
$this->title = 'Изменить';
?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'admin-change-form-pjax']) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <b class="panel-title" style="color:red;"> Кабинет администратора </b>
                </div>
                <div class="panel-body">
                    <div style="margin-top: 10px" class="table-responsive">
                        <table class="table table-bordered" style="font-size: 16px;">
                            <tbody>
                                <tr>
                                    <td colspan="2"><center><?= Html::img("@web/license_img/architect.jpg", ['style' => 'width:180px; height:180px;',])?></center></td>
                                    <td colspan="3">
                                        <center>
                                            <br>
                                            <span style="color:red;font-size: 20px;font-weight: bold;">Архитектор</span><br>
                                            <h1><?=$model->fio?></h1>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <center>
                                            <span class="label" style="font-size: 20px;">
                                                <?= Html::a('Изменить личные данные', ['update', 'id' => $model->id], ['role'=>'modal-remote',])?>
                                            </span>
                                        </center>
                                    </td>
                                    <td style=" text-align: center;"><span class="label label-info" style="font-size:16px;">СЕГОДНЯ</span></td>
                                    <td style=" text-align: center;"><span class="label label-danger" style="font-size:16px;">ЗА МЕСЯЦ</span></td>
                                    <td style=" text-align: center;"><span class="label label-success" style="font-size:16px;">ВСЕГО</span></td>
                                </tr>
                                <tr>
                                    <td><b>Логин : </b> <?=$model->login?></td>
                                    <td><b>Email : </b> <?=$model->email?></td>
                                    <td><b>Регистраций - </b><?=$today_registration?></td>
                                    <td><b>Регистраций - </b><?=$month_registration?></td>
                                    <td><b>Регистраций - </b><?=$all_registration?></td>
                                </tr>
                                <tr>
                                    <td><b>Дата рождения : </b> <?=\Yii::$app->formatter->asDate($model->birthday,'php:d.m.Y')?></td>
                                    <td><b>Телефон : </b> <?=$model->telephone?></td>
                                    <td><b>Куплено лицензий - </b><?=$today_purchased?></td>
                                    <td><b>Куплено лицензий - </b><?=$month_purchased?></td>
                                    <td><b>Куплено лицензий - </b><?=$all_purchased?></td>
                                </tr>
                                <tr>
                                    <td><b><?= Html::a('Регистрации', ['view', 'id' => $model->id,'registration' => 1], ['style'=>'color:red;', 'onclick' =>'window.location.href = "/users/view?id='.$model->id.'&registration="+1;' ])?>
                                    </b></td>
                                    <td><b><?= Html::a('Лицензиаты', ['view', 'id' => $model->id, 'litsenzed' => 1,], ['style'=>'color:red;', 'onclick' =>'window.location.href = "/users/view?id='.$model->id.'&litsenzed="+1;' ])?></b></td>
                                    <td><b>Приход  <span class=" fa fa-rub"></span> - </b><?=$today_coming == null ? 0 : $today_coming?></td>
                                    <td><b>Приход  <span class=" fa fa-rub"></span> - </b><?=$month_coming == null ? 0 : $month_coming?></td>
                                    <td><b>Приход  <span class=" fa fa-rub"></span> - </b><?=$all_coming == null ? 0 : $all_coming?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($litsenzed == 1) {?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <b class="panel-title" style="color:white;"> Лицензиаты </b>
                    </div>
                    <div class="panel-body">
                        <div style="margin-top: 10px" class="table-responsive">
                            <table class="table table-bordered" style="font-size: 16px;">
                                <tr>
                                    <th><center>Год</center></th>
                                    <th colspan="12"><center>Месяц</center></th>
                                    <th><center>Всего</center></th>
                                </tr>
                                <?php for ($statistik_year = $min_litsenzed; $statistik_year <= $max_litsenzed; $statistik_year++) { ?>
                                    <tr>
                                        <td><center><b><?=$statistik_year?> год</b></center></td>
                                        <?php for ($months = 1; $months <= 12; $months++) { 

                                            $data_begin = date('Y-m-d H:i:s', mktime(0, 0, 0, $months, 1, $statistik_year));
                                            $data_end = date('Y-m-d H:i:s', mktime(0, 0, 0, $months + 1 , 1, $statistik_year));
                                            $result = Users::find()->where(['between', 'creation_date', $data_begin , $data_end ])->andwhere(['type' => '2'])->count();

                                            $data_begin = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, $statistik_year));
                                            $data_end = date('Y-m-d H:i:s', mktime(0, 0, 0, 1 , 1, $statistik_year + 1));
                                            $total_result = Users::find()->where(['between', 'creation_date', $data_begin , $data_end ])->andwhere(['type' => '2'])->count();
                                        ?>
                                        <td><center><?= Html::a('<span class="label label-info" style="font-size:10px;">'.$model->getMonth($months).'</span>', ['view', 'id' => $model->id, 'litsenzed' => 1, 'year' => $statistik_year, 'month' => $months], ['onclick' =>'window.location.href = "/users/view?id='.$model->id.'&year='.$statistik_year.'&month='.$months.'&litsenzed="+1;' ])?><br><?=$result?></center></td>
                                    <?php } ?>
                                        <td><center><b><?=$total_result?></b></center></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="13"><b> Итого :</b></td>
                                    <td><center><b><?=$all_litsenzed?></b></center></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if($year != null) {?>
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <b class="panel-title" style="color:white;font-size: 20px;"> <center> <?=$model->getMonth($month)?> <?=$year?> </center></b>
                        </div>
                        <div class="panel-body">
                            <div style="margin-top: 10px" class="table-responsive">
                                <table class="table table-bordered" style="font-size: 16px;">
                                    <tr>
                                        <th><center>Дата</center></th>
                                        <th colspan="12"><center>Количество </center></th>
                                    </tr>
                                    <?php 
                                        $data_begin = mktime(0, 0, 0, $month, 1, $year);
                                        $data_end = mktime(0, 0, 0, $month + 1 , 1, $year);
                                        $total = 0;
                                        //if($day == null) $day = mktime(0, 0, 0, $month, 1, $year);
                                    for ($begin = $data_begin; $begin < $data_end; $begin = $begin + 86400) { 
                                       
                                        $results = Users::find()->where(['between', 'creation_date', date('Y-m-d H:i:s', $begin) , date('Y-m-d H:i:s', ($begin+ 86400)) ])->andwhere(['type' => '2'])->count();
                                        $total += $results;
                                        ?>
                                        <tr>
                                            <td><center><b><?=date('d.m.Y', $begin)?></b></center></td>
                                            <td><center><?= Html::a('<span class="label label-danger" style="font-size:16px;">'.$results.'</span>', ['view', 'id' => $model->id, 'litsenzed' => 1, 'year' => $year, 'month' => $month, 'day' => $begin], ['onclick' =>'window.location.href = "/users/view?id='.$model->id.'&year='.$year.'&month='.$month.'&day='.$begin.'&litsenzed="+1;' ])?></center></td>
                                            
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="2"><center>Регистраций за  <?=$model->getMonth($month)?> <?=$year?> <br><b style="font-size:24px;"><?=$total?></b></center></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($day != null) { ?>
                <div class="col-md-7">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <b class="panel-title" style="color:white;font-size: 20px;"> <center> Лицензиаты <?= date('d.m.Y', $day)?> </center></b>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 16px;">
                                <tr>
                                    <th><center>№</center></th>
                                    <th><center>ФИО</center></th>
                                    <th colspan="12"><center>Время</center></th>
                                </tr>
                                <?php 
                                    $count = Users::find()->where(['between', 'creation_date', date('Y-m-d H:i:s', $day) , date('Y-m-d H:i:s', ($day+ 86400)) ])->andwhere(['type' => '2'])->count();
                                    $results = Users::find()->where(['between', 'creation_date', date('Y-m-d H:i:s', $day) , date('Y-m-d H:i:s', ($day+ 86400)) ])->andwhere(['type' => '2'])->all();
                                    $i=0;
                                    foreach ($results as $value) {
                                   
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><center><?=$i?></center></td>
                                        <td><center><b><?= Html::a('<span class="label label-primary" style="font-size:16px;">'.$value->fio.'</span>', ['view', 'id' => $value->id], ['data-pjax' => 0,])?></b></center></td>
                                        <td><center><?=date('H:i', strtotime($value->creation_date))?></center></td>
                                        
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3"><center>Регистраций за <?= date('d.m.Y', $day)?> <br><b style="font-size:24px;"><?=$count?></b></center></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        <?php } ?>

    <?php } ?>

<?php Pjax::end() ?>