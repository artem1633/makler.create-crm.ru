<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\ClientsHouses;
use app\models\OwnerContacts;

$tip = "";
if($model->type == 1) $tip = "Квартира";
if($model->type == 2) $tip = "Комната";
if($model->type == 3) $tip = "Дом";
$category = "";
if($model->category == 1) $category = "Длительный срок";
if($model->category == 2) $category = "Посуточно";

?>
<div class="row">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h2 class="panel-title">
                Обьект №<?=$id?>
            </h2>
        </div>
        <div class="panel-body">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <h5>На длительный срок <span><?=($apartment_long + $room_long + $home_long)?></span></h5>
                        <div class="form-group">
                            <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_long.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 1], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_long.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 3], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_long.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 5], ['data-pjax'=>0,])?> 
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <h5>Посуточно <span><?=($apartment_short + $room_short + $home_short)?></span></h5>
                        <div class="form-group">
                            <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_short.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?>
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 2], ['data-pjax'=>0,])?>
                            <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_short.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?>
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 4], ['data-pjax'=>0,])?>
                            <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_short.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
                            <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 6], ['data-pjax'=>0,])?>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <h5>Доп. нав</h5>
                        <div class="form-group">
                            <?= Html::a('<span class="btn btn-info btn-sm">Клиенты <span>'.$clients.'</span></span>', ['/clients'], ['data-pjax'=>0,])?>
                            <?= Html::a('<span class="btn btn-info btn-sm"><i class="fa fa-plus"></i></span>', ['/clients/add'], ['role'=>'modal-remote',])?>
                            <?= Html::a('<span class="btn btn-default btn-sm">Заработано</span>', ['/site/earned'], ['data-pjax'=>0,])?>
                            <a class="btn btn-default btn-sm" href="#">Сайт</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="text-center">
                            Карточка клиента №<?=$id?><br>
                            <?=$model->fio?>
                        </h3>
                    </div>
                    <div class="col-sm-12 text-center">
                        <?= Html::a('Подобрать вариант', ['/clients/search', 'client_id' => $model->id], ['role'=>'modal-remote', 'class' => 'btn btn-primary ', 'style' => 'margin-bottom: 15px;'])?>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'base-pjax']) ?>
                            <div class="row">
                                <div class="table-responsive">
                                    <h5 class="text-right"><a class="btn btn-xs btn-primary pull-right" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>">Редактировать</a></h5>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td style="width: 250px"><b><?=$model->getAttributeLabel('fio')?></b></td>
                                                <td><?=$model->fio?></td>
                                                <td><b><?=$model->getAttributeLabel('status')?></b></td>
                                                <td><?=$model->status == 1 ? 'Ищет' : 'Снимает ' ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('telephone')?></b></td>
                                                <td><?=$model->telephone?></td>
                                                <td><b><?=$model->getAttributeLabel('date_cr')?></b></td>
                                                <td><?=\Yii::$app->formatter->asDate($model->date_cr, 'php:d.m.Y')?></td>
                                            </tr>                                        
                                            <tr>
                                                <td style="width: 250px"><b><?=$model->getAttributeLabel('type')?></b></td>
                                                <td><?=$tip?></td>
                                                <td><b><?=$model->getAttributeLabel('category')?></b></td>
                                                <td><?=$category?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('room_count')?></b></td>
                                                <td><?=$model->room_count == 0 ? 'Студия' : $model->room_count ?></td>
                                                <td><b><?=$model->getAttributeLabel('budjet')?></b></td>
                                                <td><?=$model->budjet?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('mebel')?></b></td>
                                                <td><?=$model->mebel == 1 ? 'Да' : 'Нет' ?></td>
                                                <td><b><?=$model->getAttributeLabel('floor')?></b></td>
                                                <td><?=$model->floor == 10 ? ">9" : $model->floor ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('last_call')?></b></td>
                                                <td><?=\Yii::$app->formatter->asDate($model->last_call, 'php:d.m.Y')?></td>
                                                <td><b><?=$model->getAttributeLabel('why_call')?></b></td>
                                                <td><?=\Yii::$app->formatter->asDate($model->why_call, 'php:d.m.Y')?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('comment')?></b></td>
                                                <td colspan="3"><?=$model->comment?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('why_comment')?></b></td>
                                                <td colspan="3"><?=$model->why_comment?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>
                    <?php 
                        $clients_house = ClientsHouses::find()->where(['situation' => 1, 'client_id' => $model->id])->all();
                        $clients_house_end = ClientsHouses::find()->where(['situation' => 2, 'client_id' => $model->id])->all();
                    ?>

                    <div class="col-sm-12">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'houses-pjax']) ?>
                        <h4>Список квартир которые снимает клиент</h4>
                        <div class="table-responsive">
                            <table class="table table-fix table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Адрес/Статус</th>
                                        <th>Тип</th>
                                        <th>Категория</th>
                                        <th>Кол-во<br>Комнат</th>
                                        <th>Цена</th>
                                        <th>Этаж</th>
                                        <th>Сайт</th>
                                        <th>Заверш.<br>сьема</th>
                                        <th>Комиссия</th>
                                        <th>Собственик</th>
                                        <th>Телефон</th>
                                        <th>Комментарий</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clients_house as $house) { ?>                                   
                                    <tr>
                                        <td><?=$house->house_id?></td>
                                        <td>
                                            <?php
                                            if($house->status == 1) echo '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            if($house->status == 2) echo '<a data-pjax=0 class="btn btn-success btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            if($house->status == 3) echo '<a data-pjax=0 class="btn btn-danger btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($house->house_type == 1 | $house->house_type == 2) echo 'Кв';
                                                if($house->house_type == 3 | $house->house_type == 4) echo 'Ком';
                                                if($house->house_type == 5 | $house->house_type == 6) echo 'Дом';
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($house->house_type == 1 || $house->house_type == 3 || $house->house_type == 5) echo 'Длит';
                                                if($house->house_type == 2 || $house->house_type == 4 || $house->house_type == 6) echo 'Посут';
                                            ?>
                                        </td>
                                        <td><?=$house->rooms_count?></td>
                                        <td><?=$house->cost?></td>
                                        <td>
                                            <?php
                                                if($house->house_type == 5 || $house->house_type == 6){
                                                    if($house->home_floor == 6) echo '5+';
                                                    else echo $house->home_floor;
                                                }
                                                else echo $house->house_floor. '/'. $house->floor;        
                                            ?>
                                        </td>
                                        <td <?= $house->public_avito == 1 ? 'class = "danger"' : 'class = "success"' ?> >
                                            <?= $house->public_avito == 1 ? "Нет" : "Да" ?>
                                        </td>
                                        <td><?=\Yii::$app->formatter->asDate($house->end_date, 'php:d.m.Y')?></td>
                                        <td>
                                            <?php if($house->commission == null) 
                                                    echo \yii\widgets\MaskedInput::widget([
                                                        'name' => 'house',
                                                        'value' => $house->commission,
                                                        'mask' => '9',
                                                        'options' => [
                                                            'class' =>'form-control',
                                                            'style' => 'width:80px;',
                                                            'onchange'=>"$.get('/clients/change-commission', {'id':$house->id, 'value':$(this).val()}, function(data){ 
                                                            } ); 
                                                            ",
                                                        ],
                                                        'clientOptions' => ['repeat' => 10, 'greedy' => false]
                                                    ]); 
                                                else echo $house->commission;
                                            ?>
                                        </td>
                                        <td><?= $house->surname_owner. ' ' . $house->name_owner. ' ' .$house->middle_name_owner ?></td>
                                        <td>
                                            <?php 
                                                $contact = OwnerContacts::find()->where(['house_id'=> $house->house_id])->one();
                                                if($contact != null) echo $contact->telephone;
                                            ?>
                                        </td>
                                        <td><?=$house->commentary?></td>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                        <?php Pjax::end() ?>
                        <h4>Список квартир в которых проживал клиент</h4>
                        <div class="table-responsive">
                            <table class="table table-fix table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Адрес/Статус</th>
                                        <th>Тип</th>
                                        <th>Категория</th>
                                        <th>Кол-во<br>Комнат</th>
                                        <th>Цена</th>
                                        <th>Этаж</th>
                                        <th>Сайт</th>
                                        <th>Заверш.<br>сьема</th>
                                        <th>Комиссия</th>
                                        <th>Собственик</th>
                                        <th>Телефон</th>
                                        <th>Причина сьезда</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clients_house_end as $house) { ?>                                   
                                    <tr>
                                        <td><?=$house->house_id?></td>
                                        <td>
                                            <?php
                                            if($house->status == 1) echo '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            if($house->status == 2) echo '<a data-pjax=0 class="btn btn-success btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            if($house->status == 3) echo '<a data-pjax=0 class="btn btn-danger btn-sm" href="'.Url::toRoute(['house/view', 'id' => $house->house_id]).'">'.$house->address.'</a>';
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($house->house_type == 1 | $house->house_type == 2) echo 'Кв';
                                                if($house->house_type == 3 | $house->house_type == 4) echo 'Ком';
                                                if($house->house_type == 5 | $house->house_type == 6) echo 'Дом';
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($house->house_type == 1 || $house->house_type == 3 || $house->house_type == 5) echo 'Длит';
                                                if($house->house_type == 2 || $house->house_type == 4 || $house->house_type == 6) echo 'Посут';
                                            ?>
                                        </td>
                                        <td><?=$house->rooms_count?></td>
                                        <td><?=$house->cost?></td>
                                        <td>
                                            <?php
                                                if($house->house_type == 5 || $house->house_type == 6){
                                                    if($house->home_floor == 6) echo '5+';
                                                    else echo $house->home_floor;
                                                }
                                                else echo $house->house_floor. '/'. $house->floor;        
                                            ?>
                                        </td>
                                        <td <?= $house->public_avito == 1 ? 'class = "danger"' : 'class = "success"' ?> >
                                            <?= $house->public_avito == 1 ? "Нет" : "Да" ?>
                                        </td>
                                        <td><?=\Yii::$app->formatter->asDate($house->end_date, 'php:d.m.Y')?></td>
                                        <td><?= $house->commission?></td>
                                        <td><?= $house->surname_owner. ' ' . $house->name_owner. ' ' .$house->middle_name_owner ?></td>
                                        <td>
                                            <?php 
                                                $contact = OwnerContacts::find()->where(['house_id'=> $house->house_id])->one();
                                                if($contact != null) echo $contact->telephone;
                                            ?>
                                        </td>
                                        <td><?=$house->commentary?></td>
                                    </tr>
                                <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- begin col-3 -->
</div>
