<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>                
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'type')->dropDownList($model->getType(), []) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'category')->dropDownList($model->getCategory(), []) ?>
            </div>            
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'room_count')->dropDownList($model->getRoomsCountList(), []) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'budjet')->textInput(['type' => 'number']) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'mebel')->dropDownList($model->getMebel(), []) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'floor')->dropDownList($model->getFloorList(), []) ?>
            </div>            
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'last_call')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])
                ?>
                <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'comment')->textArea(['rows' => 5]) ?>
            </div>
            <!-- <div class="col-md-4">
               </div>         -->   
        </div>

        <div class="row">
            <br>
            <div class="col-md-4">
                <?= $form->field($model, 'why_call')->widget(
                    DatePicker::className(), [
                        'inline' => false,
                        'language' => 'ru',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'startView'=>'year',
                        ]
                    ])
                ?>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'why_comment')->textArea(['rows' => 1]) ?>
            </div>
                      
        </div>

        <div style="display: none;">
            <?= $form->field($model, 'status')->textInput(['value' => '1']) ?>
        </div>

    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
