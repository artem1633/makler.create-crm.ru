<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Clients */

?>
<div class="clients-createnew">
    <?= $this->render('_form_new', [
        'model' => $model,
    ]) ?>
</div>
