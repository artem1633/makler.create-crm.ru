<?php
use yii\helpers\Url;
use app\models\OwnerContacts;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address',
        'label' => 'Адрес/Статус',
        'content' => function ($data) {
            if($data->status == 1)return '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom .'</a>';
            if($data->status == 2)return '<a data-pjax=0 class="btn btn-success btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom .'</a>';
            if($data->status == 3)return '<a data-pjax=0 class="btn btn-danger btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom .'</a>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_type',
        'content' => function ($data) {
            if($data->house_type == 1 | $data->house_type == 2) return 'Кв';
            if($data->house_type == 3 | $data->house_type == 4) return 'Ком';
            if($data->house_type == 5 | $data->house_type == 6) return 'Дом';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_type',
        'label' => 'Категория',
        'content' => function ($data) {
            if($data->house_type == 1 || $data->house_type == 3 || $data->house_type == 5) return 'Длит';
            if($data->house_type == 2 || $data->house_type == 4 || $data->house_type == 6) return 'Посут';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rooms_count',
        'label' => 'Кол-во Комнат',
        'content' => function ($data) {
            if($data->rooms_count == 0) return "Студия";
            else return $data->rooms_count;            
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'floor',
        'content' => function ($data) {
            if($data->house_type == 5 || $data->house_type == 6){
                if($data->home_floor == 6) return '5+';
                else return $data->home_floor;
            }
            else return $data->house_floor. '/'. $data->floor;            
        },
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'public_avito',
        'label' => 'Сайт',
        'content' => function ($data) {
            if($data->public_avito == 1) return 'Нет';
            else return 'Да';
        },
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        /*'content' => function ($data) {
            $contacts = OwnerContacts::find()->where(['house_id'=> $data->id])->one();
            return $contacts->telephone;
        },*/
    ],
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
];   