<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;


$this->title = 'Список комнаты';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $houses_dataProvider,
            'pjax'=>true,
            'columns' => require(__DIR__.'/house_columns.php'),
            'toolbar'=> [
                ['content'=> '',],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'summary' => false,
            'panel' => [
                'type' => 'primary', 
                'headingOptions' => "",
                'footerOptions' => "",
                /*'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-check"></i>&nbsp; Выбрать',
                                ["bulk-select", 'client_id' => $model->id] ,
                                [
                                    "class"=>"btn btn-info btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Подтвердите действие',
                                    'data-confirm-message'=>'Вы уверены что хотите добавить эти элементы?'
                                ]),
                        ]).                        
                        '<div class="clearfix"></div>',*/
            ]
        ])?>
    </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    "options" => [
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>