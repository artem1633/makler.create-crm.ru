<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    /*[
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    /*[
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        'content' => function ($data) {
            if($data->status == 1)return '<a data-pjax=0 class="btn btn-default btn-sm" href="'.Url::toRoute(['view', 'id' => $data->id]).'">'.$data->fio.'</a>';
            if($data->status == 2)return '<a data-pjax=0 class="btn btn-success btn-sm" href="'.Url::toRoute(['view', 'id' => $data->id]).'">'.$data->fio.'</a>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->type, 
                $data->getType(), 
                [
                    'onchange'=>"
                        $.get('/clients/change-type', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:100px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->category, 
                $data->getCategory(), 
                [
                    'onchange'=>"
                        $.get('/clients/change-category', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:150px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'room_count',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->room_count, 
                $data->getRoomsCountList(), 
                [
                    'onchange'=>"
                        $.get('/clients/change-room-count', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:100px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'budjet',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->budjet,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:80px;',
                    'onchange'=>"$.get('/clients/change-budjet', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mebel',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->mebel, 
                $data->getMebel(), 
                [
                    'onchange'=>"
                        $.get('/clients/change-mebel', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:70px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'floor',
        'content' => function ($data) {
            return Html::dropDownList(
                'data', 
                $data->floor, 
                $data->getFloorList(), 
                [
                    'onchange'=>"
                        $.get('/clients/change-floor', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:70px;'
                ]
            );
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_call',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => \Yii::$app->formatter->asDate($data->last_call, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/clients/change-last-call', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'why_call',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => \Yii::$app->formatter->asDate($data->why_call, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:100px;',
                    'onchange'=>"$.get('/clients/change-why-call', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        'content' => function ($data) {
            return Html::input('string', $data, $data->telephone, [
                'name' => $data,
                'value' => $data->telephone,
                'class' => 'form-control',
                'style' => 'width:140px;',
                'onchange'=>"$.get('/clients/change-telephone', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                } );
                ",
            ]);
        },
    ],
   /* [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
    ],*/
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'why_comment',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'comment',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_cr',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данный элемент?'], 
    ],

];   