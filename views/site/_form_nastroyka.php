<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

?>

<div class="panel panel-inverse users-index">
    <div class="panel-heading">
        <h2 class="panel-title">
            Местоположение
        </h2>
    </div>   
    <div class="panel-body">
<?php $form = ActiveForm::begin(['id' => 'location-form']); ?>
    <div class="col-xs-12">
        <div class="row">            
            <div class="col-md-3">
                <?= $form->field($model, 'city')->textInput([/*'disabled' => true,*/ 'id' => 'street']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'location_metro')->textInput([/*'disabled' => true,*/ ]) ?>
            </div>            
            <div class="col-md-3">
                <?= $form->field($model, 'address')->textInput([/*'disabled' => true,*/ ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'dom')->textInput([/*'disabled' => true,*/ ]) ?>
            </div>
            <?= $form->field($model, 'coordinate_x')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'coordinate_y')->hiddenInput()->label(false) ?>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?= YandexMaps::widget([
                    'htmlOptions' => [
                        'style' => 'height: 400px;',
                    ],
                    'map' => new Map('yandex_map', [
                            'center' => [$model->coordinate_x, $model->coordinate_y],
                            'zoom' => 12,
                            'controls' => [Map::CONTROL_ZOOM],
                            'behaviors' => [Map::BEHAVIOR_DRAG],
                            'type' => "yandex#map",
                        ],
                    [
                        'objects' => [
                            new Placemark(new Point($model->coordinate_x, $model->coordinate_y), [], [
                                'draggable' => true,
                                'preset' => 'islands#dotIcon',
                                'iconColor' => 'red',
                                'events' => [
                                    'dragend' => 'js:function (e) {
                                        
                                        //console.log(e.get(\'target\').geometry.getCoordinates());                                        
                                        var coords = e.get(\'target\').geometry.getCoordinates();
                                        $( "#nastroyka-coordinate_x" ).val( coords[0]);
                                        $( "#nastroyka-coordinate_y" ).val( coords[1]);

                                        var locality = ymaps.geocode(coords, {kind: \'locality\'});
                                        var metro = ymaps.geocode(coords, {kind: \'metro\'});
                                        var street = ymaps.geocode(coords, {kind: \'street\'});
                                        var house = ymaps.geocode(coords, {kind: \'house\'});

                                        $( "#street" ).val(" ");
                                        $( "#nastroyka-location_metro" ).val(" ");
                                        $( "#nastroyka-address" ).val(" ");
                                        $( "#nastroyka-dom" ).val(" ");

                                        locality.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\');
                                                $( "#street" ).val( name);
                                            });

                                        metro.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\');
                                                $( "#nastroyka-location_metro" ).val( name);

                                            });

                                        street.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                var name = street.properties.get(\'name\'); 
                                                $( "#nastroyka-address" ).val( name);
                                            });

                                        house.then(
                                            function (res) {
                                                var street = res.geoObjects.get(0);
                                                //console.log(street.properties);
                                                var name = street.properties.get(\'name\');
                                                var dom_number = "";
                                                var q=0;
                                                for (var i = 0; i < name.length; i++) {
                                                    //alert(name.charAt(i));
                                                    if(q==1) dom_number += name.charAt(i);
                                                    if(name.charAt(i) == ",") q=1;
                                                    //var a = name.charCodeAt(i);
                                                }
                                                $( "#nastroyka-dom" ).val( dom_number);
                                            });                                                   
                                    }',                                
                                ]
                            ])
                        ]
                    ])
                ]) ?>

            </div>
        </div>

        <br>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
