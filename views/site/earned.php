<?php
use yii\helpers\Html;
use app\models\ClientsHouses;
use app\models\House;

?>


<div class="row">
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<h2 class="panel-title">
				Отчет по заработкам
			</h2>
		</div>
		<div class="panel-body">
			        
			<div class="col-xs-12">
	            <div class="row">
	                <div class="col-sm-6 col-lg-4">
	                    <h5>На длительный срок <span><?=($apartment_long + $room_long + $home_long)?></span></h5>
	                    <div class="form-group">
	                        <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_long.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 1], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_long.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 3], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_long.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 5], ['data-pjax'=>0,])?> 
	                    </div>
	                </div>
	                <div class="col-sm-6 col-lg-4">
	                    <h5>Посуточно <span><?=($apartment_short + $room_short + $home_short)?></span></h5>
	                    <div class="form-group">
	                        <?= Html::a('<span class="btn btn-default btn-sm">Квартиры <span>'.$apartment_short.'</span></span>', ['/house/apartment','type' => 1], ['data-pjax'=>0,])?>
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 2], ['data-pjax'=>0,])?>
	                        <?= Html::a('<span class="btn btn-default btn-sm">Комнаты <span>'.$room_short.'</span></span>', ['/house/apartment','type' => 2], ['data-pjax'=>0,])?>
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 4], ['data-pjax'=>0,])?>
	                        <?= Html::a('<span class="btn btn-default btn-sm">Дома <span>'.$home_short.'</span></span>', ['/house/apartment','type' => 3], ['data-pjax'=>0,])?> 
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/house/apartment-create', 'type' => 6], ['data-pjax'=>0,])?>
	                    </div>
	                </div>
	                <div class="col-sm-4 col-md-3">
	                    <h5>Доп. нав</h5>
	                    <div class="form-group">
	                        <?= Html::a('<span class="btn btn-default btn-sm">Клиенты <span>'.$clients.'</span></span>', ['/clients'], ['data-pjax'=>0,])?>
	                        <?= Html::a('<span class="btn btn-default btn-sm"><i class="fa fa-plus"></i></span>', ['/clients/add'], ['role'=>'modal-remote',])?>
	                        <?= Html::a('<span class="btn btn-info btn-sm">Заработано</span>', ['/site/earned'], ['data-pjax'=>0,])?>
	                        <a class="btn btn-default btn-sm" href="#">Сайт</a>
	                    </div>
	                </div>
	            </div>
	        </div>


			<div class="col-xs-12">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th class="text-center">Год</th>
								<th colspan="12" class="text-center">Месяц</th>
								<th class="text-center" >Доход</th>
							</tr>
						</thead>
						<tbody>				
						<?php $all_sum = 0; for ($statistik_year = $min_litsenzed; $statistik_year <= $max_litsenzed; $statistik_year++) { ?>
                            <tr>
                                <td><center><b><?=$statistik_year?> год</b></center></td>
                                <?php for ($months = 1; $months <= 12; $months++) { 

	                                $data_begin = date('Y-m-d H:i:s', mktime(0, 0, 0, $months, 1, $statistik_year));
	                                $data_end = date('Y-m-d H:i:s', mktime(0, 0, 0, $months + 1 , 1, $statistik_year));
	                                $result = ClientsHouses::find()->where(['between', 'date_add_commission', $data_begin , $data_end ])->count();

	                                $data_begin = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, $statistik_year));
	                                $data_end = date('Y-m-d H:i:s', mktime(0, 0, 0, 1 , 1, $statistik_year + 1));
	                                $total_result = ClientsHouses::find()->where(['between', 'date_add_commission', $data_begin , $data_end ])->sum('commission');
                                ?>

                                <td class="text-center">
                                	<?= Html::a($model->getMonth($months), ['/site/earned/', 'year' => $statistik_year, 'month' => $months, ], ['onclick' =>'window.location.href = "/site/earned?year='.$statistik_year.'&month='.$months.'";' ])?><br><?=$result?>
                                </td>
                                <?php } $all_sum += $total_result; ?>
                                <td><center><b><?=$total_result?></b></center></td>
                            </tr>
                        <?php } ?>
							<tr>
								<td colspan="13" class="text-right">
									<b>Итого : </b>
								</td>
								<td class="text-center" >
									<b><?=$all_sum?></b>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>


	<div class="row">
		<center>
			<div class="col-md-8">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <b class="panel-title" style="color:white;font-size: 20px;"> <center> <?=$model->getMonth($month)?> <?=$year?> </center></b>
                    </div>
                    <div class="panel-body">
                        <div style="margin-top: 10px" class="table-responsive">
                            <table class="table table-bordered" style="font-size: 14px;">
                                <tr>
                                    <th><center>Дата</center></th>
                                    <th><center>Сумма</center></th>
                                    <th><center>С объекта</center></th>
                                </tr>
                                <?php 
                                    $data_begin = mktime(0, 0, 0, $month, 1, $year);
                                    $data_end = mktime(0, 0, 0, $month + 1 , 1, $year);
                                    $total = 0; $total_count = 0;
                                for ($begin = $data_begin; $begin < $data_end; $begin = $begin + 86400) { 
                                       
                                    $results = ClientsHouses::find()->where(['between', 'date_add_commission', date('Y-m-d', $begin) , date('Y-m-d', ($begin+ 86399)) ])->sum('commission');

                                    $clients_houses = ClientsHouses::find()->where(['between', 'date_add_commission', date('Y-m-d', $begin) , date('Y-m-d', ($begin+ 86399)) ])->all();
                                    $total += $results;

                                    $count = ClientsHouses::find()->where(['between', 'date_add_commission', date('Y-m-d', $begin) , date('Y-m-d', ($begin+ 86399)) ])->count();

                                    $total_count += $count;
                                    ?>
                                    <tr>
                                        <td><center><?=date('d', $begin)?></center></td>
                                        <td><center><b><?=$results == null ? "-" : $results ?></b></center></td>
                                        <td><?php if($clients_houses == null) echo "<center><b>-</b></center>"; ?>
                                        	<?php foreach ($clients_houses as $houses) {
                                        		$house = House::findOne($houses->house_id);
                                        	?>
                                        	<center>
                                        		<?= Html::a($house->address, ['/house/view', 'id' => $house->id, ], ['onclick' =>'window.location.href = "/house/view?id='.$house->id.';' ])?>	
                                        	</center>
                                        	<?php } ?>
                                        </td>
                                            
                                    </tr>
                                <?php } ?>
                                <tr>
                                	<td></td>
                                    <td><center><b style="font-size:18px;">Заработано за <?=$model->getMonth($month)?> <?=$year?> <br><?=$total?></b></center></td>
                                    <td>
                                    	<center>
                                    		<b>Всего сдано объектов<br>
                                    			<span style="font-size:24px;"> 
                                    				<?=$total_count?> 
                                    			</span>
                                    		</b>
                                    	</center>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </center>
	</div>