<?php 

use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;
//klyuch AJEAjFoBAAAAz4DpdAMAdfnqx--qCWQL2zYplBgvR6wFDQAAAAAAAAAAAABnN6RJx7uNtBv1UPuWuBoEFz1WXw==
?>
<div style="width: 100%; height: 400px;">
<?= YandexMaps::widget([
    'htmlOptions' => [
        'style' => 'height: 400px;',
    ],
    'map' => new Map('yandex_map', [
        'center' => [55.7372, 37.6066],
        'zoom' => 17,
        'controls' => [Map::CONTROL_ZOOM],
        'behaviors' => [Map::BEHAVIOR_DRAG],
        'type' => "yandex#map",
    ],
    [
        'objects' => [new Placemark(new Point(55.7372, 37.6066), [], [
            'draggable' => true,
            'preset' => 'islands#dotIcon',
            'iconColor' => '#2E9BB9',
            'events' => [
                'dragend' => 'js:function (e) {
                    console.log(e.get(\'target\').geometry.getCoordinates());
                    console.log(e.get(\'target\').properties.get(\'description\'));



        var coords = e.get(\'target\').geometry.getCoordinates();
        var myGeocoder1 = ymaps.geocode(coords, {kind: \'house\'});
        var myGeocoder2 = ymaps.geocode(coords, {kind: \'street\'});
        var myGeocoder3 = ymaps.geocode(coords, {kind: \'metro\'});
        var myGeocoder4 = ymaps.geocode(coords);
        myGeocoder1.then(
            function (res) {
                var street = res.geoObjects.get(0);
                var name = street.properties.get(\'name\');
                // Будет выведено «улица Большая Молчановка»,
                // несмотря на то, что обратно геокодируются
                // координаты дома 10 на ул. Новый Арбат.
                alert(name);
            });

        myGeocoder2.then(
            function (res) {
                var street = res.geoObjects.get(0);
                var name = street.properties.get(\'name\');                
                alert(name);
            });

            myGeocoder3.then(
            function (res) {
                var street = res.geoObjects.get(0);
                var name = street.properties.get(\'name\');               
                alert(name);
            });


            myGeocoder4.then(
            function (res) {
                var street = res.geoObjects.get(0);
                var name = street.properties.get(\'name\');                
                alert(name);
            });
                                   
                }',
                
            ]
        ])]
    ])
]) ?>
</div>