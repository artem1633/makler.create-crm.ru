<?php
use yii\helpers\Html;

$this->title = 'Инструкция';
?>
<div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="index-5">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Инструкция</h4>
        </div>
        <div class="panel-body page-container-width3 desc-page-container4">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                <div  data-scrollbar="true" data-init="true" style="overflow: hidden; width: auto; height: auto;">
                    <p style="font-size: 17px;"><b>
                        &nbsp &nbsp &nbsp &nbspProject Control –  новый продукт для управления онлайн – проектами, разработанный компанией TEO.  На изучение программы требуется не  больше 2 часов, все разделы меню наглядные и простые в работе.
                    </b></p>
                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp &nbspФункционал состоит из 5 основных разделов, которые расположены в верхнем меню программы. Система рассчитана на 5 типов пользователей – администратора,  менеджеров, аналитика проекта, исполнителей и клиентов.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp &nbspНаходясь в системе, менеджер:
                    </p>
                    <ul >
                        <li>редактирует общую информацию по проекту (он вправе сменить исполнителя, приоритет заказа, изменить этапы выполнения работ);</li>
                        <li>вводит дополнительные параметры (добавляет новый функционал, изменяет сроки сдачи заказа;</li>
                        <li>оставляет комментарии к проекту;</li>
                        <li>регистрирует новых пользователей системы;</li>
                        <li>делит сложные проекты на этапы (спринты и задачи);</li>
                        <li>отслеживает сроки их исполнения.</li>
                    </ul>

                     <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp &nbspВ системе действует четкая иерархия: администратор назначат ответственных за заказы менеджеров, и дает доступ клиентам, делая их наблюдателями. Менеджеры распределяют проекты между исполнителями. Каждый пользователь имеет доступ только к своему проекту.
                    </p>

                     <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp 1)<b>Первый раздел -  Рабочий стол.</b> Это информационная панель, которая отображает количество находящихся в работе проектов, отдельных задач и рейтинг.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp Администратор отслеживает выполнение проектов и задач, анализирует, какое количество времени было потрачено на заказ. Зайдя в раздел  «Проекты», пользователи могут  посмотреть, на сколько процентов выполнены интересующие их проекты.
                    </p>
                    <br><br><center><?= Html::img("@web/instruksiya/dashboard.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp Авторизованные пользователи видят на рабочем столе комментарии от клиентов, а также свои текущие задачи – доработать проект, исправить ошибки.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp 2)<b>Второй раздел - Доска проекта </b> нужен для создания и хранения текущих проектов. Администратор может редактировать созданные проекты: менять название, ответственного менеджера, статус заказа. 
                    </p>
                    <br><br><center><?= Html::img("@web/instruksiya/project.jpg")?></center><br><br>

                     <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp Доски проектов состоят из проектов, внутри них создаются  спринты с дедлайнами и статусами.  В спринтах проставляются задачи. Команда может  формировать новые этапы на свои проекты.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp Для задач, которые требуют исправления, создается отдельная группа со своим названием и цветом.
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/etap.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspСоздавая новый проект, администратор выбирает клиента, ответственного исполнителя, прописывает название и статус проекта. 
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspВыбирая клиента, администратор автоматически наделяет его правами доступа к проекту. Это значит, что заказчик видит и комментирует задачи, спринты, выполненные исполнителем работы.
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/create_project.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp 3)<b>Третий раздел – Задачи  </b> необходим для создания задач по проектам. В системе все задачи делятся на спринты, а спринты на задачи.
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/tasks.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspКаждая задача имеет свое название, ответственного за ее выполнение менеджера и исполнителя, статус срочности. Программист видит только ту задачу, по которой он назначен исполнителем.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspЗаходя в раздел, администратор или менеджер может:
                    </p>

                    <ul >
                        <li>выделять наиболее приоритетные и срочные  задачи, редактировать их в любое время;</li>
                    </ul>

                    <br><br><center><?= Html::img("@web/instruksiya/create_tasks.jpg")?></center><br><br>

                    <ul >
                        <li>обозначать их различными цветами для удобного поиска;</li>
                        <li>разделять задачи на более детальные этапы;</li>
                        <li>назначать ответственных за них лиц;</li>
                    </ul>

                    <br><br><center><?= Html::img("@web/instruksiya/edit_task.jpg")?></center><br><br>

                    <ul >
                        <li>проверять сроки исполнения отдельных этапов;</li>
                        <li>информировать исполнителя о необходимости доработки проекта.</li>
                    </ul>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspК задаче можно прикреплять документы и фотографии. Клиенты оставляют в системе комментарии, делают правки, указывают на ошибки, добавляют новые цели. 
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/comment.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspРаздел позволяет общаться исполнителям и клиентам прямо в системе. В результате большая часть рабочего времени не уходит на переговоры и скайп - консультации.
                    </p>

                     <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp 4)<b>Четвертый раздел – Пользователи </b> позволяет регистрировать в системе новых пользователей и распределять между ними права доступа. Любую информацию легко отредактировать или удалить. 
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspДля регистрации нового пользователя менеджеру нужно заполнить электронную форму:
                    </p>

                     <ul >
                        <li>ФИО и контактные данные;</li>
                        <li>Его адрес;</li>
                        <li>Профессиональные навыки и опыт;</li>
                        <li>Стоимость работы в час.</li>
                    </ul>

                    <br><br><center><?= Html::img("@web/instruksiya/users.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp<b>Пятый раздел – Настройки  </b>содержит справочную информацию, необходимую для быстрой работе в системе.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbspСекция « Проекты» позволяет создавать, хранить и редактировать в едином интерфейсе все заказы фирмы. Все доступные пользователю данные отображаются в виде таблицы.
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/project_list.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp <b>Секция «Спринты» </b>нужна для дробления сложных проектов на мелкие шаги. Менеджер может выставить время для выполнения каждого спринта и проконтролировать результат работы.
                    </p>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp Такая методика позволяет планировать разработку проекта и вовремя корректировать недочеты.
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/sprint.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp <b>Секции «Справочник типов» и «Приоритет» </b> нужны для формирования типов задач. Компания может настроить типы задач и сроки их выполнения под свои бизнес-процессы. 
                    </p>

                    <br><br><center><?= Html::img("@web/instruksiya/type.jpg")?></center><br><br>

                    <br><br><center><?= Html::img("@web/instruksiya/priority.jpg")?></center><br><br>

                    <p style="font-size: 15px;">
                        &nbsp &nbsp &nbsp <b><center><h4>Система прекрасно подойдет разработчикам сайтов, посадочных страниц и компьютерных программ.</h4></center></b> 
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>