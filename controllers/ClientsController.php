<?php

namespace app\controllers;

use Yii;
use app\models\Clients;
use app\models\ClientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\House;
use app\models\ClientsHouses;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex($type = 0)
    {    
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$type);

        $apartment_long = House::find()->where(['house_type' => 1])->count();
        $room_long = House::find()->where(['house_type' => 3])->count();
        $home_long = House::find()->where(['house_type' => 5])->count();

        $apartment_short = House::find()->where(['house_type' => 2])->count();
        $room_short= House::find()->where(['house_type' => 4])->count();
        $home_short = House::find()->where(['house_type' => 6])->count();

        $clients = Clients::find()->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'apartment_long' => $apartment_long,
            'room_long' => $room_long,
            'home_long' => $home_long,
            'apartment_short' => $apartment_short,
            'room_short' => $room_short,
            'home_short' => $home_short,
            'clients' => $clients,
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $apartment_long = House::find()->where(['house_type' => 1])->count();
        $room_long = House::find()->where(['house_type' => 3])->count();
        $home_long = House::find()->where(['house_type' => 5])->count();

        $apartment_short = House::find()->where(['house_type' => 2])->count();
        $room_short= House::find()->where(['house_type' => 4])->count();
        $home_short = House::find()->where(['house_type' => 6])->count();

        $clients = Clients::find()->count();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'id' => $id,
            'apartment_long' => $apartment_long,
            'room_long' => $room_long,
            'home_long' => $home_long,
            'apartment_short' => $apartment_short,
            'room_short' => $room_short,
            'home_short' => $home_short,
            'clients' => $clients,
        ]);
        
    }

    /**
     * Creates a new Clients model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Clients();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Клиенты",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }       
    }

    public function actionAdd()
    {
        $request = Yii::$app->request;
        $model = new Clients();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Добавить",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }       
    }
    public function actionAddnew($id)
    {
        $request = Yii::$app->request;
        $model = new Clients();  
        $house = House::findOne($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $house->client_id = $model->id;
            $house->save();
            $house->setClientsHouse();
            return [
                'forceClose'=>true,
                'forceReload'=>'#datas-pjax',
            ];         
        }else{  
            if($house->house_type == 1)
            {
                $model->type = 1; 
                $model->category = 1; 
            }
            if($house->house_type == 2)
            {
                $model->type = 1; 
                $model->category = 2; 
            }
            if($house->house_type == 3)
            {
                $model->type = 2; 
                $model->category = 1; 
            }
            if($house->house_type == 4)
            {
                $model->type = 2;
                $model->category = 2; 
            }
            if($house->house_type == 5)
            {
                $model->type = 3; 
                $model->category = 1; 
            }
            if($house->house_type == 6)
            {
                $model->type = 3; 
                $model->category = 2; 
            } 
            return [
                'title'=> "Добавить",
                'size' => 'large',
                'content'=>$this->renderAjax('_from_new', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])        
            ];         
        }    
    }
    public function actionSearch($client_id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($client_id);
        $houses_dataProvider = $model->getHouses();

        if($request->isGet)
        {
            return [
                'title'=> "Искать",
                'size' => 'large',
                'content'=>$this->renderAjax('search', [
                    'houses_dataProvider' => $houses_dataProvider,
                    'model' => $model,
                ]),      
            ];         
        }
        else
        {           
            return [
                'forceClose'=>true,
            ];         
        }              
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->last_call != null ) $model->last_call = \Yii::$app->formatter->asDate($model->last_call, 'php:d.m.Y');
        if($model->why_call != null ) $model->why_call = \Yii::$app->formatter->asDate($model->why_call, 'php:d.m.Y');     

        if($request->isAjax){
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#base-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionChangeType($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['type' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeCategory($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['category' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeRoomCount($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['room_count' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeBudjet($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['budjet' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeMebel($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['mebel' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeFloor($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['floor' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeLastCall($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['last_call' => \Yii::$app->formatter->asDate($value, 'php:Y-m-d') ], [ 'id' => $id ])->execute();
    }

    public function actionChangeWhyCall($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['why_call' => \Yii::$app->formatter->asDate($value, 'php:Y-m-d') ], [ 'id' => $id ])->execute();
    }

    public function actionChangeTelephone($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients', ['telephone' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionChangeCommission($id,$value)
    {
        Yii::$app->db->createCommand()->update('clients_houses', ['commission' => $value, 'date_add_commission' => date('Y-m-d') ], [ 'id' => $id ])->execute();
    }
    /**
     * Delete an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $clients_house = ClientsHouses::find()->where(['clients_id' => $id])->all();
        foreach ($clients_house as $value) {
            $value->delete();
        }
        $this->findModel($id)->delete();

        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            
            return $this->redirect(['index']);
        }


    }

     /**
     * Select multiple existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionBulkSelect($client_id)
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' ));
        foreach ( $pks as $pk ) {
            $model = House::findOne($pk);
            $model->client_id = $client_id;
            $model->save();
            $model->setClientsHouse();
        }
        Yii::$app->db->createCommand()->update('clients', ['status' => 2], ['id' => $client_id])->execute();

        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#houses-pjax'];
        }       
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
