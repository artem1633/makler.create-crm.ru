<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\House;
use app\models\Clients;
use app\models\ClientsHouses;
use app\models\Nastroyka;

class SiteController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['dashboard', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            return $this->redirect('site/boards-project');
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Рабочий стол
     * через этот екшн заходим на Рабочий стол
     * @return Response|string
     */     
    public function actionDashboard()
    {
      if (Yii::$app->user->isGuest) { 
        return $this->redirect(['site/login']);
      }
        return $this->render('dashboard', [
            'all_tasks' => $result1,
            'result' => $result,
            'comment' => $comment,
            'total_hour' => $total_hour,
            'tasks' => $all_tasks,
        ]);
    }
    public function actionHouse()
    {
        return $this->redirect(['house/apartment', 'type' => 4]);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
         Yii::$app->user->logout();

        $this->redirect(['login']);
    }

    public function actionAvtorizatsiya()
    {
      if(isset(Yii::$app->user->identity->id))
      {
        return $this->render('error');
      }        
      else
      {
          Yii::$app->user->logout();
          $this->redirect(['login']);
      }
    }
    
    public function actionEarned($year = null, $month = null)
    { 
      if($year == null)  $year = date('Y');  
      if($month == null)  $month = date('m');
      $apartment_long = House::find()->where(['house_type' => 1])->count();
      $room_long = House::find()->where(['house_type' => 3])->count();
      $home_long = House::find()->where(['house_type' => 5])->count();

      $apartment_short = House::find()->where(['house_type' => 2])->count();
      $room_short= House::find()->where(['house_type' => 4])->count();
      $home_short = House::find()->where(['house_type' => 6])->count();

      $clients = Clients::find()->count();

      $min_litsenzed = ClientsHouses::find()->min('date_add_commission');
      $max_litsenzed = ClientsHouses::find()->max('date_add_commission');

      return $this->render('earned', [
          'model' => new ClientsHouses(),
          'apartment_long' => $apartment_long,
          'room_long' => $room_long,
          'home_long' => $home_long,
          'apartment_short' => $apartment_short,
          'room_short' => $room_short,
          'home_short' => $home_short,
          'clients' => $clients,
          'year' => $year,
          'month' => $month,
          'min_litsenzed' => \Yii::$app->formatter->asDate($min_litsenzed. Yii::$app->getTimeZone(), 'php:Y'),
          'max_litsenzed' => \Yii::$app->formatter->asDate($max_litsenzed. Yii::$app->getTimeZone(), 'php:Y'),
      ]);
    }

    // через этот екшн можно войти на инструкцию
    public function actionInstruksion()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        return $this->render('instruksion', []);        
    }

    public function actionMap()
    {
      return $this->render('map', []);
    }

    public function actionTuning()
    {
        $request = Yii::$app->request;
        $model = Nastroyka::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($model == null){
            $model = new Nastroyka();
            $model->user_id = Yii::$app->user->id;
            $model->coordinate_x = '55.753187580818675';
            $model->coordinate_y = '37.62333811759736';
            $model->city = "Москва";
            $model->location_metro = "метро Площадь Революции";
            $model->address = "улица Ильинка";
            $model->dom = " 7";
        }
/*echo "<pre>";
print_r($request->post());
echo "</pre>";
die;*/
        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['site/dashboard']);
        } else {
            return $this->render('_form_nastroyka', [
                'model' => $model,
            ]);
        }
    }
}
