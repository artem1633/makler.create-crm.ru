<?php

namespace app\controllers;

use Yii;
use app\models\House;
use app\models\HouseSearch;
use yii\web\Controller; 
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\HouseFile;
use app\models\OwnerContacts;
use app\models\Clients; 
use app\models\ClientsHouses;
use app\models\Columns;
use app\models\HouseDocuments;
use yii\data\ActiveDataProvider;

class HouseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all House models.
     * @return mixed
     */
    public function actionApartment($type)
    {    
        $searchModel = new HouseSearch();
        $post = Yii::$app->request->post();
        if(Yii::$app->request->queryParams['_pjax'] == null){
            $session = Yii::$app->session;
            $session['search_id'] = isset($post['HouseSearch']['search_id']) ? $post['HouseSearch']['search_id'] : '';
            $session['search_city'] = isset($post['HouseSearch']['search_city']) ? $post['HouseSearch']['search_city'] : '';
            $session['search_metro'] = isset($post['HouseSearch']['search_metro']) ? $post['HouseSearch']['search_metro'] : '';
            $session['search_address'] = isset($post['HouseSearch']['search_address']) ? $post['HouseSearch']['search_address'] : '';
            $session['search_owner_name'] = isset($post['HouseSearch']['search_owner_name']) ? $post['HouseSearch']['search_owner_name'] : '';
            $session['search_owner_surname'] = isset($post['HouseSearch']['search_owner_surname']) ? $post['HouseSearch']['search_owner_surname'] : '';
            $session['search_telephone'] = isset($post['HouseSearch']['search_telephone']) ? $post['HouseSearch']['search_telephone'] : '';
            $session['search_home_number'] = isset($post['HouseSearch']['search_home_number']) ? $post['HouseSearch']['search_home_number'] : '';
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $type, $post);

        $apartment_long = House::find()->where(['house_type' => 1])->count();
        $room_long = House::find()->where(['house_type' => 3])->count();
        $home_long = House::find()->where(['house_type' => 5])->count();

        $apartment_short = House::find()->where(['house_type' => 2])->count();
        $room_short= House::find()->where(['house_type' => 4])->count();
        $home_short = House::find()->where(['house_type' => 6])->count();

        $clients = Clients::find()->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'post' => $post,
            'dataProvider' => $dataProvider,
            'apartment_long' => $apartment_long,
            'room_long' => $room_long,
            'home_long' => $home_long,
            'apartment_short' => $apartment_short,
            'room_short' => $room_short,
            'home_short' => $home_short,
            'type' => $type,
            'clients' => $clients,
        ]);
    }


    /**
     * Displays a single House model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $apartment_long = House::find()->where(['house_type' => 1])->count();
        $room_long = House::find()->where(['house_type' => 3])->count();
        $home_long = House::find()->where(['house_type' => 5])->count();

        $apartment_short = House::find()->where(['house_type' => 2])->count();
        $room_short= House::find()->where(['house_type' => 4])->count();
        $home_short = House::find()->where(['house_type' => 6])->count();

        $clients = Clients::find()->count();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'apartment_long' => $apartment_long,
            'room_long' => $room_long,
            'home_long' => $home_long,
            'apartment_short' => $apartment_short,
            'room_short' => $room_short,
            'home_short' => $home_short,
            'clients' => $clients,
        ]);       
    }
    public function actionFloors($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_PARAMETER;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Этажность",
                'size' => 'normal',
                'content'=>$this->renderAjax('_floors', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionDocumentsView($id)
    {
        $request = Yii::$app->request;
        $query = HouseDocuments::find()->where(['house_id' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //$model = HouseDocuments::find()->where(['house_id' => $id])->all();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Документы",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('documents_view', [
                        'dataProvider' => $dataProvider,
                    ]),
                ];    
        }else{
            return $this->render('documents_view', [
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionVideo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_DESCRIPTION;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Видео",
                'size' => 'small',
                'content'=>$this->renderAjax('_video', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionMetro($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_PARAMETER;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Метро рядом",
                'size' => 'small',
                'content'=>$this->renderAjax('_metro', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionFio($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_OWNER;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Описание",
                'size' => 'normal',
                'content'=>$this->renderAjax('_fio', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionDescription($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_DESCRIPTION;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Описание",
                'size' => 'normal',
                'content'=>$this->renderAjax('_description', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionShowPurposeCall($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_DATAS;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Цель звонка",
                'size' => 'normal',
                'content'=>$this->renderAjax('_purpose_call', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionApartmentCreate($type)
    {
        $request = Yii::$app->request;
        $model = new House(); 
        $model->house_type = $type;
        $post = $request->post();  
        $modelUpload = new UploadForm();
        
        if($post['House']['step'] == null)
        {
            $model->load($request->get());
            $model->step = 1;
            $model->scenario = House::SCENARIO_PARAMETER;
            $model->begin_date = date('d.m.Y');
            $model->end_date = date('d.m.Y');
        }

        if($post['House']['step'] == 1)
        {
            $model->step = 1;            
            if ($model->load($request->post()) && $model->validate()) 
            {
                $model->step = 2;
                $model->scenario = House::SCENARIO_OPTIONS;
            }
            if($model->step == 1) $model->scenario = House::SCENARIO_PARAMETER;
        }

        if($post['House']['step'] == 2)
        {
            $model->step = 2;   
            if ($model->load($request->post()) && $model->validate()) 
            {
                $model->step = 3;
                $model->scenario = House::SCENARIO_DESCRIPTION;
            }
            if($model->step == 2) $model->scenario = House::SCENARIO_OPTIONS;
        }
        if($post['House']['step'] == 3)
        {
            $model->step = 3;/*echo "string";die;*/
            
            if ($model->load($request->post()) && $model->validate()) 
            {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->documents = $imageName;
                //$model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);
                
                $model->step = 4;
                $model->scenario = House::SCENARIO_LOCATION;
                $model->getLocalisation();
            }
            if($model->step == 3) $model->scenario = House::SCENARIO_DESCRIPTION;
        }
        if($post['House']['step'] == 4)
        {
            $model->step = 4;
            
            if ($model->load($request->post()) && $model->validate()) 
            {
                $model->step = 5;
                $model->scenario = House::SCENARIO_DATAS;
            }
            if($model->step == 4) $model->scenario = House::SCENARIO_LOCATION;
        }
        if($post['House']['step'] == 5)
        {
            $model->step = 5;
            if ($model->load($request->post()) && $model->validate()) 
            {
                $model->step = 6;
                $model->scenario = House::SCENARIO_OWNER;
            }
            if($model->step == 5) $model->scenario = House::SCENARIO_DATAS;
        }
        if($post['House']['step'] == 6)
        {
            if ($model->load($request->post()) && $model->validate() && $model->save()) 
            {
                $model->contacts = $post['House']['contacts'];
                
                foreach ($model->contacts as $value) {
                    $relative = new OwnerContacts();
                    $relative->house_id = $model->id; 
                    $relative->contact = $value['contact'];
                    $relative->telephone = $value['telephone'];
                    $relative->save();
                }

                $tempFiles = json_decode($model->tempFiles, true);
                $model->addFiles($tempFiles, $model->id);

                $documents = json_decode($model->documents, true);
                $model->addDocuments($documents, $model->id);
                
                $model->setClientsHouse();
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                $model->step = 6;
                $model->scenario = House::SCENARIO_OWNER;
            }
        }

        return $this->render('create_apartment', ['model' => $model,'modelUpload' => $modelUpload,]);       
    }

    public function actionDownloadImages($id)
    {
        $house_files = HouseFile::find()->where(['house_id'=>$id])->all();
        $zip = new \ZipArchive;
        unlink(Yii::getAlias('download.zip'));
        $download = 'download.zip';
        $zip->open($download, \ZipArchive::CREATE);
        foreach ($house_files as $file) { /* Add appropriate path to read content of zip */
            $zip->addFile($file->path);
        }
        $zip->close();
        header('Content-Type: application/zip');
        header("Content-Disposition: attachment; filename = $download");
        header('Content-Length: ' . filesize($download));
        header("Location: $download");

        \Yii::$app->response->sendFile('download.zip');  
    }

    /**
     * Delete an existing House model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;

        

        $house_file = HouseFile::find()->where(['house_id' => $id])->all();

        foreach ($house_file as $file) {
            unlink(Yii::getAlias($file->path));
            $file->delete();
        }

        $contacts = OwnerContacts::find()->where(['house_id' => $id])->all();

        foreach ($contacts as $contact) {
            $contact->delete();
        }

        $model = House::findOne($id);
        $imageName = $model->documents;
        Yii::$app->db->createCommand()->delete('house', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);
        
       // $this->findModel($id)->delete();
        
       
        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            
            return $this->redirect(['index']);
        }
    }

    public function actionEditLocation($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_LOCATION;

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('_form_location', [
                'model' => $model,
            ]);
        }
    }

    public function actionEditStatus($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_PARAMETER;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#status-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'small',
                'content'=>$this->renderAjax('_form_status', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditPublicSite($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_PARAMETER;
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#status-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'small',
                'content'=>$this->renderAjax('_form_public_site', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditParameter($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_PARAMETER;
        if($model->begin_date != null ) $model->begin_date = \Yii::$app->formatter->asDate($model->begin_date, 'php:d.m.Y');
        if($model->end_date != null ) $model->end_date = \Yii::$app->formatter->asDate($model->end_date, 'php:d.m.Y');
            
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#parameter-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_parameter', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditOptions($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_OPTIONS;

        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#options-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'normal',
                'content'=>$this->renderAjax('_form_options', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditOwner($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);    
        $post = Yii::$app->request->post();    
        $model->scenario = House::SCENARIO_OWNER;

        if ($model->load($request->post()) && $model->save()) {

            $contacts = OwnerContacts::find()->where(['house_id'=> $id])->all();
            foreach ($contacts as $value) {
                if (($rel = OwnerContacts::findOne($value->id)) !== null) {
                   $rel->delete();
                }
            }

            $owner_contact = $post['House']['contacts'];              
            foreach ($owner_contact as $value) {
                $relative = new OwnerContacts();
                $relative->house_id = $id; 
                $relative->contact = $value['contact'];
                $relative->telephone = $value['telephone'];
                $relative->save();
            }
            return [
                'forceReload'=>'#owner-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_owner', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditDatas($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        $client = $model->client_id;      
        $model->scenario = House::SCENARIO_DATAS;
        if($model->last_call != null ) $model->last_call = \Yii::$app->formatter->asDate($model->last_call, 'php:d.m.Y');
        if($model->call_back != null ) $model->call_back = \Yii::$app->formatter->asDate($model->call_back, 'php:d.m.Y');
        if($model->when_delivered != null ) $model->when_delivered = \Yii::$app->formatter->asDate($model->when_delivered, 'php:d.m.Y');

        if ($model->load($request->post()) && $model->save()) {
            if($model->client_id != null && $client == null) $model->setClientsHouse();
            return [
                'forceReload'=>'#datas-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_datas', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    public function actionSendDocument($file)
    {
        return \Yii::$app->response->sendFile($file);
    }
    public function upload()
    {

        if ($this->validate()) {
            $this->documents->saveAs('uploads/' . $this->documents->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
    public function actionEditDescription($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_DESCRIPTION;
        $modelUpload = new UploadForm();
        $model->tempFiles = "";
        $model->documents = "";

        if ($model->load($request->post()) && $model->validate()) {
            $tempFiles = json_decode($model->tempFiles, true);
            $model->addFiles($tempFiles, $model->id);

            $documents = json_decode($model->documents, true);
            $model->addDocuments($documents, $model->id);

            /*$model->other_file = UploadedFile::getInstance($model, 'other_file');
            $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;*/


            //if (!empty($model->other_file)) $model->documents = $imageName;
            $model->save();

            //if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);

            return [
                'forceReload'=>'#description-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_description', [
                    'model' => $model,
                    'modelUpload' => $modelUpload,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionAddClient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_DATAS;
        $client = $model->client_id;

        if ($model->load($request->post()) && $model->save()) {
            if($model->client_id != null && $client == null) $model->setClientsHouse();
            return [
                'forceReload'=>'#datas-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Добавить",
                'size' => 'normal',
                'content'=>$this->renderAjax('_form_client', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionChangeLocation($id,$type)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        $model->scenario = House::SCENARIO_LOCATION;

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['/house/apartment', 'type' => $type]);
        } else {
            return $this->render('_form_location', [
                'model' => $model,
            ]);
        }
    }
    public function actionEditTelephone($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['telephone' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditCost($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['cost' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionTotal($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['total_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionKitchen($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['kitchen_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionLiving($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['living_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionRoom($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['room_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionDistance($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['distance_to_city' => $value ], [ 'id' => $id ])->execute();
    }

    public function actionEditLastCall($id,$value)
    {
        $strlen = strlen( $value );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $value, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 8) Yii::$app->db->createCommand()->update('house', ['last_call' => null ], [ 'id' => $id ])->execute();
        else Yii::$app->db->createCommand()->update('house', ['last_call' => \Yii::$app->formatter->asDate($value, 'php:Y-m-d') ], [ 'id' => $id ])->execute();
    }
    public function actionEditCallBack($id,$value)
    {
        $strlen = strlen( $value );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $value, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 8) Yii::$app->db->createCommand()->update('house', ['call_back' => null ], [ 'id' => $id ])->execute();
        else Yii::$app->db->createCommand()->update('house', ['call_back' => \Yii::$app->formatter->asDate($value, 'php:Y-m-d') ], [ 'id' => $id ])->execute();
    }
    public function actionEditFridge($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['fridge' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditWasher($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['washer' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditZalog($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['zalog' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditTv($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['tv' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditFurniture($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['furniture' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditHomeFloor($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['home_floor' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditFloor($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['floor' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditConditioner($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['conditioner' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditPublicAvito($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['public_avito' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditWallMaterial($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['wall_material' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditSite($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['public_site' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditRooms($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['rooms_count' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditLand($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['land_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditHome($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['home_area' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionEditHomeType($id,$value)
    {
        Yii::$app->db->createCommand()->update('house', ['home_type' => $value ], [ 'id' => $id ])->execute();
    }
    public function actionUpload()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $house = new House();
        $model = new UploadForm();

        $orderId = Yii::$app->request->post()['orderId'];

        if($orderId <= 0) {
            $orderId = $house->getLastId();
        }

        if (Yii::$app->request->isPost) {
            $model->anyFiles = UploadedFile::getInstances($model, 'anyFiles');
            if($path = $model->upload(new Directory(), $orderId)) {
                $response = ['path' => $path];
                return $response;
            }
        }

        $response = ['error' => 'Upload faile'];
        return $response;
    }

    public function actionUploadDocuments()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $house = new House();
        $model = new UploadForm();

        $orderId = Yii::$app->request->post()['orderId'];

        if($orderId <= 0) {
            $orderId = $house->getLastId();
        }
        $model->load(Yii::$app->request->post());
        if (Yii::$app->request->isPost) {
            $model->anyDocuments = UploadedFile::getInstances($model, 'anyDocuments');
            if($path = $model->uploadDocuments(new Directory(), $orderId)) {
                $response = ['path' => $path];
                return $response;
            }
        }

        $response = ['error' => 'Upload faile'];
        return $response;
    }
    //с этим экшном удаляется файлы из задачи 
    public function actionRemoveFile($id) 
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = HouseFile::findOne($id);
        unlink(Yii::getAlias($file->path));
        if($file->delete()) {
            return [
                'forceReload'=>'#description-pjax',
                'forceClose' => true,
            ];
        } else {
            return false;
        }
    }

    public function actionDeleteDocument($id) 
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $document = HouseDocuments::findOne($id);
        unlink(Yii::getAlias($document->path));
        if($document->delete()) {
            return [
                //'forceReload'=>'#description-pjax',
                'forceClose' => true,
            ];
        } else {
            return false;
        }
    }

    public function actionRemoveClient($id, $house_id) 
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $client_house = ClientsHouses::findOne($id); 
        $client_house->situation = 2;
        $client_house->save();

        $model = $this->findModel($house_id);

        /*$client_house = ClientsHouses::find()->where(['house_id' => $model->id, 'client_id' => $model->client_id, 'situation' => 1])->one();
        $client_house->situation = 2;
        $client_house->save();*/

        $model->client_id = null;
        $model->save();  

        $clients_houses = ClientsHouses::find()->where(['situation' => 1, 'clients_id' => $client_house->clients_id])->one();
        if($clients_houses == null)
        {
            Yii::$app->db->createCommand()->update('clients', ['status' => 1 ], [ 'id' => $client_house->clients_id ])->execute();
        }
        
        return [
            'forceReload'=>'#datas-pjax',
            'forceClose' => true,
        ];
        
    }

    public function actionHouseFloorList($id) 
    {
        for ($i=""; $i <= $id; $i++) { 
            echo "<option value = '".$i."'>".$i."</option>" ;
        }       
    }

    public function actionSorting($type)
    {    
        //Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->id;
        /* echo "<pre>";
         print_r($post['active']);
         echo "</pre>";*/
         //die;
       

        if ($request->post()) {
            $post = $request->post();

            //aktiv columnlarni bazaga yozilishi
            $active = $post['active'];
            $strlen = strlen( $active );
            $id = ''; $order_number = 0;
            for( $i = 0; $i <= $strlen; $i++ ) 
            {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    Yii::$app->db->createCommand()->update('columns', ['status' => 1, 'order_number' => $order_number ], [ 'id' => $id ])->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            $order_number++;
            Yii::$app->db->createCommand()->update('columns', ['status' => 1, 'order_number' => $order_number ], [ 'id' => $id ])->execute();

            //aktiv columnlarni bazaga yozilishi
            $noactive = $post['no-active'];
            $strlen = strlen( $noactive );
            $id = ''; 
            for( $i = 0; $i <= $strlen; $i++ )  
            {
                $char = substr( $noactive, $i, 1 );             
                if($char == ',') {
                    Yii::$app->db->createCommand()->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            Yii::$app->db->createCommand()->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();

            return $this->redirect(['house/apartment', 'type' => 4]);
        } 
        else {
            $column = Columns::find()->where(['user_id' => $user_id])->one();
            if($column == null ){
                $col = new Columns();
                $col->setDefaultValues($user_id);
            }

            $columns = Columns::find()->where(['user_id' => $user_id, 'status' => 1])->orderBy([ 'order_number' => SORT_ASC /*, 'item_no'=>SORT_ASC*/ ])->all();
            $active = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = '<div class="grid-item text-danger " style="font-weight:bold; margin-left:200px; font-size:16px;">  Столбец  # '.$i. ' '. $column->name . '</div>';
                $active += [
                    $column->id => ['content' => $name],
                ];
            }

            $columns = Columns::find()->where(['user_id' => $user_id, 'status' => 0])->all();
            $noactive = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = '<div class="grid-item text-danger " style="font-weight:bold; margin-left:200px; font-size:16px;">  Столбец  # '.$i. ' '. $column->name . '</div>';
                $noactive += [
                    $column->id => ['content' => $name],
                ];
            }
            
            return $this->render('sorting',[
                'active' => $active,
                'noactive' => $noactive,
            ]);
            /*return [
                'title'=> "Добавить",
                'size' => 'normal',
                'content'=>$this->renderAjax('sorting', [
                    //'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];*/
        }
    }


    /**
     * Finds the House model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return House the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = House::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
