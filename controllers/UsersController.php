<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Change;
use app\models\PurchasedLicenses;
use app\models\Clients;
use app\models\House;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $apartment_long = House::find()->where(['house_type' => 1])->count();
        $room_long = House::find()->where(['house_type' => 3])->count();
        $home_long = House::find()->where(['house_type' => 5])->count();

        $apartment_short = House::find()->where(['house_type' => 2])->count();
        $room_short= House::find()->where(['house_type' => 4])->count();
        $home_short = House::find()->where(['house_type' => 6])->count();

        $clients = Clients::find()->count();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'apartment_long' => $apartment_long,
            'room_long' => $room_long,
            'home_long' => $home_long,
            'apartment_short' => $apartment_short,
            'room_short' => $room_short,
            'home_short' => $home_short,
            'clients' => $clients,
        ]);
    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $litsenzed = null, $registration = null, $year = null, $month = null, $day = null)
    {   
        $user = Users::findOne($id);
        if($user->type == 1)
        {
            $now = date('Y-m-d H:i:s');
            $begin = date('Y-m-d H:i:s', mktime(0, 0, 0, \Yii::$app->formatter->asDate($now, 'php:m'), 1, \Yii::$app->formatter->asDate($now, 'php:Y')));
            $end = date('Y-m-d H:i:s', mktime(0, 0, 0, \Yii::$app->formatter->asDate($now, 'php:m')+1, 1, \Yii::$app->formatter->asDate($now, 'php:Y')));
            
            $today_registration = Users::find()->where(['between', 'creation_date', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])->andwhere(['type' => '2'])->count();
            $month_registration = Users::find()->where(['between', 'creation_date', $begin, $end])->andwhere(['type' => '2'])->count();
            $all_registration = Users::find()->where(['type' => '2'])->count();

            $today_purchased = PurchasedLicenses::find()->where(['between', 'data', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])->andwhere(['user_type' => '2'])->count();
            $month_purchased = PurchasedLicenses::find()->where(['between', 'data', $begin, $end])->andwhere(['user_type' => '2'])->count();
            $all_purchased = PurchasedLicenses::find()->where(['user_type' => '2'])->count();

            $today_coming = PurchasedLicenses::find()->where(['between', 'data', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])->andwhere(['user_type' => '2'])->sum('cost');
            $month_coming = PurchasedLicenses::find()->where(['between', 'data', $begin, $end])->andwhere(['user_type' => '2'])->sum('cost');
            $all_coming = PurchasedLicenses::find()->where(['user_type' => '2'])->sum('cost');


            $all_litsenzed = Users::find()->where(['type' => 2])->orderBy('creation_date asc')->count();
            $min_litsenzed = Users::find()->where(['type' => 2])->min('creation_date');
            $max_litsenzed = Users::find()->where(['type' => 2])->max('creation_date');


            return $this->render('view_admin', [
                'model' => $this->findModel($id),
                'litsenzed' => $litsenzed,
                'year' => $year,
                'month' => $month,
                'day' => $day,
                'today_registration' => $today_registration,
                'month_registration' => $month_registration,
                'all_registration' => $all_registration,
                'today_purchased' => $today_purchased,
                'month_purchased' => $month_purchased,
                'all_purchased' => $all_purchased,
                'today_coming' => $today_coming,
                'month_coming' => $month_coming,
                'all_coming' => $all_coming,
                'all_litsenzed' => $all_litsenzed,
                'min_litsenzed' => \Yii::$app->formatter->asDate($min_litsenzed. Yii::$app->getTimeZone(), 'php:Y'),
                'max_litsenzed' => \Yii::$app->formatter->asDate($max_litsenzed. Yii::$app->getTimeZone(), 'php:Y'),
            ]);
        }
        if($user->type == 2)
        {
            return $this->render('view_litsenzed', [
                'model' => $this->findModel($id),
            ]);
        }
        if($user->type == 3)
        {
            return $this->render('view_agent', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  
        $model->balance = 0;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить",
                    "size" => "large",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить",
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить",
                    "size" => "large",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        if($model->birthday != null)$model->birthday = \Yii::$app->formatter->asDate($model->birthday, 'php:d.m.Y');     

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceClose' => true,
                    'forceReload'=>'#admin-change-form-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить Пользователя #".$id,
                    "size" => "large",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionAddCash($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);   
        $model->money = 0;    

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $model->balance = $model->balance + $model->money;
                $model->save();
                $model->addHistory();
                return [
                    'forceClose' => true,
                    'forceReload'=>'#admin-change-form-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Пополнить на счёт",
                    'size' => "small",
                    'content'=>$this->renderAjax('add-cash', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionAddAgent($id)
    {
        $request = Yii::$app->request;
        $model = new Users();   
        $model->licensed_user = $id;    
        $model->type = 3;    

        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){                
                return [
                    'forceClose' => true,
                    'forceReload'=>'#admin-change-form-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Добавить агент",
                    'size' => "large",
                    'content'=>$this->renderAjax('add-agent', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionViewHistory($id)
    {   
        $history = Change::find()->where(['user_id' => $id])->all();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "История платежей",
            'size' => "modal-lg",
            'content'=>$this->renderAjax('view_history', [
                'history' => $history,
            ]),
        ];    
    }
    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Пользователя #".$id,
                    //'size' => "modal-lg",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionTransfer($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
    
        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){                
                return [
                    'forceClose' => true,
                    'forceReload'=>'#admin-change-form-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Передать все свои объекты",
                    'size' => "normal",
                    'content'=>$this->renderAjax('transfer', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionChangeLicense($id,$type)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->license = $type;
        $model->balance = $model->balance - (int)$model->getMonthCost($type);

        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->save()){
                $license = new PurchasedLicenses();
                $license->user_id = $id;
                $license->license_type = $type;
                $license->month = 1;
                $license->user_type = $model->type;
                $license->data = date('Y-m-d H:i:s');
                $license->cost = (int)$model->getMonthCost($type);
                $license->save();
                return ['forceClose'=>true,'forceReload'=>'#admin-change-form-pjax'];
            }
        }
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $purchased = PurchasedLicenses::find()->where(['user_id' => $id])->all();
        foreach ($purchased as $value) {
            $value->delete();
        }
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
