<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MagnificPopupAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/libs';
    public $css = [
        'MagnificPopup/css/magnific-popup.css',
    ];
    public $js = [
        'MagnificPopup/js/jquery.magnific-popup.min.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
