<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
 * ClientsSearch represents the model behind the search form about `app\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type', 'category', 'room_count', 'mebel', 'floor'], 'integer'],
            [['fio', 'last_call', 'why_call', 'why_comment', 'comment', 'telephone', 'date_cr'], 'safe'],
            [['budjet'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type)
    {
        if($type == 0)$query = Clients::find();
        if($type == 1)$query = Clients::find()->where(['status' => 1]);
        if($type == 2)$query = Clients::find()->where(['status' => 2]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'type' => $this->type,
            'category' => $this->category,
            'room_count' => $this->room_count,
            'budjet' => $this->budjet,
            'mebel' => $this->mebel,
            'floor' => $this->floor,
            'last_call' => $this->last_call,
            'why_call' => $this->why_call,
            'date_cr' => $this->date_cr,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'why_comment', $this->why_comment])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'telephone', $this->telephone]);

        return $dataProvider;
    }
}
