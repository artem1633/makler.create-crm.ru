<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients_houses".
 *
 * @property int $id
 * @property int $clients_id Клиент
 * @property int $house_id Дом/Квартира/Комната
 * @property string $commentary Комментарий
 * @property string $cause Причина сьезда
 * @property int $situation Ситуация
 * @property int $commission Комиссия
 * @property string $date_cr Дата создания
 * @property string $date_up Дата изменения
 * @property int $house_type Квартиры/Комнаты/Дома
 * @property string $begin_date Дата начала
 * @property string $end_date Дата окончании
 * @property int $status Статус
 * @property int $rooms_count Количество комнат
 * @property int $type Тип дома
 * @property int $floor Этаж
 * @property int $house_floor Этажей в доме
 * @property double $total_area Общая площадь
 * @property double $kitchen_area Площадь кухни
 * @property double $living_area Жилая площадь
 * @property int $beds_count Количество кроватей
 * @property int $berth_count Количество спальных мест
 * @property int $conditioner Кондиционер
 * @property int $camin Камин
 * @property int $balcony Балкон
 * @property int $microwave Микроволновка
 * @property int $fridge Холодильник
 * @property int $washer Стиральная машина
 * @property int $wifi WiFi
 * @property int $tv Телевизор
 * @property int $digital_tv Цифровое ТВ
 * @property int $with_pets Можно с питомцами
 * @property int $with_childrens Можно с детьми
 * @property int $with_events Можно для мероприятий
 * @property string $description Описание
 * @property double $cost Цена
 * @property int $zalog Залог
 * @property string $video Видео
 * @property string $tempFiles Фото
 * @property string $surname_owner Фамилия собственника
 * @property string $name_owner Имя собственника
 * @property string $middle_name_owner Отчество собственника
 * @property string $last_call Последний звонок собеседнику
 * @property string $call_back Перезвонить
 * @property string $purpose_call Цель звонка
 * @property int $public_avito Публикация на авито
 * @property string $description_object Описание объекта
 * @property int $client_id Кто снимает сейчас
 * @property string $metro Метро рядом
 * @property int $wall_material Материал стен
 * @property double $distance_to_city Расстояние до города
 * @property double $home_area Площадь дома
 * @property double $land_area Площадь участка
 * @property string $city Город, населённый пункт
 * @property string $location_metro Метро
 * @property string $address Адрес
 * @property string $dom Дом
 * @property string $map Посмотреть на карте
 * @property string $coordinate_x Координата х
 * @property string $coordinate_y Координата y
 * @property int $room_area Площадь комнаты
 * @property int $home_type Вид обьекта
 * @property int $home_floor Этажей в доме
 *
 * @property Clients $clients
 */
class ClientsHouses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients_houses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clients_id', 'house_id', 'situation', 'house_type', 'status', 'rooms_count', 'type', 'floor', 'house_floor', 'beds_count', 'berth_count', 'conditioner', 'camin','furniture', 'balcony', 'microwave', 'fridge', 'washer', 'wifi', 'tv', 'digital_tv', 'with_pets', 'with_childrens', 'with_events', 'zalog', 'public_avito', 'client_id', 'wall_material', 'room_area', 'home_type', 'home_floor'], 'integer'],
            [['commentary', 'cause', 'description', 'tempFiles', 'description_object'], 'string'],
            [['date_cr', 'date_up', 'begin_date', 'end_date', 'last_call', 'call_back', 'date_add_commission'], 'safe'],
            [['house_type'], 'required'],
            [['total_area', 'kitchen_area', 'living_area', 'cost', 'distance_to_city', 'home_area', 'land_area', 'commission'], 'number'],
            [['video', 'surname_owner', 'name_owner', 'middle_name_owner', 'telephone', 'purpose_call', 'metro', 'city', 'location_metro', 'address', 'dom', 'map', 'coordinate_x', 'coordinate_y'], 'string', 'max' => 255],
            [['clients_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['clients_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clients_id' => 'Clients ID',
            'house_id' => 'House ID',
            'commentary' => 'Commentary',
            'cause' => 'Cause',
            'situation' => 'Situation',
            'commission' => 'Commission',
            'date_add_commission' => 'Date Add Commission',
            'date_cr' => 'Date Cr',
            'date_up' => 'Date Up',
            'house_type' => 'House Type',
            'begin_date' => 'Begin Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'rooms_count' => 'Rooms Count',
            'type' => 'Type',
            'floor' => 'Floor',
            'house_floor' => 'House Floor',
            'total_area' => 'Total Area',
            'kitchen_area' => 'Kitchen Area',
            'living_area' => 'Living Area',
            'beds_count' => 'Beds Count',
            'berth_count' => 'Berth Count',
            'conditioner' => 'Conditioner',
            'camin' => 'Camin',
            'furniture' => 'Furniture',
            'balcony' => 'Balcony',
            'microwave' => 'Microwave',
            'fridge' => 'Fridge',
            'washer' => 'Washer',
            'wifi' => 'Wifi',
            'tv' => 'Tv',
            'digital_tv' => 'Digital Tv',
            'with_pets' => 'With Pets',
            'with_childrens' => 'With Childrens',
            'with_events' => 'With Events',
            'description' => 'Description',
            'cost' => 'Cost',
            'zalog' => 'Zalog',
            'video' => 'Video',
            'tempFiles' => 'Temp Files',
            'surname_owner' => 'Surname Owner',
            'name_owner' => 'Name Owner',
            'middle_name_owner' => 'Middle Name Owner',
            'telephone' => 'Telephone',
            'last_call' => 'Last Call',
            'call_back' => 'Call Back',
            'purpose_call' => 'Purpose Call',
            'public_avito' => 'Public Avito',
            'description_object' => 'Description Object',
            'client_id' => 'Client ID',
            'metro' => 'Metro',
            'wall_material' => 'Wall Material',
            'distance_to_city' => 'Distance To City',
            'home_area' => 'Home Area',
            'land_area' => 'Land Area',
            'city' => 'City',
            'location_metro' => 'Location Metro',
            'address' => 'Address',
            'dom' => 'Dom',
            'map' => 'Map',
            'coordinate_x' => 'Coordinate X',
            'coordinate_y' => 'Coordinate Y',
            'room_area' => 'Room Area',
            'home_type' => 'Home Type',
            'home_floor' => 'Home Floor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasOne(Clients::className(), ['id' => 'clients_id']);
    }

    public function getMonth($i)
    {
        if($i == 1) return 'Январь';
        if($i == 2) return 'Февраль';
        if($i == 3) return 'Март';
        if($i == 4) return 'Апрель';
        if($i == 5) return 'Май';
        if($i == 6) return 'Июнь';
        if($i == 7) return 'Июль';
        if($i == 8) return 'Август';
        if($i == 9) return 'Сентябрь';
        if($i == 10) return 'Октябрь';
        if($i == 11) return 'Ноябрь';
        if($i == 12) return 'Декабрь';
    }
}
