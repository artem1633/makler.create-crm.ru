<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchased_licenses".
 *
 * @property int $id
 * @property int $user_id
 * @property int $license_type
 * @property int $user_type
 * @property int $month
 * @property double $cost
 * @property string $data
 *
 * @property Users $user
 */
class PurchasedLicenses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchased_licenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'license_type', 'user_type', 'month'], 'integer'],
            [['cost'], 'number'],
            [['data'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'license_type' => 'License Type',
            'user_type' => 'User Type',
            'month' => 'Month',
            'cost' => 'Cost',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
