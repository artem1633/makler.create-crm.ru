<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "owner_contacts".
 *
 * @property int $id
 * @property string $telephone
 * @property string $contact
 * @property int $house_id
 *
 * @property House $house
 */
class OwnerContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'owner_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_id'], 'integer'],
            [['telephone', 'contact'], 'string', 'max' => 255],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephone' => 'Telephone',
            'contact' => 'Contact',
            'house_id' => 'House ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }
}
