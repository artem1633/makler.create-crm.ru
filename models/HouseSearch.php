<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\House;

/**
 * HouseSearch represents the model behind the search form about `app\models\House`.
 */
class HouseSearch extends House
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'house_type', 'status', 'rooms_count', 'type', 'floor', 'house_floor', 'beds_count', 'berth_count', 'conditioner', 'camin','furniture', 'balcony', 'microwave', 'fridge', 'washer', 'wifi', 'tv', 'digital_tv', 'with_pets', 'with_childrens', 'with_events', 'zalog', 'public_avito', 'client_id', 'wall_material'], 'integer'],
            [['begin_date', 'end_date','when_delivered','information', 'description', 'video', 'documents','surname_owner', 'name_owner', 'middle_name_owner', 'telephone', 'last_call', 'call_back', 'purpose_call', 'description_object', 'metro', 'address', 'dom', 'city', 'location_metro'], 'safe'],
            [['total_area', 'kitchen_area', 'living_area', 'cost', 'distance_to_city', 'home_area', 'land_area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type, $post)
    {
        //$query = House::find();
        if($type == 1) $query = House::find()->where(['house_type' => [1,2]]);
        if($type == 2) $query = House::find()->where(['house_type' => [3,4]]);
        if($type == 3) $query = House::find()->where(['house_type' => [5,6]]);

        if($type == 4) $query = House::find()->where(['house_type' => 1 ]);
        if($type == 5) $query = House::find()->where(['house_type' => 3 ]);
        if($type == 6) $query = House::find()->where(['house_type' => 5 ]); 

        if($type == 7) $query = House::find()->where(['house_type' => 2 ]);
        if($type == 8) $query = House::find()->where(['house_type' => 4 ]);
        if($type == 9) $query = House::find()->where(['house_type' => 6 ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['call_back'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //$query->orderBy('id DESC');
        $query->andFilterWhere([
            'id' => $this->id,
            'house_type' => $this->house_type,
            'begin_date' => $this->begin_date,
            'when_delivered'=>$this->when_delivered,
            'end_date' => $this->end_date,
            'status' => $this->status,
            'rooms_count' => $this->rooms_count,
            'type' => $this->type,
            'floor' => $this->floor,
            'house_floor' => $this->house_floor,
            'total_area' => $this->total_area,
            'kitchen_area' => $this->kitchen_area,
            'living_area' => $this->living_area,
            'beds_count' => $this->beds_count,
            'berth_count' => $this->berth_count,
            'conditioner' => $this->conditioner,
            'camin' => $this->camin,
            'balcony' => $this->balcony,
            'furniture'=>$this->furniture,
            'microwave' => $this->microwave,
            'fridge' => $this->fridge,
            'washer' => $this->washer,
            'wifi' => $this->wifi,
            'tv' => $this->tv,
            'digital_tv' => $this->digital_tv,
            'with_pets' => $this->with_pets,
            'with_childrens' => $this->with_childrens,
            'with_events' => $this->with_events,
            'cost' => $this->cost,
            'zalog' => $this->zalog,
            'information'=>$this->information,
            'last_call' => $this->last_call,
            'call_back' => $this->call_back,
            'public_avito' => $this->public_avito,
            'client_id' => $this->client_id,
            'wall_material' => $this->wall_material,
            'distance_to_city' => $this->distance_to_city,
            'home_area' => $this->home_area,
            'land_area' => $this->land_area,
        ]);
        $session = Yii::$app->session;
        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'house.id', $session['search_id']])
            ->andFilterWhere(['like', 'surname_owner', $session['search_owner_surname']])
            ->andFilterWhere(['like', 'telephone', $session['search_telephone']])
            ->andFilterWhere(['like', 'address', $session['search_address']])
            ->andFilterWhere(['like', 'dom', $session['search_home_number']])
            ->andFilterWhere(['like', 'city', $session['search_city']])
            ->andFilterWhere(['like', 'location_metro', $session['search_metro']])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'name_owner', $session['search_owner_name']])
            ->andFilterWhere(['like', 'middle_name_owner', $this->middle_name_owner])
            ->andFilterWhere(['like', 'purpose_call', $this->purpose_call])
            ->andFilterWhere(['like', 'description_object', $this->description_object])
            ->andFilterWhere(['like', 'metro', $this->metro]);

        return $dataProvider;
    }
}
