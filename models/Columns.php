<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\TextInput;

/**
 * This is the model class for table "columns".
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $name
 * @property int $status
 */
class Columns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'columns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['type', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    public function setDefaultValues($user_id)
    {
        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'id'; //Адрес/Статус
        $column->name = 'ID';
        $column->status = 1;
        $column->order_number = 1;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'addres/status'; //Адрес/Статус
        $column->name = 'Адрес/Статус';
        $column->status = 1;
        $column->order_number = 2;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'house_type'; //Тип
        $column->name = 'Тип';
        $column->status = 1;
        $column->order_number = 3;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'category'; //Категория
        $column->name = 'Категория';
        $column->status = 1;
        $column->order_number = 4;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'rooms_count'; //К-во комн
        $column->name = 'К-во комн';
        $column->status = 1;
        $column->order_number = 5;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'cost'; //Цена
        $column->name = 'Цена';
        $column->status = 1;
        $column->order_number = 6;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'floor'; //Этаж
        $column->name = 'Этаж';
        $column->status = 1;
        $column->order_number = 7;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'site'; //Сайт
        $column->name = 'Сайт';
        $column->status = 1;
        $column->order_number = 8;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'last_call'; //Последний звонок
        $column->name = 'Последний звонок';
        $column->status = 1;
        $column->order_number = 9;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'call_why'; //Позвонить Зачем
        $column->name = 'Позвонить';
        $column->status = 1;
        $column->order_number = 10;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'purpose_call'; //Цель звонка
        $column->name = 'Цель звонка';
        $column->status = 1;
        $column->order_number = 11;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'owner_fio'; //ФИО
        $column->name = 'ФИО';
        $column->status = 1;
        $column->order_number = 12;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'telephone'; //Телефон
        $column->name = 'Телефон';
        $column->status = 1;
        $column->order_number = 13;
        $column->save();



        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'home_type'; //Тип дома
        $column->name = 'Тип дома'; 
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'total_area'; //Общая площадь
        $column->name = 'Общая площадь'; 
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'kitchen_area'; //Кухня площадь
        $column->name = 'Кухня площадь'; 
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'living_area'; //Жилая  площадь
        $column->name = 'Жилая  площадь'; 
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'room_area'; //Площадь комнаты
        $column->name = 'Площадь комнаты'; 
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'home_area'; //Площадь дома
        $column->name = 'Площадь дома';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'land_area'; //Площадь участка
        $column->name = 'Площадь участка';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'conditioner'; //Кондиционер
        $column->name = 'Кондиционер';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'tv'; //ТВ
        $column->name = 'ТВ';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'furniture'; //Без мебели
        $column->name = 'Без мебели';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'washer'; //Стиральная машина
        $column->name = 'Стиральная машина';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'fridge'; //Холодильник
        $column->name = 'Холодильник';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'description'; //Описание
        $column->name = 'Описание';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'zalog'; //Залог
        $column->name = 'Залог';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'video'; //Видео
        $column->name = 'Видео';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'public_avito'; //Авито
        $column->name = 'Авито';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'wall_material'; //Материал стен
        $column->name = 'Материал стен';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'distance_to_city'; //Расст.до города
        $column->name = 'Расст.до города';
        $column->status = 0;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->type = 'metro'; //Метро рядом
        $column->name = 'Метро рядом';
        $column->status = 0;
        $column->save();
    }

    public function getAttributeVisible($user_id, $content, $name)
    {
        if($content == 0){
            if($name == 'addres/status' || $name == 'house_type' || $name == 'category' || $name == 'rooms_count' || $name == 'cost' || $name == 'floor' || $name == 'site' || $name == 'last_call' || $name == 'call_why'|| $name == 'purpose_call' || $name == 'owner_fio' || $name == 'telephone' )
            return true;
            else return false;
        }
        else { 
            $column = Columns::find()->where(['user_id' => $user_id, 'type' => $name])->one();
            /*echo "user_id = ". $user_id. ' <br> name = '.$type;
            echo "<pre>";
            print_r($column);
            echo "</pre>";
            die;*/
            if($column->status == 1) return true;
            else return false;
        }
    }

    public function getColumns($type)
    {
        //echo "type=".$type;die;
        $datehouse ->house->date_cr;
        $user_id = Yii::$app->user->id;
        $content = 0;
        $column = Columns::find()->where(['user_id' => $user_id])->one();
        if($column == null) $content = 0;
        else $content = 1;
        $columns = new Columns();

        $active_columns = Columns::find()->where(['user_id' => $user_id, 'status' => 1])->orderBy([ 'order_number' => SORT_ASC ])->all();
        $result = [];
        foreach ($active_columns as $value) {

            if($value->type == 'id')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'id',
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
               ];
            }

            if($value->type == 'addres/status')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'address',
                    'label' => 'Адрес/Статус',
                    'content' => function ($data) {
                        $class = "";
                        if($data->status == 1) $class = "btn-default";
                        if($data->status == 2) $class = "btn-success";
                        if($data->status == 3) $class = "btn-danger";

                        return '<a data-pjax=0 class="btn '.$class.' btn-sm" href="'.Url::toRoute(['house/view', 'id' => $data->id]).'">'.$data->address. ', ' . $data->dom . '</a> '. Html::a('<span style="font-size:16px;" class="glyphicon glyphicon-pencil"></span>', ['/house/change-location?id='.$data->id.'&type='.($data->house_type+3)], ['title' => "Редактировать адрес", ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
               ]; 
            }

            if($value->type == 'house_type')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'house_type',
                    'content' => function ($data) {
                        if($data->house_type == 1 | $data->house_type == 2) return 'Кв';
                        if($data->house_type == 3 | $data->house_type == 4) return 'Ком';
                        if($data->house_type == 5 | $data->house_type == 6) return 'Дом';
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
               ];
            }

            if($value->type == 'category')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'house_type',
                    'label' => 'Категория',
                    'content' => function ($data) {
                        if($data->house_type == 1 || $data->house_type == 3 || $data->house_type == 5) return 'Длит';
                        if($data->house_type == 2 || $data->house_type == 4 || $data->house_type == 6) return 'Посут';
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'rooms_count')
            {
                if($type != 3 && $type != 6 && $type != 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'rooms_count',
                        'label' => 'Кол-во Комнат',
                        'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->rooms_count, 
                            $data->getRoomsCountList(), 
                            [
                                'prompt' => '', 
                                'id' => 'type',
                                'onchange'=>"
                                    $.get('/house/edit-rooms', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:100px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'cost')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'cost',
                    'content' => function($data){
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->cost,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/edit-cost', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'floor')
            {
                if($type != 3 && $type != 6 && $type != 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'floor',
                        'content' => function ($data) {
                            if($data->house_type == 5 || $data->house_type == 6){
                                if($data->home_floor == 6) return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/floors', 'id' => $data->id]).'">'.'5+'. '</a>';
                                else return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/floors', 'id' => $data->id]).'">'.$data->home_floor. '</a>';
                            }
                            else return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/floors', 'id' => $data->id]).'">'.$data->house_floor. '/'. $data->floor. '</a>';          
                        },
                        'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                   ];
               }
            }

            if($value->type == 'site')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'public_site',
                    'label' => 'Сайт',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->public_site, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-site', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'last_call')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'last_call',
                    'label' => 'Последний звонок',
                    'content' => function ($data) {
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => \Yii::$app->formatter->asDate($data->last_call, 'php:d.m.Y'),
                            'options' => [
                                'class' =>'form-control',
                                'onchange'=>"$.get('/house/edit-last-call', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['alias' =>  'dd.mm.yyyy']
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'call_why')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'call_back',
                    'content' => function ($data) {
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => \Yii::$app->formatter->asDate($data->call_back, 'php:d.m.Y'),
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/edit-call-back', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['alias' =>  'dd.mm.yyyy']
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'purpose_call')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'purpose_call',
                    'content' => function ($data) {
                        $title = substr($data->purpose_call, 0, 20) . '...';
                        return '<span class="label" style="font-size:13px;">'. Html::a( $title, ['/house/show-purpose-call','id' =>$data->id], ['title' => 'Просмотр', 'style' => 'color:#337ab7;','role'=>'modal-remote',]) . '</span>';
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'owner_fio')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'surname_owner',
                    'label' => 'ФИО',
                    'content' => function ($data) {
                           return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/fio', 'id' => $data->id]).'">'.$data->surname_owner . ' ' . $data->name_owner . ' ' . $data->middle_name_owner. '</a>';        
                        },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'telephone')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'telephone',
                    'content' => function($data){
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'telephone',
                            'value' => $data->telephone,
                            'mask' => '+7 (999) 999-99-99',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:130px;',
                                'onchange'=>"$.get('/house/edit-telephone', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 1, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                ];
            }

            if($value->type == 'home_type')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'home_type',
                        'content' => function ($data) {
                        return Html::dropDownList( 
                            'data', 

                            $data->home_type, 
                            $data->getHomeTypeNameColumns(), 
                            [
                                'prompt' => '', 
                                'id' => 'type',
                                'onchange'=>"
                                    $.get('/house/edit-home-type', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:100px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'total_area')
            {
                if($type == 1 || $type == 4 || $type == 7)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'total_area',
                        'content' => function($data){
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->total_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/total', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'kitchen_area')
            {
                if($type == 1 || $type == 4 || $type == 7)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'kitchen_area',
                        'content' => function($data){
                        return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->kitchen_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/kitchen', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'living_area')
            {
                if($type == 1 || $type == 4 || $type == 7)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'living_area',
                        'content' => function($data){
                         return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->living_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/living', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'room_area')
            {
                if($type == 2 || $type == 5 || $type == 8)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'room_area',
                        'content' => function($data){
                         return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->room_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/room', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'home_area')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'home_area',
                        'content' => function($data){
                         return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->home_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/edit-home', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'land_area')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'land_area',
                        'content' => function($data){
                         return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->land_area,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/edit-land', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'conditioner')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'conditioner',
                     'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->conditioner, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-conditioner', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'tv')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'tv',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->tv, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-tv', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'furniture')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'furniture',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->furniture, 
                            $data->getFurniture(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-furniture', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }
            if($value->type == 'washer')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'washer',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->washer, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-washer', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'fridge')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'fridge',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->fridge, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-fridge', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'description')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'description',
                    'content' => function ($data) {
                        $title = substr($data->description, 0, 20) . '...';
                        return '<span class="label" style="font-size:13px;">'. Html::a( $title, ['/house/description','id' =>$data->id], ['title' => 'Просмотр', 'style' => 'color:#337ab7;','role'=>'modal-remote',]) . '</span>';
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'zalog')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'zalog',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->zalog, 
                            $data->getZalogList(), 
                            [
                                'prompt' => '', 
                                'id' => 'type',
                                'onchange'=>"
                                    $.get('/house/edit-zalog', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:110px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'video')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'video',
                    'content' => function ($data) {
                           return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/video', 'id' => $data->id]).'">'.$data->video . '</a>';        
                        },
                        'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'public_avito')
            {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'public_avito',
                    'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->public_avito, 
                            $data->getAvitoList(), 
                            [
                                'onchange'=>"
                                    $.get('/house/edit-public-avito', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:70px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                ];
            }

            if($value->type == 'wall_material')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'wall_material',
                        'content' => function ($data) {
                        return Html::dropDownList(
                            'data', 
                            $data->wall_material, 
                            $data->getWallMaterialList(), 
                            [
                                'prompt' => '', 
                                'id' => 'type',
                                'onchange'=>"
                                    $.get('/house/edit-wall-material', {'id':$data->id, 'value':$(this).val()}, function(data){} ); 
                                ",
                                'class' =>'form-control',
                                'style'=>'width:120px;'
                            ]
                        );
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                   ];
               }
            }

            if($value->type == 'distance_to_city')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'distance_to_city',
                        'content' => function($data){
                         return \yii\widgets\MaskedInput::widget([
                            'name' => 'data',
                            'value' => $data->distance_to_city,
                            'mask' => '9',
                            'options' => [
                                'class' =>'form-control',
                                'style' => 'width:100px;',
                                'onchange'=>"$.get('/house/distance', {'id':$data->id, 'value':$(this).val()}, function(data){ 
                                } ); 
                                ",
                            ],
                            'clientOptions' => ['repeat' => 10, 'greedy' => false]
                        ]);
                    },
                    'contentOptions' => function ($model, $key, $index, $column) {
                    if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                    },
                   ];
               }
            }

            if($value->type == 'metro')
            {
                if($type == 3 || $type == 6 || $type == 9)
                {
                    $result [] = [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'metro',
                       'content' => function ($data) {
                           return '<a role="modal-remote" class="btn btn-warning btn-sm" href="'.Url::toRoute(['house/metro', 'id' => $data->id]).'">'.$data->metro . '</a>';        
                        },
                        'contentOptions' => function ($model, $key, $index, $column) {
                        if(date('Y-m-d', strtotime('+1 day')) == $model->call_back) return ['style' => 'color:black;background-color:red' ];
                        },
                    ];
                }
            }
        }

        $result [] = [
            
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{delete}',
            'dropdown' => false,
            'vAlign'=>'middle',
            'urlCreator' => function($action, $model, $key, $index) { 
                    return Url::to([$action,'id'=>$key]);
            },
            'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
            'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
            'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить данного элемента?'], 
        ]; 

        return $result;
    }
}
