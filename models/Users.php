<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $creation_date
 * @property string $end_date
 * @property string $birthday
 * @property string $telephone
 * @property string $email
 * @property string $login
 * @property string $password
 * @property int $type
 * @property double $balance
 * @property int $license
 * @property int $licensed_user
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    public $new_password;
    public $money;
    public $send_objects;
    public $transfer;
    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'type'], 'required'],
            [['creation_date', 'end_date', 'birthday'], 'safe'],
            [['type', 'license', 'licensed_user'], 'integer'],
            [['balance', 'money'], 'number'],
            [['email'], 'email'],
            [['fio', 'telephone', 'email', 'login', 'password','new_password', 'send_objects'], 'string', 'max' => 255],
            [['license'], 'required', 'when' => function() {
                   if($this->type == 2) return TRUE;
                   else return FALSE;
            }],
            [['licensed_user'], 'required', 'when' => function() {
                   if($this->type == 3) return TRUE;
                   else return FALSE;
            }],
            ['money', 'validateMoney'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'creation_date' => 'Дата создания счёта',
            'end_date' => 'Дата окончание лицензии',
            'birthday' => 'Дата рождения',
            'telephone' => 'Телефон',
            'email' => 'Email',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Тип',
            'balance' => 'Счёт',
            'license' => 'Лицензия',
            'licensed_user' => 'Выберите Лицензиата',
            'new_password' => 'Новый пароль',
            'money' => 'Пополнить',
            'transfer' => 'Передать',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanges()
    {
        return $this->hasMany(Change::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchasedLicenses()
    {
        return $this->hasMany(PurchasedLicenses::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNastroykas()
    {
        return $this->hasMany(Nastroyka::className(), ['user_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = md5($this->password);
        }
        if($this->type == 2) $this->end_date =  date('Y-m-d', (time() + 14 * 60 * 60 * 24));
        if($this->new_password != null) $this->password = md5($this->new_password);
        if($this->birthday != null) $this->birthday = \Yii::$app->formatter->asDate($this->birthday, 'php:Y-m-d');
        return parent::beforeSave($insert);
    }

    public function getType()
    {
        return ArrayHelper::map([
            ['id' => '1','name' => 'Администратор',],
            ['id' => '2','name' => 'Лицензиат',],
            ['id' => '3','name' => 'Агент',], 
        ], 'id', 'name');
    }

    public function getLisenceType()
    {
        return ArrayHelper::map([
            ['id' => '1','name' => 'ONE (1 агент) - 2 000 руб. / месяц ',],
            ['id' => '2','name' => 'TRIO (3 агента) - 4 000 руб. / месяц ',],
            ['id' => '3','name' => 'FIVE (5 агентов) - 6 000 руб. / месяц ',], 
            ['id' => '4','name' => 'PRO (12 агентов) - 12 000 руб. / месяц',], 
            //['id' => '5','name' => 'SuperPRO',], 
        ], 'id', 'name');
    }

    public function getMonth($i)
    {
        if($i == 1) return 'Январь';
        if($i == 2) return 'Февраль';
        if($i == 3) return 'Март';
        if($i == 4) return 'Апрель';
        if($i == 5) return 'Май';
        if($i == 6) return 'Июнь';
        if($i == 7) return 'Июль';
        if($i == 8) return 'Август';
        if($i == 9) return 'Сентябрь';
        if($i == 10) return 'Октябрь';
        if($i == 11) return 'Ноябрь';
        if($i == 12) return 'Декабрь';
    }

    public function getMonthCost($type)
    {
        if($type == 1) return '2000';
        if($type == 2) return '4000';
        if($type == 3) return '6000';
        if($type == 4) return '12000';
    }

    public function getCurrentStatus($type)
    {
        if($type == $this->license) return 1;
        else return 0;
    }

    public function getLisencedUser()
    {
        $users = Users::find()->where(['type' => 2])->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }
    public function addHistory()
    {
        $history = new Change();
        $history->data = date('Y-m-d H:i:s');
        $history->user_id = $this->id;
        $history->summa = $this->money;
        $history->save();

    }

    public function validateMoney($attribute)
    { 
       if($this->money < 0) $this->addError($attribute, 'Вводите положителное средства');
    }

}
