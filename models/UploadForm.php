<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $anyFiles;
    public $anyDocuments;

    /** 
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anyFiles'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 20],
            [['anyDocuments'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'anyFiles' => 'Файлы',
            'anyDocuments' => 'Документы',
        ];
    }

    /**
     * Ajax upload images
     *
     * @param Directory $directory
     * @param $order_id
     * @return bool|string
     */
    public function upload(Directory $directory, $order_id)
    {
        if ($this->validate()) {

            $path = $directory->createDirectory($order_id);

            $file = $this->anyFiles[0];
            $filename = Yii::$app->security->generateRandomString(8) . '.' . $file->extension;
            $file->saveAs($path . $filename);

            return $path . $filename;
        } else {
            return false;
        }
    }

    public function uploadDocuments(Directory $directory, $order_id)
    {
        if ($this->validate()) {

            $path = $directory->createDirectoryDocuments($order_id);

            $file = $this->anyDocuments[0];
            $filename = Yii::$app->security->generateRandomString(8) . '.' . $file->extension;
            $file->saveAs($path . $filename);

            return $path . $filename;
        } else {
            return false;
        }
    }
}