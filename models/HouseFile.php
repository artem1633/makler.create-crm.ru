<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "house_file".
 *
 * @property int $id
 * @property int $house_id
 * @property string $title
 * @property string $path
 *
 * @property House $house
 */
class HouseFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'house_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_id'], 'integer'],
            [['path'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'house_id' => 'House ID',
            'title' => 'Title',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }
    
    public static function getPathes($orderId) {
        $pathes = self::find()->select('path')->asArray()->all();
        $res = [];
        foreach ($pathes as $path) {
            $res[] = DIRECTORY_SEPARATOR . $path['path'];
        }

        return $res;
    }
// через эту функцию можно скачать файлов задач 
    public static function getFilesContent($orderId) {
        $resArr = self::find()->where(['order_id' => $orderId])->asArray()->all();

        $str = "<div class='files-content'>";
        foreach ($resArr as $item) {
            $id = $item['id'];
            $title = $item['title'];
            $path = Url::to(['tasks/download', 'path' => $item['path']]);
            $removeUrl = Url::to(['/ajax/remove-file', 'id' => $id]);
            $str .= "<div class='file-item'>
                        <a href='{$path}' id='{$id}' class='filename'>{$title}</a>
                        <a href='{$removeUrl}' class='remove-file'><i class='fa fa-times' aria-hidden='true'></i></a>
                     </div>";
        }

        $str .= "</div>";

        return $str;
    }
}
