<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nastroyka".
 *
 * @property int $id
 * @property int $user_id
 * @property string $coordinate_x
 * @property string $coordinate_y
 * @property string $city
 * @property string $location_metro
 * @property string $address
 * @property string $dom
 *
 * @property Users $user
 */
class Nastroyka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nastroyka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['coordinate_x', 'coordinate_y', 'city', 'location_metro', 'address', 'dom'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'coordinate_x' => 'Coordinate X',
            'coordinate_y' => 'Coordinate Y',
            'city' => 'Город, населённый пункт',
            'location_metro' => 'Метро',
            'address' => 'Адрес, улица',
            'dom' => 'Дом',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
