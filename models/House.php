<?php

namespace app\models; 

use Yii;
use yii\helpers\ArrayHelper;
 
/** 
 * This is the model class for table "house".
 *
 * @property int $id
 * @property int $house_type Квартиры/Комнаты/Дома
 * @property string $begin_date Дата начала
 * @property string $end_date Дата окончании
 * @property int $status Статус
 * @property int $rooms_count Количество комнат
 * @property int $type Тип дома
 * @property int $floor Этаж
 * @property int $house_floor Этажей в доме
 * @property double $total_area Общая площадь
 * @property double $kitchen_area Площадь кухни
 * @property double $living_area Жилая площадь
 * @property int $beds_count Количество кроватей
 * @property int $berth_count Количество спальных мест
 * @property int $conditioner Кондиционер
 * @property int $camin Камин
 * @property int $balcony Балкон
 * @property int $microwave Микроволновка
 * @property int $fridge Холодильник
 * @property int $washer Стиральная машина
 * @property int $wifi WiFi
 * @property int $tv Телевизор
 * @property int $digital_tv Цифровое ТВ
 * @property int $with_pets Можно с питомцами
 * @property int $with_childrens Можно с детьми
 * @property int $with_events Можно для мероприятий
 * @property string $description Описание
 * @property double $cost Цена
 * @property int $zalog Залог
 * @property string $video Видео
 * @property string $surname_owner Фамилия собственника
 * @property string $name_owner Имя собственника
 * @property string $middle_name_owner Отчество собственника
 * @property string $last_call Последний звонок собеседнику
 * @property string $call_back Перезвонить
 * @property string $purpose_call Цель звонка
 * @property int $public_avito Публикация на авито
 * @property string $description_object Описание объекта
 * @property int $client_id Кто снимает сейчас
 * @property string $metro Метро рядом
 * @property int $wall_material Материал стен
 * @property double $distance_to_city Расстояние до города
 * @property double $home_area Площадь дома
 * @property double $land_area Площадь участка
 */
class House extends \yii\db\ActiveRecord
{
    
    const SCENARIO_PARAMETER = 'PARAMETER';
    const SCENARIO_OPTIONS = 'OPTIONS';
    const SCENARIO_DESCRIPTION = 'DESCRIPTION';
    const SCENARIO_LOCATION = 'LOCATION';
    const SCENARIO_OWNER = 'OWNER';
    const SCENARIO_DATAS = 'DATAS';

    public $step;
    public $files;
    public $image;
    public $contacts;
    public $search_id;
    public $other_file;
    public $search_city;
    public $search_metro;
    public $search_owner_name;
    public $search_address;
    public $search_owner_surname;
    public $search_telephone;
    public $search_home_number;
    public static function tableName()
    {
        return 'house';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_PARAMETER] = ['status', 'rooms_count', 'type', 'floor', 'house_floor', 'total_area', 'kitchen_area', 'living_area', 'wall_material', 'distance_to_city', 'land_area', 'home_area', 'metro', 'home_type', 'begin_date', 'end_date', 'room_area', 'home_floor', 'public_site'];
        $scenarios[self::SCENARIO_OPTIONS] = ['beds_count', 'berth_count', 'conditioner', 'camin','balcony', 'microwave', 'fridge', 'washer','wifi', 'tv','furniture', 'digital_tv', 'with_pets','with_childrens', 'with_events'];
        $scenarios[self::SCENARIO_DESCRIPTION] = ['description','cost','documents', 'zalog', 'video','information', 'tempFiles'];
        $scenarios[self::SCENARIO_LOCATION] = ['city', 'location_metro', 'address', 'dom', 'map', 'coordinate_x', 'coordinate_y'];
        $scenarios[self::SCENARIO_OWNER] = ['surname_owner', 'name_owner', 'middle_name_owner', 'contacts', 'telephone'];
        $scenarios[self::SCENARIO_DATAS] = ['last_call','when_delivered', 'call_back', 'public_avito', 'description_object', 'client_id', 'purpose_call'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_type', 'status',], 'required', 'on' => self::SCENARIO_PARAMETER],
            [['house_type', 'status', 'rooms_count', 'type', 'floor', 'house_floor', 'beds_count', 'berth_count', 'conditioner', 'camin', 'balcony', 'microwave', 'fridge', 'washer', 'wifi', 'tv','furniture', 'digital_tv', 'with_pets', 'with_childrens', 'with_events', 'zalog', 'public_avito', 'client_id', 'wall_material', 'room_area', 'home_type', 'home_floor', 'public_site'], 'integer'],
            [['begin_date', 'end_date', 'last_call','when_delivered', 'call_back'], 'safe'],
            [['total_area', 'kitchen_area', 'living_area', 'cost', 'distance_to_city', 'home_area', 'land_area'], 'number', 'min' => 0],
            [['description', 'description_object', 'documents','tempFiles'], 'string'],
            [['video', 'surname_owner', 'name_owner', 'middle_name_owner', 'telephone', 'purpose_call', 'metro', 'city', 'location_metro','information', 'address', 'dom', 'map', 'coordinate_x', 'coordinate_y'], 'string', 'max' => 255],
            [['other_file'], 'file','maxSize' => 1024*1024*5,'maxFiles' => 10,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //-------------------Основние поля--------------------------//
            'id' => 'ID',
            'date_cr' => 'Создание карточки',
            'date_up' => 'Последнее изменение данных', 
            //-------------------Параметры-----------------------------//

            'house_type' => 'Тип', // 1=>Квартиры на длительный срок, 2=>Квартиры посуточно, 3=>Комнаты на длительный срок, 4=> Комнаты посуточно, 5=>Дома на длительный срок 6=>Дома посуточно,;
            'status' => 'Статус',
            'public_site' => 'Публикуется на сайте',
            'rooms_count' => 'Количество комнат',//Всего комнат
            'type' => 'Тип дома',
            'floor' => 'Этажность', //'Этаж',
            'house_floor' => 'Этаж', //'Этажей в доме',
            'total_area' => 'Общая площадь',
            'kitchen_area' => 'Площадь кухни',
            'living_area' => 'Жилая площадь',
            'begin_date' => 'Дата начала',
            'end_date' => 'Дата окончании',
            'when_delivered'=>'Когда сдана',
            //------------------------------------------------------------ //
            'room_area' => 'Площадь комнаты',
            //-----------------Доп парамерты, опция------------------------//
            'beds_count' => 'Количество кроватей',
            'berth_count' => 'Количество спальных мест',
            'conditioner' => 'Кондиционер',
            'camin' => 'Камин',
            'balcony' => 'Балкон',
            'microwave' => 'Микроволновка',
            'fridge' => 'Холодильник',
            'washer' => 'Стиральная машина',
            'wifi' => 'WiFi',
            'furniture'=>'Без мебели',
            'tv' => 'ТВ',
            'digital_tv' => 'Цифровое ТВ',
            'with_pets' => 'Можно с питомцами',
            'with_childrens' => 'Можно с детьми',
            'with_events' => 'Можно для мероприятий',
            //---------------------Описание---------------------------------//
            'description' => 'Описание',
            'cost' => 'Цена', 
            'zalog' => 'Залог',
            'video' => 'Видео',
            'documents'=> 'Документы',
            'other_file'=> 'Документы',
            'information'=>'Информация для сотрудников',
            //--------------------Местоположение----------------------------//
            'city' => 'Город, населённый пункт',
            'location_metro' => 'Метро',
            'address' => 'Адрес, улица',
            'dom' => 'Дом',
            'map' => 'Посмотреть на карте',
            'coordinate_x' => 'X',
            'coordinate_y' => 'Y',
            //--------------------Собственник-----------------------------//
            'surname_owner' => 'Фамилия собственника',
            'name_owner' => 'Имя собственника',
            'middle_name_owner' => 'Отчество собственника',
            'contacts' => 'Контакт номер',
            'telephone' => 'Телефон номер',
            //--------------------Даты------------------------------------//
            'last_call' => 'Последний звонок собеседнику',
            'call_back' => 'Позвонить', //'Перезвонить',
            'public_avito' => 'Публикация на авито',
            'description_object' => 'Описание объекта',
            'client_id' => 'Кто снимает сейчас',
            'purpose_call' => 'Цель звонка',
            //-----------Доп. параметры для Дома на длительный срок-----------//
            'wall_material' => 'Материал стен',
            'distance_to_city' => 'Расстояние до города',
            'land_area' => 'Площадь участка',
            'home_area' => 'Площадь дома',
            'metro' => 'Метро рядом',
            'home_type' => 'Тип дома', //'Вид обьекта',
            'home_floor' => 'Этажей в доме',
            //----------------Общие параметры--------------------------//
            'tempFiles' => 'Фото',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouseDocuments()
    {
        return $this->hasMany(HouseDocuments::className(), ['house_id' => 'id']);
    }
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getHouseFiles()
    {
        return $this->hasMany(HouseFile::className(), ['house_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerContacts()
    {
        return $this->hasMany(OwnerContacts::className(), ['house_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_cr = date('Y-m-d H:i:s');
            $this->public_site = 1;
        }
        $this->date_up = date('Y-m-d H:i:s');
        if($this->last_call != null ) $this->last_call = \Yii::$app->formatter->asDate($this->last_call, 'php:Y-m-d');
        if($this->call_back != null ) $this->call_back = \Yii::$app->formatter->asDate($this->call_back, 'php:Y-m-d');
        if($this->begin_date != null ) $this->begin_date = \Yii::$app->formatter->asDate($this->begin_date, 'php:Y-m-d');
        if($this->end_date != null ) $this->end_date = \Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d');
        if($this->when_delivered != null ) $this->when_delivered = \Yii::$app->formatter->asDate($this->when_delivered, 'php:Y-m-d');
        $this->setLocalisation();
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function setLocalisation()
    {
        $user_id = Yii::$app->user->id;
        $nastroyka = Nastroyka::find()->where(['user_id' => $user_id])->one();

        if($nastroyka == null){
            $nastroyka = new Nastroyka();
            $nastroyka->user_id = $user_id;
            $nastroyka->coordinate_x = $this->coordinate_x;
            $nastroyka->coordinate_y = $this->coordinate_y;
            $nastroyka->city = $this->city;
            $nastroyka->location_metro = $this->location_metro;
            $nastroyka->address = $this->address;
            $nastroyka->dom = $this->dom;
            $nastroyka->save();
        }
        else{
            $nastroyka->coordinate_x = $this->coordinate_x;
            $nastroyka->coordinate_y = $this->coordinate_y;
            $nastroyka->city = $this->city;
            $nastroyka->location_metro = $this->location_metro;
            $nastroyka->address = $this->address;
            $nastroyka->dom = $this->dom;
            $nastroyka->save();
        }
    }

    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
     
        if($this->client_id != null && $this->isNewRecord) $this->setClientsHouse();
    }*/

    public function addFiles($tempFiles, $order_id) {
        if(is_null($tempFiles)) {
            return false;
        }
        foreach ($tempFiles as $itemFile) {
            if($itemFile['name'] != null && $itemFile['path'] != null){
                $file = new HouseFile();
                $file->house_id = $order_id;
                $file->title = $itemFile['name'];
                $file->path = $itemFile['path'];
                $file->save();
                unset($file);
            }
        }
    }

    public function addDocuments($tempFiles, $order_id) {
        if(is_null($tempFiles)) {
            return false;
        }
        foreach ($tempFiles as $itemFile) {
            if($itemFile['name'] != null && $itemFile['path'] != null){
                $file = new HouseDocuments();
                $file->house_id = $order_id;
                $file->title = $itemFile['name'];
                $file->path = $itemFile['path'];
                $file->save();
                unset($file);
            }
        }
    }

    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'house'")
            ->one();
        if($res)
            return $res["AUTO_INCREMENT"];
    }

    public function getRoomsCountList($type = null) 
    {
        if($type == 'komnata') return ArrayHelper::map([
            ['id' => '2', 'title' => '2',],
            ['id' => '3', 'title' => '3',],
            ['id' => '4', 'title' => '4',],
            ['id' => '5', 'title' => '5',],
            ['id' => '6', 'title' => '6',],
            ['id' => '7', 'title' => '7',],
            ['id' => '8', 'title' => '8',],
            ['id' => '9', 'title' => '9',],
        ],
        'id', 'title');
        else return ArrayHelper::map([
            ['id' => '0', 'title' => 'Студия ',],
            ['id' => '1', 'title' => '1',],
            ['id' => '2', 'title' => '2',],
            ['id' => '3', 'title' => '3',],
            ['id' => '4', 'title' => '4',],
            ['id' => '5', 'title' => '5',],
            ['id' => '6', 'title' => '6',],
            ['id' => '7', 'title' => '7',],
            ['id' => '8', 'title' => '8',],
            ['id' => '9', 'title' => '9',],
        ],
        'id', 'title');
    }

    public function getBedsCountList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => '1',],
            ['id' => '2', 'title' => '2',],
            ['id' => '3', 'title' => '3',],
            ['id' => '4', 'title' => '4',],
            ['id' => '5', 'title' => '5',],
            ['id' => '6', 'title' => '6',],
            ['id' => '7', 'title' => '7',],
            ['id' => '8', 'title' => '8',],
            ['id' => '9', 'title' => '8+',],
        ],
        'id', 'title');
    }

    public function getBertsCountList()
    {
        $result = [];
        for ($i=1; $i < 17; $i++) { 
            $result [] = [
                'id' => $i,
                'title' => $i,
            ];
        }
        $result [] = [
                'id' => 17,
                'title' => '16+',
            ];
        return ArrayHelper::map($result, 'id', 'title');
    }

    public function getTypeHouseList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Кирпичный',],
            ['id' => '2', 'title' => 'Панельный',],
            ['id' => '3', 'title' => 'Блочный',],
            ['id' => '4', 'title' => 'Монолитный',],
            ['id' => '5', 'title' => 'Деревянный',],
        ],
        'id', 'title');
    }
    public function getHouseTypeName($id)
    {
        if($id == 1) return 'Кирпичный';
        if($id == 2) return 'Панельный';
        if($id == 3) return 'Блочный';
        if($id == 4) return 'Монолитный';
        if($id == 5) return 'Деревянный';
    }

    public function getHomeTypeName($id)
    {
        if($id == 1) return 'Дом';
        if($id == 2) return 'Дача';
        if($id == 3) return 'Коттедж';
        if($id == 4) return 'Таунхаус';
    }
    public function getHomeTypeNameColumns()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Дом',],
            ['id' => '2', 'title' => 'Дача',],
            ['id' => '3', 'title' => 'Коттедж',],
            ['id' => '4', 'title' => 'Таунхаус',],
        ],
        'id', 'title');
    }
    public function getWallMaterialList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Кирпичный',],
            ['id' => '2', 'title' => 'Брус',],
            ['id' => '3', 'title' => 'Бревно',],
            ['id' => '4', 'title' => 'Газоблоки ',],
            ['id' => '5', 'title' => 'Металл',],
            ['id' => '6', 'title' => 'Пеноблоки',],
            ['id' => '7', 'title' => 'Сэндвич-панели',],
            ['id' => '8', 'title' => 'Ж/б понели',],
            ['id' => '9', 'title' => 'Экспериментальные материалы',],
        ],
        'id', 'title');
    }

    public function getWallMaterialNameList($id)
    {
        if($id == 1) return 'Кирпичный';
        if($id == 2) return 'Брус';
        if($id == 3) return 'Бревно';
        if($id == 4) return 'Газоблоки ';
        if($id == 5) return 'Металл';
        if($id == 6) return 'Пеноблоки';
        if($id == 7) return 'Сэндвич-панели';
        if($id == 8) return 'Ж/б понели';
        if($id == 9) return 'Экспериментальные материалы';
    }

    public function getZalogList()
    {
        return ArrayHelper::map([
            ['id' => '0', 'title' => 'Без залога',],
            ['id' => '1', 'title' => '0,5 месяца',],
            ['id' => '2', 'title' => '1 месяц',],
            ['id' => '3', 'title' => '1,5 месяца',],
            ['id' => '4', 'title' => '2 месяца',],
            ['id' => '5', 'title' => '3 месяца',],
        ],
        'id', 'title');
    }
    public function getZalogName($id)
    {
        if($id == 0) return 'Без залога';
        if($id == 1) return '0,5 месяца';
        if($id == 2) return '1 месяц';
        if($id == 3) return '1,5 месяца';
        if($id == 4) return '2 месяца';
        if($id == 5) return '3 месяца';
    }

    public function getStatus()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Неизвестно',],
            ['id' => '2', 'title' => 'Свободен',],
            ['id' => '3', 'title' => 'Занят',],
        ],
        'id', 'title');
    }

    public function getAvitoList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Нет ',],
            ['id' => '2', 'title' => 'Да',],
        ],
        'id', 'title');
    }
    public function getFurniture()
    {
        return ArrayHelper::map([
            ['id' => '0', 'title' => 'Нет ',],
            ['id' => '1', 'title' => 'Да',],
        ],
        'id', 'title');
    }
    public function getTypeHomeList()
    {
        return ArrayHelper::map([
            ['id' => '0', 'title' => ' ',],
            ['id' => '1', 'title' => 'Дом ',],
            ['id' => '2', 'title' => 'Дача ',],
            ['id' => '3', 'title' => 'Коттедж ',],
            ['id' => '4', 'title' => 'Таунхаус  ',],
        ],
        'id', 'title');
    }

    public function getHomeFloorList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => '1',],
            ['id' => '2', 'title' => '2',],
            ['id' => '3', 'title' => '3',],
            ['id' => '4', 'title' => '4',],
            ['id' => '5', 'title' => '5',],
            ['id' => '6', 'title' => '5+',],
        ],
        'id', 'title');
    }

    public function getFloorList()
    {
        $result = [];
        for ($i=""; $i < 100; $i++) { 
            $result [] = [
                'id' => $i,
                'title' => $i,
            ];
        }
        return ArrayHelper::map($result, 'id', 'title');
    }

    public function getHouseFloorList($end)
    {
        if($end == null) $end =1;
        $result = [];
        for ($i=""; $i <=$end; $i++) { 
            $result [] = [
                'id' => $i,
                'title' => $i,
            ];
        }
        return ArrayHelper::map($result, 'id', 'title');
    }

    public function getClientsList()
    {
        $clients = Clients::find()->all();
        return ArrayHelper::map($clients, 'id', 'fio');
    }

    public function getClientName()
    {
        $client = Clients::findOne($this->client_id);
        return $client->fio;
    }
    public function getLocalisation()
    {
        $nastroyka = Nastroyka::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($nastroyka == null){

            $this->coordinate_x = '55.753187580818675';
            $this->coordinate_y = '37.62333811759736';
            $this->city = "Москва";
            $this->location_metro = "метро Площадь Революции";
            $this->address = "улица Ильинка";
            $this->dom = " 7";
        }
        else{

            $this->coordinate_x = $nastroyka->coordinate_x;
            $this->coordinate_y = $nastroyka->coordinate_y;
            $this->city = $nastroyka->city;
            $this->location_metro = $nastroyka->location_metro;
            $this->address = $nastroyka->address;
            $this->dom = $nastroyka->dom;
        }
        return $client->fio;
    }

    public function setClientsHouse()
    {
        /*$client_house = ClientsHouses::find()->where(['clients_id' => $this->client_id, 'house_id' => $this->id])->one();
        if($client_house == null) */
        if($this->client_id != null)
        {
            $client_house = new ClientsHouses();

            $client_house->clients_id = $this->client_id;
            $client_house->house_id = $this->id;
            $client_house->commentary = "";
            $client_house->cause = "";
            $client_house->situation = 1;

            $client_house->date_cr = $this->date_cr;
            $client_house->date_up = $this->date_up;
            $client_house->house_type = $this->house_type;
            $client_house->begin_date = $this->begin_date;
            $client_house->end_date = $this->end_date;
            $client_house->status = $this->status;
            $client_house->rooms_count = $this->rooms_count;
            $client_house->type = $this->type;
            $client_house->floor = $this->floor;
            $client_house->house_floor = $this->house_floor;
            $client_house->total_area = $this->total_area;
            $client_house->kitchen_area = $this->kitchen_area;
            $client_house->living_area = $this->living_area;
            $client_house->beds_count = $this->beds_count;
            $client_house->berth_count = $this->berth_count;
            $client_house->conditioner = $this->conditioner;
            $client_house->camin = $this->camin;
            $client_house->balcony = $this->balcony;
            $client_house->microwave = $this->microwave;
            $client_house->fridge = $this->fridge;
            $client_house->washer = $this->washer;
            $client_house->wifi = $this->wifi;
            $client_house->tv = $this->tv;
            $client_house->furniture = $this->furniture;
            $client_house->digital_tv = $this->digital_tv;
            $client_house->with_pets = $this->with_pets;
            $client_house->with_childrens = $this->with_childrens;
            $client_house->with_events = $this->with_events;
            $client_house->description = $this->description;
            $client_house->cost = $this->cost;
            $client_house->zalog = $this->zalog;
            $client_house->video = $this->video;
            $client_house->tempFiles = $this->tempFiles;
            $client_house->surname_owner = $this->surname_owner;
            $client_house->name_owner = $this->name_owner;
            $client_house->middle_name_owner = $this->middle_name_owner;
            $client_house->telephone = $this->telephone;
            $client_house->last_call = $this->last_call;
            $client_house->call_back = $this->call_back;
            $client_house->purpose_call = $this->purpose_call;
            $client_house->public_avito = $this->public_avito;
            $client_house->description_object = $this->description_object;
            $client_house->client_id = $this->client_id;
            $client_house->metro = $this->metro;
            $client_house->wall_material = $this->wall_material;
            $client_house->distance_to_city = $this->distance_to_city;
            $client_house->home_area = $this->home_area;
            $client_house->land_area = $this->land_area;
            $client_house->city = $this->city;
            $client_house->location_metro = $this->location_metro;
            $client_house->address = $this->address;
            $client_house->dom = $this->dom;
            $client_house->map = $this->map;
            $client_house->coordinate_x = $this->coordinate_x;
            $client_house->coordinate_y = $this->coordinate_y;
            $client_house->room_area = $this->room_area;
            $client_house->home_type = $this->home_type;
            $client_house->home_floor = $this->home_floor;
            $client_house->save();
        }
    }
}
